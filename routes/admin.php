<?php

use App\Http\Controllers\TinymceController;
use App\Http\Controllers\Backend\{Auth\LoginController,
    CategoryController,
    ConfigController,
    MainInfoController,
    PageController,
    PhotoController,
    PostController,
    Settings\LogController,
    Settings\SettingController,
    SiteController,
    TagController,
    TranslateController,
    UserController};
use App\Http\Controllers\CkeditorController;
use Illuminate\Support\Facades\Route;

Route::middleware(['auth', 'preventBackHistory'])->group(function () {
    Route::get('/', [SiteController::class, 'index'])->name('home');
    Route::get('/change-locale/{lang}', [SiteController::class, 'changeLocale'])->name('change-locale');

    Route::middleware('can:settings/*')->group(function () {
        Route::resource('settings', SettingController::class)->except(['show']);
        Route::resource('logs', LogController::class)->only(['index', 'show']);
        Route::put('settings/chosen-tags/update/{id}', [SettingController::class, 'updateChosenTags'])->name('settings.chosen-tags.update');
    });

    Route::middleware('can:categories/*')->group(function () {
        Route::resource('categories', CategoryController::class)->except(['show']);
        Route::get('categories/move/{id}/{type}', [CategoryController::class, 'move'])
            ->where('type', 'up|down')
            ->where('id', '[0-9]+')
            ->name('categories.move');
        Route::get('categories/translate-list', [CategoryController::class, 'translateList'])
            ->middleware('crossAjax')
            ->name('categories.translate-list');
        Route::get('categories/create-translate', [CategoryController::class, 'create'])
            ->middleware('crossAjax')
            ->name('categories.createTranslate');
    });

    Route::middleware('can:tags/*')->group(function () {
        Route::get('tags/merge', [TagController::class, 'merge'])
            ->name('tags.merge');
        Route::resource('tags', TagController::class);
    });

    Route::get('post/restore/{id}', [PostController::class, 'restore'])
        ->middleware('can_any:post/*|post/restore')
        ->name('post.restore');
    Route::get('post/publish/{id}', [PostController::class, 'publish'])
        ->middleware('can_any:post/*|post/publish')
        ->name('post.publish');
    Route::get('post/reject/{id}', [PostController::class, 'reject'])
        ->middleware('can_any:post/*|post/reject')
        ->name('post.reject');

    Route::get('post/delete-reason', [PostController::class, 'deleteReason'])
        ->middleware('can_any:*/delete|posts/*')
        ->name('post.deleteReason');

    Route::resource('post', PostController::class);
    Route::put('post/update-counts/{id}', [PostController::class, 'updateCounts'])
        ->middleware('can_any:post/*|posts/*')
        ->name('post.update-counts');
    Route::get('post/cancel-form/{id}', [PostController::class, 'cancelForm'])
        ->name('post.cancel-form');

    Route::get('posts/tag-list', [PostController::class, 'tagList'])
        ->middleware(['can_any:posts/*|posts/create|posts/edit', 'crossAjax'])
        ->name('posts.tag-list');

    Route::get('posts/translate-list', [PostController::class, 'translateList'])
        ->middleware('can_any:posts/*|posts/create|posts/edit')
        ->name('posts.translate-list');

    Route::get('posts/reference-list', [PostController::class, 'referenceList'])
        ->middleware(['can_any:posts/*|posts/create|posts/edit', 'crossAjax'])
        ->name('posts.reference-list');

    Route::get('posts/create-translate', [PostController::class, 'create'])
        ->middleware(['can_any:posts/*|posts/create', 'crossAjax'])
        ->name('posts.createTranslate');

    Route::get('posts/{type}', [PostController::class, 'index'])
        ->middleware('post_authorize')
        ->name('posts.list');

    Route::middleware('can:pages/*')->group(function () {
        Route::resource('pages', PageController::class)->except(['show']);
        Route::post('pages/image-delete/{id}', [PageController::class, 'imageDelete'])
            ->where('id', '[0-9]+')
            ->name('pages.image-delete');
        Route::get('pages/move/{id}/{type}', [PageController::class, 'move'])
            ->where('type', 'up|down')
            ->where('id', '[0-9]+')
            ->name('pages.move');
    });

    Route::middleware('can:users/*')->group(function () {
        Route::resource('users', UserController::class);
        Route::post('users/delete-thumb/{id}', [UserController::class, 'deleteThumb'])
            ->name('users.delete-image');
    });

    Route::middleware('can:main-info/*')->group(function () {
        Route::post('main-info/delete-logo/{id}/{lang}', [MainInfoController::class, 'deleteLogo'])
            ->name('main-info.delete-logo');
        Route::post('main-info/delete-favicon/{id}/{lang}', [MainInfoController::class, 'deleteFavicon'])
            ->name('main-info.delete-favicon');
        Route::get('main-info', [MainInfoController::class, 'index'])->name('main-info');
        Route::put('main-info', [MainInfoController::class, 'update'])->name('main-info.update');
    });

    Route::resource('configs', ConfigController::class)
        ->middleware('can:configs/*')
        ->except(['show']);

    Route::resource('translations', TranslateController::class)
        ->middleware('can:translations/*')
        ->except(['show']);

    Route::resource('translates', TranslateController::class)
        ->middleware('can:translations/*')
        ->except(['show']);

    Route::get('translates/view', [TranslateController::class, 'view'])
        ->middleware('can:translations/*')
        ->name('translates.view');

    Route::post('ckeditor/image_upload', [CkeditorController::class, 'upload'])->name('ckeupload');
    Route::post('tinymce/addWidget', [TinymceController::class, 'addWidget'])
        ->name('tinymce.addWidget');

    Route::prefix('photos')->middleware('can:photos/*')->name('photos.')->group(function () {
        Route::get('index', [PhotoController::class, 'index'])->name('index');
        Route::get('folder', [PhotoController::class, 'folderIndex'])->name('folder');
        Route::get('trash', [PhotoController::class, 'trashIndex'])->name('trash');
        Route::get('restore', [PhotoController::class, 'restore'])->name('restore');
        Route::post('create', [PhotoController::class, 'create'])->name('create');
        Route::get('update', [PhotoController::class, 'update'])->name('update');
        Route::post('update', [PhotoController::class, 'update'])->name('update.submit');
        Route::post('size', [PhotoController::class, 'size'])->name('size.submit');
        Route::get('create-folder', [PhotoController::class, 'folderCreate'])->name('create-folder');
        Route::get('delete-folder', [PhotoController::class, 'folderDelete'])->name('delete-folder');
        Route::get('update-folder', [PhotoController::class, 'folderUpdate'])->name('update-folder');
        Route::post('update-folder', [PhotoController::class, 'folderUpdate'])->name('update-folder.submit');
        Route::post('create-folder', [PhotoController::class, 'folderCreate'])->name('create-folder.submit');
        Route::post('delete', [PhotoController::class, 'delete'])->name('delete');
        Route::post('soft-delete', [PhotoController::class, 'softDelete'])->name('softDelete');
        Route::post('crop', [PhotoController::class, 'crop'])->name('crop');
        Route::get('sort', [PhotoController::class, 'sort'])->name('sort');
        Route::post('render-gallery', [PhotoController::class, 'renderGallery'])->name('render-gallery');
        Route::post('get-size', [PhotoController::class, 'getSize'])->name('get-size');
    });
});

Route::middleware(['preventBackHistory'])->group(function () {
    Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
    Route::post('login', [LoginController::class, 'login'])->name('login.login');
    Route::get('logout', [LoginController::class, 'logout'])->name('logout');
});
