const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix
    .styles([
        'resources/css/style.css'
    ],
        'public/assets/frontend/css/app.css'
    )
    .scripts([
        'resources/js/main.js',
    ],
        'public/assets/frontend/js/app.js'
    )
    .sourceMaps()
    .copy('resources/images', 'public/assets/frontend/images')
    .version();
