<?php

return [
    'success' => [
        'save'      => 'Changes saved successfully',
        'tag_merge' => ':count tags merged successfully',
        'delete'    => 'Data deleted successfully',
        'restore'   => 'Data restored successfully',
        'publish'   => 'Data published successfully',
        'reject'    => 'Data rejected successfully',
    ],
    'error'   => [
        'cant_remove_this_user' => 'Could`t delete this user',
        'save'                  => 'Failed to save changes',
        'tag_merge'             => 'Unable to merge tags',
        'delete'                => 'Could`t delete data',
        'restore'               => 'Could`t restore data',
        'publish'               => 'Could`t published data',
        'reject'                => 'Could`t rejected data',
        'user_in_post'          => 'This post busy by another editor',
    ]
];
