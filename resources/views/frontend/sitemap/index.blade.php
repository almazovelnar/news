<?php
header('Content-Type: application/xml; charset=utf-8');
echo "<?xml version='1.0' encoding='UTF-8'?>";

/**
 * @var $indexGenerateData \App\Services\Sitemap\IndexItem[]
 */

?>
<sitemapindex xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>
    <?php foreach ($indexGenerateData as $data):?>
    <sitemap>
        <loc><?= $data->location ?></loc>

        <?php if ($data->lastModified !== null): ?>
        <lastmod><?= date('c', $data->lastModified) ?></lastmod>
        <?php endif; ?>
    </sitemap>
    <?php endforeach;?>
</sitemapindex>

