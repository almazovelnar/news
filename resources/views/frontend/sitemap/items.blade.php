<?php
header('Content-Type: application/xml; charset=utf-8');
echo "<?xml version='1.0' encoding='UTF-8'?>";

/**
 * @var $items \App\Services\Sitemap\MapItem[]
 */

?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <?php foreach ($items as $item):?>
    <url>
        <loc><?= $item->location ?></loc>
        <?php if($item->lastModified):?>
        <lastmod><?= date("c", $item->lastModified)?></lastmod>
        <?php endif;?>
        <?php if($item->changeFrequency):?>
        <changefreq><?= $item->changeFrequency ?></changefreq>
        <?php endif;?>
        <?php if($item->priority):?>
        <priority><?= number_format($item->priority, 1, '.', ',')?></priority>
        <?php endif; ?>
    </url>
    <?php endforeach; ?>
</urlset>



