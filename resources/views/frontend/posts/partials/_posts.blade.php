@if(!$posts->isEmpty())
    @foreach($posts as $post)
        <div class="col-12 col-lg-4">
            <div class="post-item with-bg" data-date="{{ $post->published_at }}">
                <div class="post-img">
                    <a href="{{ route('post', ['category' => $post->category_slug, 'slug' => $post->slug]) }}">
                        <img src="{{ get_image($post->image, $post->path, 'large') }}" alt="{{ $post->title }}" class="img-fluid">
                        @if(!empty($post->video) && $post->duration != '00:00')
                            <span class="time">{{ $post->duration }}</span>
                        @endif
                    </a>
                </div>
                <div class="post-content">
                    <h2 class="post-title">
                        <a href="{{ route('post', ['category' => $post->category_slug, 'slug' => $post->slug]) }}">{{ $post->title }}</a>
                    </h2>
                    <div class="post-footer">
                        <a class="post-category" href="{{ route('category', $post->category_slug) }}">{{ $post->category_title }}</a>
                        <div class="post-meta">
                            <span>{{ getParsedDate($post->published_at) }}</span>
                            @if($post->views)
                                <span>@lang('site.post.view', ['views' => $post->views])</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif
