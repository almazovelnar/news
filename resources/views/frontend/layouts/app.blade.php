<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @if(isset($title))
        <title>{{ $title }}</title>
    @else
        <title>@yield('title')</title>
    @endif

    {!! MetaTag::tag('description') !!}
    {!! MetaTag::tag('keywords') !!}
    {!! MetaTag::tag('og:title') !!}
    {!! MetaTag::tag('og:description') !!}
    {!! MetaTag::tag('og:image') !!}

    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    @include('frontend.layouts.icons')
    @include('frontend.layouts.styles')
</head>
<body>
    @include('frontend.layouts.header')

    @yield('content')

    @include('frontend.layouts.footer')
    @include('frontend.layouts.scripts')
</body>
</html>
