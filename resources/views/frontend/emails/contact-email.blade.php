@component('mail::message')

    <p>{{ $data['message'] }}</p>

@endcomponent
