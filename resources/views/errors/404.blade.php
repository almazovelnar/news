@extends('frontend.layouts.app')

@section('title', trans('site.page.404_not_found'))

@section('content')
    <!-- Live content -->
    <div class="content-area">
        <div class="error-area text-center mb-5">
            <div class="error-img text-center">
                @ios
                    <img src="/assets/frontend/img/Comp_2068.png" alt="error" class="img-fluid">
                @elseios
                    <video class="video_404" autoplay loop muted playsinline>
                        <source src="/file/4042.webm" type='video/webm;codecs="vp8, vorbis"'>
                    </video>
                @endios
            </div>

            <h1>@lang('site.layout.404_not_found')</h1>
        </div>
    </div>
@endsection

@section('addJs')
    <script>
        window.setTimeout(function(){
            window.location.href = "{{ route('home') }}";
        }, 10000);
    </script>
@endsection



