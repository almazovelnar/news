<button class="btn btn-success btn-sm confirm-btn"
        @if(!empty($htmlAttributes)) {!! $htmlAttributes !!} @endif
    data-action="{!! $url !!}" type="button" data-confirm="{{ trans('admin.restore_item_confirm') }}"
>
    <i class="fas fa-trash-restore"></i>
</button>&nbsp
