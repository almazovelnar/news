<button class="btn btn-danger btn-sm delete-btn"
        @if(!empty($htmlAttributes)) {!! $htmlAttributes !!} @endif
    data-action="{!! $url !!}" type="button"
>
    <i class="fas fa-trash"></i>
</button>&nbsp
