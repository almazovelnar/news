<button class="btn btn-danger btn-sm post-delete-btn"
        @if(!empty($htmlAttributes)) {!! $htmlAttributes !!} @endif
    data-action="{!! $url !!}" data-id="{{ $id }}" type="button"
>
    <i class="fas fa-trash"></i>
</button>&nbsp
