<a class="btn btn-default btn-sm" target="_blank" href="{!! $url !!}" @if(!empty($htmlAttributes)) {!! $htmlAttributes !!} @endif title="Preview">
    <i class="fas fa-eye"></i>
</a>&nbsp;
