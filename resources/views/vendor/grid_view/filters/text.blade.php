@php
/** @var string $name */
/** @var string $value */
/** @var string $className */
@endphp
<input type="text" class="{{ $className ?? 'form-control' }}" name="filters[{{ $name }}]"
       @if(!empty($htmlAttributes)) {!! $htmlAttributes !!} @endif
       value="{{ $value }}" role="grid-view-filter-item">
