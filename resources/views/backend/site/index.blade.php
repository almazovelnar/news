@extends('backend.layouts.app')

@section('title', trans('admin.dashboard'))

@section('content')
    <div class="row">
        <div class="col-lg-3 col-6">
            <a class="small-box bg-info" href="{{ route('admin.posts.list', ['type' => \App\Enum\PostActionType::PUBLISHED]) }}">
                <div class="inner">
                    <h3>{{$activePostCount}}</h3>
                    <p> @lang('admin.statistic.posts_count')</p>
                </div>
                <div class="icon">
                    <i class="ion ion-bag"></i>
                </div>
            </a>
        </div>

        <div class="col-lg-3 col-6">

            <a class="small-box bg-success" href="{{ route('admin.categories.index') }}">
                <div class="inner">
                    <h3>{{$activeCategoryCount}}</h3>
                    <p>@lang('admin.statistic.categories_count')</p>
                </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
            </a>
        </div>
    </div>
@endsection
