<script src="{{ asset('assets/backend/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/jquery-ui.min.js') }}"></script>
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<script src="{{ asset('assets/backend/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/jquery.knob.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/moment.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/adminlte.min.js') }}"></script>
<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('vendor/tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/fileinput.min.js') }}"></script>
<script src="{{ asset('assets/backend/js/dropzone-min.js') }}"></script>
<script src="/assets/backend/js/admin.js"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function () {
        $('.date-picker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
        });
        $('.datetimepicker').datetimepicker({
            format: 'yyyy-MM-DD HH:mm:ss',
            autoclose: true,
            icons: {
                time: "fa fa-clock"
            }
        });

        if ($('.ckEditor').length > 0) {
            $('.ckEditor').each(function () {
                CKEDITOR.replace($(this).prop('id'), {
                    filebrowserUploadUrl: "{{route('admin.ckeupload', ['_token' => csrf_token() ])}}",
                    filebrowserUploadMethod: 'form',
                    height: 400,
                    removeButtons: 'Image'
                });
            });
        }

        $('.delete-btn').click(function () {
            let res = confirm("{{ trans('admin.delete_item_confirm') }}");
            if (!res) return false;

            let url = $(this).data('action');
            let form = $('#delete-form');
            form.attr('action', url).submit()
        });

        $('.post-delete-btn').click(function () {
            let that = $(this);
            let res = prompt("{{ trans('admin.post_delete_reason') }}");
            if (!res) return false;

            $.ajax({
                url: "{{ route('admin.post.deleteReason') }}",
                data: {'reason': res, 'postId': that.data('id')},
                dataType: 'json',
                success: function (response) {
                    if (response.success === true) {
                        let url = that.data('action');
                        let form = $('#delete-form');
                        form.attr('action', url).submit()
                    }
                }
            })
        });
    })
</script>

@yield('additional_scripts')
@yield('additional_partial_scripts')
