@php
    /** @var \App\Models\User $user */
    $user = auth()->user();
@endphp
<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column mb-5" data-widget="treeview" role="menu" data-accordion="false">
        @canany(['posts/*', 'posts/index'])
            <li class="nav-header">@lang('admin.posts')</li>
            @canany(['posts/*', 'posts/pending'])
                <li class="nav-item">
                    <a href="{{ route('admin.posts.list', ['type' => \App\Enum\PostActionType::PENDING]) }}" class="nav-link">
                        <i class="nav-icon fas fa-list"></i>
                        <p>
                            @lang('admin.menu.posts-pending')
                        </p>
                    </a>
                </li>
            @endcanany
            @canany(['posts/*', 'posts/published'])
                <li class="nav-item">
                    <a href="{{ route('admin.posts.list', ['type' => \App\Enum\PostActionType::PUBLISHED]) }}" class="nav-link">
                        <i class="nav-icon fas fa-list"></i>
                        <p>
                            @lang('admin.menu.posts-published')
                        </p>
                    </a>
                </li>
            @endcanany
            @canany(['posts/*', 'posts/deleted'])
                <li class="nav-item">
                    <a href="{{ route('admin.posts.list', ['type' => \App\Enum\PostActionType::DELETED]) }}" class="nav-link">
                        <i class="nav-icon fas fa-list"></i>
                        <p>
                            @lang('admin.menu.posts-deleted')
                        </p>
                    </a>
                </li>
            @endcanany
        @endif

        <li class="divider"></li>

        @canany(['tags/*', 'tags/index'])
            <li class="nav-item">
                <a href="{{ route('admin.tags.index') }}" class="nav-link">
                    <i class="nav-icon fas fa-tags"></i>
                    <p>
                        @lang('admin.menu.tags')
                    </p>
                </a>
            </li>
        @endcanany

        @canany(['categories/*', 'categories/index'])
            <li class="nav-item">
                <a href="{{ route('admin.categories.index') }}" class="nav-link">
                    <i class="nav-icon fas fa-align-left"></i>
                    <p>
                        @lang('admin.menu.categories')
                    </p>
                </a>
            </li>
        @endcanany

        @canany(['pages/*', 'pages/index'])
            <li class="nav-item">
                <a href="{{ route('admin.pages.index') }}" class="nav-link">
                    <i class="nav-icon fas fa-book"></i>
                    <p>
                        @lang('admin.menu.pages')
                    </p>
                </a>
            </li>
        @endcanany

        @canany(['users/*', 'users/index'])
            <li class="nav-item">
                <a href="{{ route('admin.users.index') }}" class="nav-link">
                    <i class="nav-icon fas fa-user"></i>
                    <p>
                        @lang('admin.menu.users')
                    </p>
                </a>
            </li>
        @endcanany

        <li class="divider"></li>

        @canany(['main-info/*'])
            <li class="nav-item">
                <a href="{{ route('admin.main-info') }}" class="nav-link">
                    <i class="nav-icon fas fa-info-circle"></i>
                    <p>
                        @lang('admin.menu.main-info')
                    </p>
                </a>
            </li>
        @endcanany
        @canany(['configs/*'])
            <li class="nav-item">
                <a href="{{ route('admin.configs.index') }}" class="nav-link">
                    <i class="nav-icon fas fa-cogs"></i>
                    <p>
                        @lang('admin.menu.configs')
                    </p>
                </a>
            </li>
        @endcanany
        @canany(['translations/*'])
            <li class="nav-item">
                <a href="{{ route('admin.translates.view') }}" class="nav-link">
                    <i class="nav-icon fas fa-language"></i>
                    <p>
                        @lang('admin.menu.translations')
                    </p>
                </a>
            </li>
        @endcanany

        @canany(['settings/*'])
            <li class="nav-item">
                    <a href="javascript:void(0);" class="nav-link">
                        <i class="nav-icon fas fa-cogs"></i>
                        <p>
                            @lang('admin.menu.settings')
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin.settings.index') }}" class="nav-link">
                                <p>
                                    <i class="nav-icon">-</i>
                                    @lang('admin.menu.general_settings')
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.logs.index') }}" class="nav-link">
                                <p>
                                    <i class="nav-icon">-</i>
                                    @lang('admin.menu.logs')
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
        @endcan
    </ul>
</nav>
