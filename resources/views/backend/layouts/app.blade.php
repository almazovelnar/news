<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') | Admin Panel</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @include('backend.layouts.styles')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">

        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="{{ route('admin.home') }}" class="nav-link">@lang('admin.home_page')</a>
            </li>
        </ul>

        <ul class="navbar-nav ml-auto">

            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    {{ strtoupper(app()->getLocale()) }}
                </a>
                <div class="dropdown-menu dropdown-menu-sm-right">
                    @foreach(config('app.admin_locales') as $locale)
                        @if($locale != app()->getLocale())
                            <a href="{{ route('admin.change-locale', $locale) }}" class="dropdown-item">{{ strtoupper($locale) }}</a>
                        @endif
                    @endforeach
                </div>
            </li>
            <li class="nav-item dropdown profile-button">
                <a class="nav-link" data-toggle="dropdown" href="javascript:void(0);" aria-expanded="false">
                    <img src="{{ user_image(auth()->user()?->thumb) }}" class="img-circle elevation-2" alt="User Image" style="height: 25px; margin-right: 5px;">
                </a>
                <div class="dropdown-menu dropdown-menu-right" style="left: inherit; right: 0px;">
                    <a href="{{ route('admin.users.show', auth()->id()) }}" class="dropdown-item"> {{ auth()->user()->full_name }}</a>
                    <div class="dropdown-divider"></div>
                    <a data-action="{{ route('admin.logout') }}" href="javascript:void(0);" class="dropdown-item confirm-btn" data-confirm="{{ trans('admin.logout_confirm') }}">{{ trans('admin.logout') }} <i class="fa fa-sign-out-alt"></i></a>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link" data-widget="fullscreen" href="javascript:void(0);" role="button">
                    <i class="fas fa-expand-arrows-alt"></i>
                </a>
            </li>
        </ul>
    </nav>

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{ route('admin.home') }}" class="brand-link">
            <img src="{{ asset('images/AdminLogo.png') }}" class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light">@lang('admin_panel_name')</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">

            @include('backend.layouts.menu')

        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @include('backend.layouts.breadcrumb')

        <button class="open-menu"><img src="/images/menu.svg" alt="icon"></button>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                @include('backend.layouts.notification')
                @yield('content')
            </div><!-- /.container-fluid -->
        </section>
    </div>

    @include('backend.layouts.footer')

    @yield('end_load')
</div>
<!-- ./wrapper -->
    @include('backend.layouts.scripts')
</body>
</html>
