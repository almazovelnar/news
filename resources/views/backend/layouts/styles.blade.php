<link rel="stylesheet" href="{{ asset('assets/backend/css/fontawesome/css/all.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/backend/css/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('assets/backend/css/fileinput.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/backend/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/backend/css/tempusdominus-bootstrap-4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/backend/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/backend/css/adminlte.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/backend/css/OverlayScrollbars.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/backend/css/dropzone.css') }}">
<link rel="stylesheet" href="{{ asset('assets/backend/css/bootstrap-icons.min.css') }}">
<link rel="stylesheet" href="/assets/backend/css/admin.css?v=1.1.6">

@yield('admin_styles')
