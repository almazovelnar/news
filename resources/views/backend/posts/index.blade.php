@extends('backend.layouts.app')

@section('title', trans('admin.posts-list.' . $actionType))

@section('content')
    <div class="mt-3 post-container">
        {!! grid([
            'dataProvider' => $dataProvider,
            'langCreate' => $actionType!=\App\Enum\PostActionType::DELETED,
            'langSelect' => true,
            'hideCreateButton' => true,
            'createUrl' => route('admin.post.create'),
            'dataInfo' => false,
            'strictFilters' => true,
            'itemCounter' => false,
            'rowsPerPage' => 20,
            'columnFields' => [
                [
                    'label' => '#',
                    'attribute' => 'id',
                    'htmlAttributes' => [
                        'width' => '6%'
                    ]
                ],
                [
                    'label' => trans('admin.image'),
                    'value' => function ($data) {
                        return $data->image ? "<img src='" . get_image($data->image, $data->path,'list') . "' style='width:80px'>" : "";
                    },
                    'filter' => false,
                    'format' => 'html'
                ],
                [
                    'label' => trans('admin.title'),
                    'attribute' => 'title',
                    'value' => function ($data) {
                        return "<a href='" . route('admin.post.show', $data->id) . "' class='post-title'>{$data->title}</a>";
                    },
                    'format' => 'html'
                ],
                [
                    'label' => trans('admin.post_category'),
                    'value' => function(\App\Models\Post\Post $post) {
                        return implode(' ',
                             array_map(fn($title) => "<span class='badge badge-info'>{$title}</span>",
                             $post->categories->pluck('title')->toArray())
                        );
                    },
                    'filter' => [
                        'name' => 'category',
                        'class' => Itstructure\GridView\Filters\DropdownFilter::class,
                        'data' => $categoriesList
                    ],
                    'format' => 'html'
                ],
                [
                    'label' => trans('admin.date'),
                    'attribute' => 'published_at',
                    'filter' => [
                        'className' => 'form-control date-picker',
                        'htmlAttributes' => [
                            'autocomplete' => 'off',
                            'onchange' => 'this.form.submit()'
                        ],
                        'class' => \App\Components\Grid\Filters\TextFilter::class,
                    ]
                ],
                'views',
                [
                    'label' => trans('admin.busy'),
                    'value' => function($data){
                        return ($userId = cache()->get('admin-post-view.' . $data->id)) ?
                        "<span class='badge badge-warning'>" . \App\Models\User::find($userId)?->name . "</span>" :
                        null;
                    },
                    'filter' => false,
                    'format' => 'html'
                ],
                [
                    'label' => trans('admin.translates'),
                    'value' => function($data){
                        foreach (\App\Enum\Language::getAnotherList($data->language) as $lang){
                            echo "<a href='" . route('admin.posts.createTranslate', ['id' => $data->id, 'lang' => $lang]) . "'>
                                    <img src='" . asset('images/flag/' . $lang . '.svg') . "' height='15' class='flag-lang " . getPostLangClass($data->id, $data->group_id, $lang)."'>
                                </a> ";
                        }
                    },
                    'filter'=>false,
                    'format'=>'html'
                ],
                [
                    'label' => trans('admin.status.status'),
                    'value' => function($data) use ($actionType) {
                        return view('backend.partials._post_status', ['data' => $data, 'actionType' => $actionType]);
                    },
                    'filter' => $actionType=='published' ? false : [
                        'name' => 'priority',
                        'class' => Itstructure\GridView\Filters\DropdownFilter::class,
                        'data' => \App\Enum\PostPriority::getList()
                    ],
                    'format'=>'html'
                ],
                [
                    'label' => '',
                    'class' => \App\Components\Grid\ActionColumn::class,
                    'htmlAttributes' => [
                        'width' => '120px'
                    ],
                    'actionTypes' => [
                        $actionType != \App\Enum\PostActionType::DELETED ? [
                            'class' => \App\Components\Grid\Actions\PostPreview::class,
                            'url' => function ($data) {
                                return get_preview($data->id);
                            }
                        ] : false,
                        'edit' => function ($data) {
                            return route('admin.post.edit', $data->id);
                        },
                        $actionType == \App\Enum\PostActionType::DELETED ? [
                            'class' => \App\Components\Grid\Actions\Restore::class,
                            'url' => function ($data) {
                                return route('admin.post.restore', $data->id);
                            }
                        ] : [
                            'class' => \App\Components\Grid\Actions\PostDelete::class,
                            'url' => function ($data) {
                                return route('admin.post.destroy', $data->id);
                            },
                            'id' => function ($data) {
                                return $data->id;
                            }
                        ],
                    ]
                ],
            ],
        ]) !!}
    </div>


@endsection
