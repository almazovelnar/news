@extends('backend.layouts.app')

@section('title', $post->title)
@section('content')
    <div class="card post-card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="row mb-2">
                        <div class="text-muted">{{ getParsedDate($post->published_at) }}</div>
                    </div>
                    <div class="row">
                        <span
                            class="alert alert-secondary visible mr-3">{{ $post->isDeleted() ? trans('admin.deleted') : \App\Enum\PostStatus::get($post->status) }}</span>
                        @foreach($post->categories as $category)
                            <span class="alert alert-info visible mr-1">{{ $category->title }}</span>
                        @endforeach
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <div class="post-content mt-2">
                                {!! $post->description !!}
                            </div>
                        </div>
                        <div class="col-4">
                            <div>
                                <img src="{{ get_image($post->path, $post->image, 'large') }}">
                            </div>

                            <div class="counts-block mt-3">
                                <form action="{{ route('admin.post.update-counts', $post->id) }}" method="POST">
                                    @method('PUT')
                                    @csrf
                                    <div class="form-group">
                                        <label for="views">@lang('admin.views')</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">{{ $post->views }} +</span>
                                            </div>
                                            <input type="number" value="{{ old('views', 0) }}" name="views"
                                                   id="views"
                                                   class="form-control @error('views') is-invalid @enderror">
                                        </div>

                                        @error('views')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="likes">@lang('admin.likes')</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">{{ $post->likes }} +</span>
                                            </div>
                                            <input type="number" value="{{ old('likes', 0) }}" name="likes"
                                                   id="likes"
                                                   class="form-control @error('likes') is-invalid @enderror">
                                        </div>

                                        @error('likes')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="dislikes">@lang('admin.dislikes')</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">{{ $post->dislikes }} +</span>
                                            </div>
                                            <input type="number" value="{{ old('dislikes', 0) }}" name="dislikes"
                                                   id="dislikes"
                                                   class="form-control @error('dislikes') is-invalid @enderror">
                                        </div>

                                        @error('dislikes')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <div class="float-right">
                                            <button type="submit" class="btn btn-primary float-right"
                                                    id="formSubmit">@lang('admin.save')</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <style>
        .post-card {
            font-size: 12px;
        }

        .post-card img {
            width: 100%;
        }

        .post-content p {
            margin-bottom: 5px;
        }
    </style>
@endsection
