@extends('backend.layouts.app')

@section('title', $post->title)

@section('content')
    <div class="pb-3">
        <form action="{{ route('admin.post.update', $post->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-8">
                    <div class="card card-primary card-outline card-tabs h-100">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                            <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link  active " id="tab-{{ $post->language }}" data-toggle="pill"
                                       href="#tablink-{{ $post->language }}" role="tab"
                                       aria-controls="tablink-{{ $post->language }}"
                                       aria-selected="true">{{ strtoupper($post->language) }}</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <input type="hidden" name="postId" value="{{$post->id}}">
                            <div class="tab-content" id="custom-tabs-three-tabContent">
                                <div class="tab-pane fade  show active " id="tablink-{{ $post->language }}"
                                     role="tabpanel" aria-labelledby="tab-{{ $post->language }}"
                                     data-lang="{{ $post->language }}">
                                    <div class="form-group">
                                        <label for="title-{{ $post->language }}">@lang('admin.title')</label>

                                        <div class="input-group">
                                            <input type="text" name="title" id="title"
                                                   class="title form-control autoGenerateSlug @error('title') is-invalid @enderror"
                                                   value="{{ old('title', $post->title) }}">
                                        </div>

                                        @error('title')
                                        <span class="text-danger">{{ $message}}</span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="slug-{{ $post->language }}">@lang('admin.slug')</label>

                                        <div class="input-group">
                                            <input type="text" name="slug" id="slug"
                                                   class="slug form-control @error('slug') is-invalid @enderror"
                                                   value="{{ old('slug', $post->slug) }}">
                                        </div>

                                        @error('title')
                                        <span class="text-danger">{{ $message}}</span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        @widget('PhotoManager\PhotoManager', [
                                        'main' => $main,
                                        'photos' => $photos
                                        ])
                                    </div>
                                    <div class="form-group">
                                        <label for="description">@lang('admin.description')</label>
                                        <textarea name="description" id="postContent"
                                                  class="form-control tinymce">{{ old('description', $post->description) }}</textarea>

                                        @error('description')
                                        <span class="text-danger">{{ $message}}</span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="tags-{{ $post->language }}">@lang('admin.tags')</label>
                                        <select name="tags[{{ $post->language }}][]" id="tags-{{ $post->language }}"
                                                class="tags form-control" multiple
                                                data-url="{{ route('admin.posts.tag-list', [], false) }}"
                                                data-placeholder="@lang('admin.all_tags')">
                                            @if(old('tags'))
                                                @foreach(old('tags.' . $post->language) as $tag)
                                                    <option value="{{ $tag }}" selected>{{ $tag }}</option>
                                                @endforeach
                                            @else
                                                @foreach($post->tags->where('language', $post->language) as $tag)
                                                    <option value="{{ $tag->name }}" selected>{{ $tag->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>

                                        @error('tags')
                                        <span class="text-danger">{{ $message}}</span>
                                        @enderror

                                        @error('tags.' . $post->language)
                                        <span class="text-danger">{{ $message}}</span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="translate">@lang('admin.translates')</label>
                                        <select name="translate[]" id="translate"
                                                class="form-control select2-with-results @error('translate') is-invalid @enderror "
                                                multiple data-placeholder="@lang('admin.select_post_translate')" data-url="{{ route('admin.posts.translate-list', [], false) }}">
                                            @foreach($translatedPosts as $translate)
                                                <option
                                                    value="{{ $translate->id }}" {{$translate->group_id== old('translate', $post->group_id) ? 'selected' :''}}>{{ $translate->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="reference">@lang('admin.reference')</label>
                                        <select name="reference[]" id="reference"
                                                class="form-control select2-with-results @error('reference') is-invalid @enderror "
                                                multiple data-placeholder="@lang('admin.select_references')" data-url="{{ route('admin.posts.reference-list', ['id' => $post->id], false) }}">
                                            @foreach($referencePosts as $referencePost)
                                                <option
                                                    value="{{ $referencePost->r_id }}"
                                                    {{$referencePost->id== old('reference', $post->id) ? 'selected' :''}}
                                                >{{ $referencePost->r_title }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>

                                @if($post->meta)
                                    @include('backend.partials._meta_update',['lang'=>'', 'data'=>$post])
                                @else
                                    @include('backend.partials._meta_create',['lang'=>'', 'data'=>$post])
                                @endif
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <div class="col-md-4 ">
                    <div class="card card-secondary h-100 mt-3 mt-md-0">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="category">@lang('admin.post_category')</label>
                                <select name="category[]" id="category"
                                        class="form-control @error('category') is-invalid @enderror sortable-select2 select2"
                                        data-placeholder="@lang('admin.select_post_category')" multiple="multiple">
                                    @foreach(\App\Helpers\SelectListHelper::getItemsWithChosen($categories, $post->categories) as $category)
                                        <option value="{{ $category->id }}" data-id="{{ $category->id }}"
                                                @if(in_array($category->id, old('category', $post->categories->pluck('id')->toArray()))) selected @endif>{{ $category->title }}</option>
                                    @endforeach
                                </select>

                                @error('category')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror

                                @error('category.0')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="priority">@lang('admin.priority.priority')</label>
                                <select name="priority" id="priority" class="form-control">
                                    @foreach(\App\Enum\PostPriority::getListWithColor() as $value => $priority)
                                        <option
                                            value="{{ $value }}" {{$value == old('priority', $post->priority) ? 'selected' :''}}>{{ $priority }}</option>
                                    @endforeach
                                </select>

                                @error('priority')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="type">@lang('admin.type.type')</label>
                                <select name="type" id="type" class="form-control repeater" data-property="type">
                                    @foreach($types as $value => $type)
                                        <option
                                            value="{{ $value }}" {{$value== old('type', $post->type) ? 'selected' :''}}>{{ $type }}</option>
                                    @endforeach
                                </select>

                                @error('type' )
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <!-- Date and time -->
                            <div class="form-group">
                                <label for="date">@lang('admin.date')</label>
                                <div class="input-group date datetimepicker" id="date" data-target-input="nearest">
                                    <input type="text" name="date" class="form-control datetimepicker-input"
                                           data-target="#date" value="{{ old('date', $post->published_at) ?? now() }}"/>
                                    <div class="input-group-append" data-target="#date" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>

                                @error('date')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <div class="float-left flex-left">
                                    <label for="is_slider">@lang('admin.is_slider')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="is_slider" id="is_slider"
                                               @if($post->isSlider()) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="float-right flex-right">
                                    <label for="on_band">@lang('admin.on_band')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="on_band" id="on_band"
                                               @if($post->isOnBand()) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <div class="float-left flex-left">
                                    <label for="chosen">@lang('admin.chosen')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="chosen" id="chosen"
                                               @if($post->isChosen()) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="float-right flex-right">
                                    <label for="fixed">@lang('admin.fixed')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="fixed" id="fixed"
                                               @if($post->isFixed()) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>

                                <div class="clearfix"></div>

                                <div class="fixed_until_block @if($post->isFixed()) active @endif">
                                    {!! html()->input('fixed_until', $post->getFixedUntil(), [
                                        'class' => 'form-control date-picker',
                                        'label' => trans('admin.fixed_until'),
                                        'autocomplete' => 'off'
                                    ]) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="float-left flex-left">
                                    <label for="no_find">@lang('admin.no_find')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="no_find" id="no_find"
                                               @if($post->isNoFind()) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="float-right flex-right">
                                    <label for="no_index">@lang('admin.no_index')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="no_index" id="no_index"
                                               @if($post->isNoIndex()) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <div class="float-left">
                                    <a href="{{ route('admin.post.cancel-form', ['id' => $post->id]) }}" class="btn btn-default">@lang('admin.cancel')</a>
                                </div>
                                <div class="float-right">
                                    <button type="submit" class="btn btn-primary float-right"
                                            id="formSubmit">@lang('admin.save')</button>
                                </div>
                            </div>

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </form>
    </div>
@endsection
