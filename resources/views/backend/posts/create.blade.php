@extends('backend.layouts.app')

@section('title', trans('admin.new_post'))

@section('content')
    <div class="pb-3">
        <form action="{{ route('admin.post.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-8">
                    <div class="card card-primary card-outline card-tabs h-100">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                            <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link  active " id="tab-{{ $lang }}" data-toggle="pill"
                                       href="#tablink-{{ $lang }}" role="tab" aria-controls="tablink-{{ $lang }}"
                                       aria-selected="true">{{ strtoupper($lang) }}</a>
                                </li>
                            </ul>
                        </div>

                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-three-tabContent">
                                <div class="tab-pane fade show active " id="tablink-{{ $lang }}" role="tabpanel"
                                     aria-labelledby="tab-{{ $lang }}" data-lang="{{ $lang }}">

                                    <input type="hidden" name="lang" id="lang" value="{{$lang}}">
                                    <input type="hidden" name="translatedPostId" id="translatedPostId" value="{{$translatedPostId}}">

                                    <div class="form-group">
                                        <label for="title-{{ $lang }}">@lang('admin.title')</label>

                                        <div class="input-group">
                                            <input type="text" name="title" id="title-{{ $lang }}"
                                                   class="title form-control autoGenerateSlug @error('title') is-invalid @enderror"
                                                   value="{{ old('title') }}">
                                        </div>

                                        @error('title')
                                        <span class="text-danger">{{ $message}}</span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="slug-{{ $lang }}">@lang('admin.slug')</label>

                                        <div class="input-group">
                                            <input type="text" name="slug" id="slug-{{ $lang }}"
                                                   class="slug form-control generatedSlug @error('slug') is-invalid @enderror"
                                                   value="{{ old('slug') }}">
                                        </div>

                                        @error('slug')
                                        <span class="text-danger">{{ $message}}</span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        @widget('PhotoManager\PhotoManager', ['photos' => $photos,'main' => $main])

                                        @error('photos')
                                        <span class="text-danger">{{ $message}}</span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="description">@lang('admin.description')</label>
                                        <textarea name="description" id="postContent"
                                                  class="tinymce form-control">{{ old('description') }}</textarea>

                                        @error('description')
                                        <span class="text-danger">{{ $message}}</span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="tags-{{ $lang }}">@lang('admin.tags')</label>
                                        <select name="tags[{{ $lang }}][]" id="tags-{{ $lang }}"
                                                class="tags form-control  @error('tags.' . $lang) is-invalid @enderror"
                                                data-placeholder="@lang('admin.all_tags')"
                                                data-url="{{ route('admin.posts.tag-list') }}"
                                                multiple>
                                            @if(old('tags.' . $lang))
                                                @foreach(old('tags.' . $lang) as $tag)
                                                    <option value="{{ $tag }}" selected>{{ $tag }}</option>
                                                @endforeach
                                            @endif
                                        </select>

                                        @error('tags')
                                        <span class="text-danger">{{ $message}}</span>
                                        @enderror

                                        @error('tags.' . $lang)
                                        <span class="text-danger">{{ $message}}</span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="reference">@lang('admin.reference')</label>
                                        <select name="reference[]" id="reference"
                                                class="form-control select2-with-results @error('reference') is-invalid @enderror "
                                                multiple data-placeholder="@lang('admin.select_references')" data-url="{{ route('admin.posts.reference-list') }}">
                                        </select>
                                    </div>
                                </div>

                                @include('backend.partials._meta_create', ['lang'=>''])
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <div class="col-md-4 ">
                    <div class="card card-secondary h-100 mt-3 mt-md-0">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="category">@lang('admin.post_category')</label>
                                <select name="category" id="category"
                                        class="form-control @error('category') is-invalid @enderror select2"
                                        data-placeholder="@lang('admin.select_post_category')">
                                    <option value="" selected>@lang('admin.select_category')</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}"
                                            {{$category->id == old('category') ? 'selected' :''  }}>{{ $category->title }}</option>
                                    @endforeach
                                </select>

                                @error('category')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror

                                @error('category.0')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="priority">@lang('admin.priority.priority')</label>
                                <select name="priority" id="priority" class="form-control">
                                    @foreach(\App\Enum\PostPriority::getListWithColor() as $value => $priority)
                                        <option value="{{$value}}" {{ old('priority')==$value ? 'selected' : ''}}>{{$priority}}</option>
                                    @endforeach
                                </select>

                                @error('priority')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="type">@lang('admin.type.type')</label>
                                <select name="type" id="type" class="form-control repeater" data-property="type">
                                    @foreach($types as $value => $type)
                                        <option value="{{$value}}" {{ old('type')==$value ? 'selected' : ''}}>{{$type}}</option>
                                    @endforeach
                                </select>

                                @error('type' )
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>

                            <!-- Date and time -->
                            <div class="form-group">
                                <label for="date">@lang('admin.date')</label>
                                <div class="input-group date datetimepicker" id="date" data-target-input="nearest">
                                    <input type="text" name="date" class="form-control datetimepicker-input"
                                           data-target="#date" value="{{ old('date', now()) }}"/>
                                    <div class="input-group-append" data-target="#date" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>

                            <br>
                            <div class="form-group">
                                <div class="float-left flex-left">
                                    <label for="is_slider">@lang('admin.is_slider')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="is_slider" id="is_slider"
                                               @if((bool) old('is_slider') == 1) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="float-right flex-right">
                                    <label for="on_band">@lang('admin.on_band')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="on_band" id="on_band"
                                               @if((bool) old('on_band') == 1) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <div class="float-left flex-left">
                                    <label for="chosen">@lang('admin.chosen')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="chosen" id="chosen"
                                               @if((bool) old('chosen') == 1) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="float-right flex-right">
                                    <label for="fixed">@lang('admin.fixed')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="fixed" id="fixed"
                                               @if((bool) old('fixed') == 1) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                                <div class="fixed_until_block">
                                    <div class="form-group">
                                        {!! html()->input('fixed_until', null, [
                                            'class' => 'form-control date-picker',
                                            'label' => trans('admin.fixed_until'),
                                            'autocomplete' => 'off'
                                        ]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="float-left flex-left">
                                    <label for="no_find">@lang('admin.no_find')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="no_find" id="no_find"
                                               @if((bool) old('no_find') == 1) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="float-right flex-right">
                                    <label for="no_index">@lang('admin.no_index')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="no_index" id="no_index"
                                               @if((bool) old('no_index') == 1) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                                <div class="row float-right">
                                    <div class="float-right">
                                        <button type="submit" class="btn btn-primary float-right"
                                                id="formSubmit">@lang('admin.save')</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>

            </div>
        </form>
    </div>
@endsection

@section('additional_scripts')
    @include('backend.layouts.slug_generate_scripts')
@endsection

