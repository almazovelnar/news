@extends('backend.layouts.app')
@section('title', trans('admin.translates'))

@section('content')
    <div class="mt-3">
        {!!
            grid([
                'dataProvider' => $dataProvider,
                'langSelect' => true,
                'columnFields' => [
                [
                    'label' => trans('admin.translate_group'),
                    'attribute' => 'group'
                ],
                [
                    'label' => trans('admin.translate_key'),
                    'attribute' => 'key'
                ],
                [
                    'label' => trans('admin.translation'),
                    'attribute' => 'text',
                    'value' => function ($model) {
                        return $model->text;
                    }
                ],
                [
                    'label' => '',
                    'class' => \App\Components\Grid\ActionColumn::class,
                    'actionTypes' => [
                        'edit',
                        'delete' => function ($data) {
                            return route('admin.translates.destroy', $data->id);
                        }
                    ]
                ]
            ]
            ])
        !!}
    </div>
@endsection
