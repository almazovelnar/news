@extends('backend.layouts.app')
@section('title', trans('admin.translations'))

@section('content')
    <div class="mt-3">
        <iframe src="{{ route('admin.translations.index') }}" frameborder="0" width="100%"></iframe>
    </div>
    <style>
        iframe {
            height: 80vh;
        }
    </style>
@endsection
