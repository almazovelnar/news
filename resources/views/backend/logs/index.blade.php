@extends('backend.layouts.app')

@section('title', trans('admin.logs'))
{{--<meta http-equiv="refresh" content="10">--}}

@section('content')
    <div class="mt-3">
        {!! grid([
            'dataProvider' => $dataProvider,
            'hideCreateButton' => true,
            'itemCounter' => false,
            'columnFields' => [
                [
                    'label' => trans('admin.user'),
                    'value' => function ($data) {
                        return $data->user->name;
                    },
                    'filter' => false
                ],
                [
                    'label' => trans('admin.log.model'),
                    'value' => function ($data) {
                        return \App\Helpers\LogHelper::getModelName($data->auditable_type);
                    },
                    'filter' => false
                ],
                [
                    'label' => trans('admin.log.model_id'),
                    'value' => function ($data) {
                        return $data->auditable_id;
                    },
                    'filter' => false
                ],
                [
                    'label' => trans('admin.log.message'),
                    'value' => function ($data) {
                        return \App\Helpers\LogHelper::getEventMessage($data->event, $data->auditable_type);
                    },
                    'filter' => false
                ],
                [
                    'label' => 'ip',
                    'attribute' => 'ip_address',
                    'filter' => false,
                    'sort' => false,
                ],
                [
                    'label' => trans('admin.date'),
                    'value' => function ($data) {
                        return getParsedDate($data->created_at);
                    },
                    'filter' => false,
                ],
                [
                    'label' => '',
                    'class' => \App\Components\Grid\ActionColumn::class,
                    'htmlAttributes' => [
                        'width' => '60px'
                    ],
                    'actionTypes' => [
                        'view' => function () {
                            return 'test';
                        }
                    ]
                ]
            ],
        ]) !!}
    </div>
@endsection
