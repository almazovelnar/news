@extends('backend.layouts.app')

@section('title', trans('admin.menu.types'))

@section('content')
    <div class="mt-3">
        {!! grid([
            'dataProvider' => $dataProvider,
            'columnFields' => [
                [
                    'label' => trans('admin.title'),
                    'attribute' => 'title',
                ],

                [
                    'label' => '',
                    'class' => \App\Components\Grid\ActionColumn::class,
                    'actionTypes' => [
                        'edit',
                        'delete' => function ($data) {
                            return route('admin.type.destroy', $data->id);
                        }
                    ]
                ]
            ],
        ]) !!}
    </div>
@endsection
