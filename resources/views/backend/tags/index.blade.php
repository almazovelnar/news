@extends('backend.layouts.app')

@section('title', trans('admin.tags'))

@section('content')
    <div class="mt-3">
        {!!
            grid([
                'dataProvider' => $dataProvider,
                'gridHeader' => view('backend.tags._partial')->render(),
                'columnFields' => [
                [
                    'label' => trans('admin.name'),
                    'attribute' => 'name'
                ],
                [
                    'label' => trans('admin.language'),
                    'attribute' => 'language',
                    'filter' => [
                        'class' => \Itstructure\GridView\Filters\DropdownFilter::class,
                        'data' => array_combine(config('translatable.locales'), config('translatable.locales'))
                    ],
                ],
                [
                    'label' => trans('admin.count'),
                    'attribute' => 'count',
                    'value' => function ($data) {
                        return $data->posts->count();
                    }
                ],
                [
                    'label' => '',
                    'class' => \App\Components\Grid\ActionColumn::class,
                    'actionTypes' => [
                        'edit',
                        'delete' => function ($data) {
                            return route('admin.tags.destroy', $data->id);
                        }
                    ]
                ]
            ]
            ])
        !!}
    </div>
@endsection
