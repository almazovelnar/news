<div class="form-group">
    <label for="value">@lang('admin.value')</label>
    <input type="text" id="value" class="form-control @error('value') is-invalid @enderror" name="value"
           value="{{ $setting->value }}">
    @error('value')
    <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>
