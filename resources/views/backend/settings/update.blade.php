@extends('backend.layouts.app')

@section('title', trans('admin.settings.' . $setting->name))

@section('content')
    <div class="mt-3">
        <div>
            <form action="{{ route('admin.settings.update', $setting->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-primary card-outline card-tabs">
                            <div class="card-body">
                                @include('backend.settings.type.' . $setting->type)
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <input type="submit" value="@lang('admin.save')" class="btn btn-success float-right">
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
