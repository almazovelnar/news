@extends('backend.layouts.app')

@section('title', trans('admin.settings.tags'))

@section('content')
    <div class="mt-3">
        <div>
            <form action="{{ route('admin.settings.chosen-tags.update', ['id' => $setting->id]) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-md-8">

                        <div class="card card-primary card-outline card-tabs">
                            <div class="card-header p-0 pt-1 border-bottom-0">
                                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                    @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                        <li class="nav-item">
                                            <a class="nav-link @if($key == 0) active @endif" id="tab-{{ $lang }}" data-toggle="pill" href="#tablink-{{ $lang }}" role="tab" aria-controls="tablink-{{ $lang }}" aria-selected="true">{{ strtoupper($lang) }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-three-tabContent">
                                    @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $key => $lang)
                                        <div class="tab-pane fade @if($key == 0) show active @endif" id="tablink-{{ $lang }}" role="tabpanel" aria-labelledby="tab-{{ $lang }}" data-lang="{{ $lang }}">
                                            <div class="form-group">
                                                <label for="tags-{{ $lang }}">@lang('admin.tags')</label>
                                                <select name="tags[{{ $lang }}][]" id="tags-{{ $lang }}"
                                                        class="form-control tags sortable-select2 @error('tags.' . $lang) is-invalid @enderror"
                                                        data-placeholder="@lang('admin.all_tags')"
                                                        data-url="{{ route('admin.posts.tag-list') }}"
                                                        multiple>
                                                    @foreach($setting->getValue($lang) as $tagId => $tagName)
                                                        <option value="{{ $tagId }}" data-id="{{ $tagId }}" selected>{{ $tagName }}</option>
                                                    @endforeach
                                                </select>

                                                @error('tags')
                                                <span class="text-danger">{{ $message}}</span>
                                                @enderror

                                                @error('tags.' . $lang)
                                                <span class="text-danger">{{ $message}}</span>
                                                @enderror
                                            </div>

                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-secondary">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <input type="submit" value="@lang('admin.save')" class="btn btn-success float-right">
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
