@extends('backend.layouts.app')

@section('title', trans('admin.settings.settings'))

@section('content')
    <div class="mt-3">
        {!!
            grid([
                'dataProvider' => $dataProvider,
                'hideCreateButton' => true,
                'rowsPerPage' => 50,
                'columnFields' => [
                [
                    'label' => 'Name',
                    'value' => function ($data) {
                        return trans('admin.settings.' . $data->name);
                    },
                    'filter' => false
                ],
                [
                    'label' => 'Value',
                    'value' => function ($data) {
                        if ($data->type == \App\Enum\SettingType::JSON) {
                            return '';
                        }

                        return $data->value;
                    },
                    'filter' => false
                ],
                [
                    'label' => '',
                    'class' => \App\Components\Grid\ActionColumn::class,
                    'actionTypes' => [
                        'edit'
                    ],
                    'htmlAttributes' => [
                        'width' => '120px'
                    ]
                ]
            ]
            ])
        !!}
    </div>
@endsection
