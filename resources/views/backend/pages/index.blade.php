@extends('backend.layouts.app')

@section('title', trans('admin.pages'))

@section('content')
    <div class="mt-3">
        {!! grid([
            'dataProvider' => $dataProvider,
            'columnFields' => [
                [
                    'format' => 'html',
                    'value' => function (\App\Models\Page\Page $model) {
                        return '<a title="' . trans('admin.move_up') . '" href="' . route('admin.pages.move', ['id' => $model->id, 'type' => 'up']) . '"><span class="fa fa-arrow-up"></span></a> ' .
                                '<a title="' . trans('admin.move_down') . '" href="' . route('admin.pages.move', ['id' => $model->id, 'type' => 'down']) . '"><span class="fa fa-arrow-down"></span></a>';
                    },
                    'filter' => false,
                    'htmlAttributes' => ['style' => 'width:100px']
                ],
                [
                    'label' => trans('admin.title'),
                    'attribute' => 'title',
                    'value' => function (\App\Models\Page\Page $model) {
                        return $model->translate(config('app.admin_content_locale'))->title;
                    }
                ],
                [
                    'label' => trans('admin.status.status'),
                    'attribute' => 'status',
                    'value' => function($data) {
                        return \App\Enum\Status::get($data->status);
                    },
                    'filter' => [
                        'class' => Itstructure\GridView\Filters\DropdownFilter::class,
                        'data' => \App\Enum\Status::getList()
                    ]
                ],
                [
                    'label' => '',
                    'class' => \App\Components\Grid\ActionColumn::class,
                    'actionTypes' => [
                        'edit',
                        'delete' => function ($data) {
                            return route('admin.pages.destroy', $data->id);
                        }
                    ]
                ]
            ],
        ]) !!}
    </div>
@endsection
