@extends('backend.layouts.app')

@section('title', trans('admin.new_category'))

@section('content')
    <div>
        <form action="{{ route('admin.categories.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-8">

                    <div class="card card-primary card-outline card-tabs">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                            <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="tab-{{ $lang }}"
                                           data-toggle="pill" href="#tablink-{{ $lang }}" role="tab"
                                           aria-controls="tablink-{{ $lang }}"
                                           aria-selected="true">{{ strtoupper($lang) }}</a>
                                    </li>
                            </ul>
                        </div>

                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-three-tabContent">
                                <div class="tab-pane fade show active"
                                         id="tablink-{{ $lang }}" role="tabpanel" aria-labelledby="tab-{{ $lang }}" data-lang="{{ $lang }}">

                                    <input type="hidden" name="lang" id="lang" value="{{$lang}}">
                                    <input type="hidden" name="translatedCategoryId" id="translatedCategoryId" value="{{$translatedCategoryId}}">

                                    <div class="form-group">
                                            <label for="title-{{ $lang }}">@lang('admin.title')</label>
                                            <input type="text" name="title" id="title-{{ $lang }}"
                                                   class="form-control autoGenerateSlug @error('title') is-invalid @enderror"
                                                   value="{{ old('title') }}">

                                            @error('title')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="slug-{{ $lang }}">@lang('admin.slug')</label>
                                            <input type="text" name="slug" id="slug-{{ $lang }}"
                                                   class="generatedSlug form-control @error('slug') is-invalid @enderror"
                                                   value="{{ old('slug') }}">
                                            @error('slug')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="description-{{ $lang }}">@lang('admin.description')</label>
                                            <textarea name="description" id="description-{{ $lang }}"
                                                      class="ckEditor form-control"
                                                      rows="4">{{ old('description') }}</textarea>
                                        </div>

                                        @include('backend.partials._meta_create', ['lang' => ''])
                                    </div>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-secondary">
                        <div class="card-body">

                            <div class="form-group">
                                <label for="image">@lang('admin.image')</label>
                                <input name="image" id="input-id" type="file" class="file_uploader"
                                       data-preview-file-type="text">
                                @error('image')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <br>

                            <div class="form-group">
                                <div class="float-left">
                                    <label for="chosen">@lang('admin.chosen_category')</label>
                                    <label class="switch">
                                        <input type="checkbox" id="chosen" name="chosen"
                                               @if((bool) old('chosen')==1) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="float-right">
                                    <label for="status">@lang('admin.status.status')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="status" id="status"
                                               @if((bool) old('status', true)==1) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                                <div class="float-left">
                                    <label for="on_menu">@lang('admin.on_menu')</label>
                                    <label class="switch">
                                        <input type="checkbox" id="on_menu" name="on_menu"
                                               @if((bool) old('on_menu')==1) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <input type="submit" value="@lang('admin.save')"
                                           class="btn btn-success float-right">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </form>
    </div>
@endsection
@section('additional_scripts')
    @include('backend.layouts.slug_generate_scripts')
@endsection
