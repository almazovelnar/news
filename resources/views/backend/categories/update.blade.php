@extends('backend.layouts.app')

@section('title', $category->title)

@php $lang = $category->language @endphp

@section('content')
    <div>
        <form action="{{ route('admin.categories.update', $category->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-8">

                    <div class="card card-primary card-outline card-tabs">
                        <div class="card-header p-0 pt-1 border-bottom-0">
                            <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="tab-{{ $lang }}" data-toggle="pill" href="#tablink-{{ $lang }}" role="tab" aria-controls="tablink-{{ $lang }}" aria-selected="true">{{ strtoupper($lang) }}</a>
                                    </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="custom-tabs-three-tabContent">
                                <div class="tab-pane fade show active" id="tablink-{{ $lang }}" role="tabpanel" aria-labelledby="tab-{{ $lang }}" data-lang="{{$lang}}">
                                        <div class="form-group">
                                            <label for="title-{{ $lang }}">@lang('admin.title')</label>
                                            <input type="text" value="{{ $category->title }}" name="title" id="title-{{ $lang }}" class="form-control autoGenerateSlug @error('title') is-invalid @enderror">
                                            @error('title')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="slug-{{ $lang }}">@lang('admin.slug')</label>
                                            <input type="text" value="{{ $category->slug }}" name="slug" id="slug-{{ $lang }}" class="form-control @error('slug') is-invalid @enderror">
                                            @error('slug')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="description-{{ $lang }}">@lang('admin.description')</label>
                                            <textarea name="description" id="description-{{ $lang }}" class="ckEditor form-control @error('slug') is-invalid @enderror" rows="4">{!! $category->description !!}</textarea>

                                            @error('description')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="translate">@lang('admin.translates')</label>
                                            <select name="translate[]" id="translateCategory"
                                                    class="form-control select2-with-results @error('translate') is-invalid @enderror "
                                                    multiple
                                                    data-url="{{ route('admin.categories.translate-list') }}"
                                                    data-placeholder="@lang('admin.select_post_translate')">
                                                @foreach($translatedCategories as $translate)
                                                    <option value="{{ $translate->id }}" {{$translate->group_id== old('translate', $category->group_id) ? 'selected' :''}}>{{ $translate->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        @if($category->meta)
                                            @include('backend.partials._meta_update',['lang'=>'', 'data'=> $category])
                                        @else
                                            @include('backend.partials._meta_create',['lang'=>'', 'data'=> $category])
                                        @endif
                                    </div>
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-secondary">
                        <div class="card-body">

                            <div class="form-group">
                                <label for="image">@lang('admin.image')</label>
                                <input
                                    value="{{ $category->image ? asset('storage/categories/' . $category->image) : null }}"
                                    name="image" id="image" type="file"
                                    class="file_uploader  @error('image') is-invalid @enderror"
                                    data-preview-file-type="image">
                                @error('image')
                                <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                            <br>

                            <div class="form-group">
                                <div class="float-left">
                                    <label for="chosen">@lang('admin.chosen_category')</label>
                                    <label class="switch">
                                        <input type="checkbox" id="chosen"  name="chosen" @if($category->isChosen()) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="float-right">
                                    <label for="status">@lang('admin.status.status')</label>
                                    <label class="switch switch-success">
                                        <input type="checkbox" name="status" id="status" @if($category->getStatus()) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                                <div class="float-left">
                                    <label for="on_menu">@lang('admin.on_menu')</label>
                                    <label class="switch">
                                        <input type="checkbox" id="on_menu"  name="on_menu" @if($category->isOnMenu()) checked @endif>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <input type="submit" value="@lang('admin.save')" class="btn btn-success float-right">
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </form>
    </div>
@endsection
