@extends('backend.layouts.app')

@section('title', $user->name)

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <div class="text-muted">
                    <p><strong>ID: </strong>{{ $user->id }}</p>
                </div>

                <div class="text-muted">
                    <p><strong>@lang('admin.email'):</strong></p>
                    {{ $user->email }}
                </div>
                <br>

                <div class="text-muted">
                    <p><strong>@lang('admin.status.status')</strong></p>
                    {{ \App\Enum\Status::get($user->status) }}
                </div>
                <br>
                <div>
                    <a href="{{ route('admin.users.edit', $user->id) }}" class="btn btn-info">@lang('admin.edit')</a>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-body -->
</div>
@endsection
