@extends('backend.layouts.app')

@section('title', trans('admin.users'))

@section('content')
    <div class="mt-3">
        {!!
            grid([
                'dataProvider' => $dataProvider,
                'columnFields' => [
                [
                    'label' => trans('admin.name'),
                    'attribute' => 'name',
                    'sort' => false
                ],
                [
                    'label' => trans('admin.email'),
                    'attribute' => 'email',
                    'sort' => false
                ],
                [
                    'label' => trans('admin.role.role'),
                    'value' => function ($data) {
                        return trans('admin.role.' . $data->getRole()->name);
                    },
                    'filter' => false
                ],
                [
                    'label' => trans('admin.status.status'),
                    'attribute' => 'status',
                    'value' => function($data) {
                        return \App\Enum\Status::get($data->status);
                    },
                    'filter' => [
                        'class' => Itstructure\GridView\Filters\DropdownFilter::class,
                        'data' => \App\Enum\Status::getList()
                    ],
                    'sort' => false
                ],
                [
                    'label' => '',
                    'class' => \App\Components\Grid\ActionColumn::class,
                    'actionTypes' => [
                        'edit',
                        'delete' => function ($data) {
                            return route('admin.users.destroy', $data->id);
                        }
                    ]
                ]
            ]
            ])
        !!}
    </div>
@endsection
