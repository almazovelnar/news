@if($actionType == \App\Enum\PostActionType::PENDING)
    <button type='button'
            data-action="{{route('admin.post.publish', ['id' => $data->id])}}"
            class="confirm-btn btn btn-sm btn-{{ \App\Enum\PostPriority::getLabel($data->priority) }}" data-confirm="{{ trans('admin.publish_item_confirm') }}">
        {{ \App\Enum\PostStatus::get($data->status) }}
    </button>
@else
    <button type='button'
             data-action="{{route('admin.post.reject', ['id' => $data->id])}}"
            class="@if($actionType != 'deleted') confirm-btn  @endif btn btn-sm btn-default" data-confirm="{{ trans('admin.reject_item_confirm') }}">
        {{ $data->isToBePublished() ? trans('admin.status.to_be_published') : \App\Enum\PostStatus::get($data->status) }}
    </button>
@endif
