@php
    /**
     * @var \Illuminate\Pagination\LengthAwarePaginator $paginator
     * @var array[] $elements
     */
@endphp
@if ($paginator->hasPages())
    <div class="pagination-block cl-a">
        <div class="pagination paginationAjax" id="pagination">
            {{-- Pagination Elements --}}
            @foreach($elements as $key => $element)
                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <a class="link-page active">{{ $page }}</a>
                        @else
                            <a class="link-page" href="{{ $paginator->url($page) }}">{{ $page }}</a>
                        @endif
                    @endforeach
                    {{-- "Three Dots" Separator --}}
                @elseif(is_string($element))
                    <a class="link-page disabled">{{ $element }}</a>
                @endif
            @endforeach
            <div class="clearfix"></div>
        </div>
    </div>
@endif

<style>
    a.link-page {
        border: 1px solid grey;
        color: #333;
        height: 35px;
        width: 35px;
        display: flex;
        align-items: center;
        justify-content: center;
        margin-left: 10px;
    }
    a.link-page.active {
        background: #17a2b8bd;
        color: #ffffff;
    }
</style>

