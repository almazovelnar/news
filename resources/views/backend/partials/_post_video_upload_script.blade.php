<style>
    .dz-image img {
        width: 100%;
        height: 100%;
    }

    .dropzone.dz-started .dz-message {
        display: block !important;
    }

    .dropzone {
        border: 2px dashed #028AF4 !important;;
    }

    .dropzone .dz-preview.dz-complete .dz-success-mark {
        opacity: 1;
    }

    .dropzone .dz-preview.dz-error .dz-success-mark {
        opacity: 0;
    }

    .dropzone .dz-preview .dz-error-message {
        top: 144px;
    }

    .post-video {
        display: none;
        width: 100%;
        height: 250px;
    }

    .post-video.active {
        display: block;
    }
</style>

<script>
    $('.dropzone').each(function () {
        var that = $(this);
        newDropzone({
            dropzone: that,
            url: that.data('upload-url'),
            deleteUrl: that.data('delete-url'),
            paramName: that.data('param-name'),
            previewEl: that.data('preview-el')
        })
    })

    function newDropzone(opt) {
        opt.videoFilename = '';
        // Dropzone has been added as a global variable.
        opt.dropzone.dropzone({
            url: opt.url,
            paramName: opt.paramName,
            maxFiles: 1,
            maxFilesize: 1024,
            acceptedFiles: 'video/*',
            uploadMultiple: false,
            addRemoveLinks: true,
            dictRemoveFile: '{{ trans('admin.remove_video_file') }}',
            dictCancelUpload: '{{ trans('admin.cancel_upload_video_file') }}',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            removedfile: function (file) {
                if (this.options.dictRemoveFile) {
                    {{--return Dropzone.confirm("{{ trans('admin.confirm_remove_video') }}", function () {--}}
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type: 'POST',
                            url: opt.deleteUrl,
                            data: {filename: opt.videoFilename},
                            success: function () {
                                var $previewEl = $("#" + opt.previewEl);
                                $previewEl.removeClass('active');
                                $('input[name="' + opt.paramName + '"]').val('');
                                $('input[name="duration"]').val('');
                                $('input[name="size"]').val('');
                            },
                            error: function (e) {
                                console.log(e);
                            }
                        });

                        var fileRef;
                        return (fileRef = file.previewElement) != null ?
                            fileRef.parentNode.removeChild(file.previewElement) : void 0;
                    // });
                }
            },
            success: function (file, response) {
                if (response.success) {
                    var resFile = response.file;
                    opt.videoFilename = resFile['name'];
                    $('input[name="' + opt.paramName + '"]').val(resFile['name']);
                    $('input[name="duration"]').val(resFile['duration']);
                    $('input[name="size"]').val(resFile['size']);
                    var $previewEl = $("#" + opt.previewEl);
                    if ($previewEl[0].canPlayType(file.type) !== "no") {
                        var fileURL = URL.createObjectURL(file);

                        $($previewEl).one('loadeddata', function () {
                            URL.revokeObjectURL(fileURL);
                        });
                        $previewEl[0].src = fileURL;

                        $previewEl.addClass('active')
                    }
                }
            },
            error: function (file, response) {
                var message;
                if ($.type(response) === "string") {
                    message = response;
                } else {
                    message = response.message;
                }
                file.previewElement.classList.add("dz-error");
                _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
                _results = [];
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    node = _ref[_i];
                    _results.push(node.textContent = message);
                }
                return _results;
            },
            maxfilesexceeded: function (file) {
                this.removeAllFiles();
                this.addFile(file);
            }
        });
    }
</script>
