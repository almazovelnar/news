@section('end_load')
    <!-- Modal Photomanager -->
    <div class="modal fade modal-photo-manager" id="photo-manager" role="dialog"
         data-url="{{ route('admin.photos.index') }}">
        <div class="modal-dialog modal-simple modal-center">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="title-search">
                        <h4 class="modal-title">@lang('photo.photo_manager')</h4>

                        <div class="toggle-search">
                            <i class="icon md-search"></i>
                        </div>
                    </div>

                    <button type="button" class="close" data-dismiss="modal">
                        <i class="icon md-close"></i>
                    </button>
                </div>
                <!-- Modal header -->
                <div class="modal-body">
                    <div class="files-search">
                        <form action="{{ route('admin.photos.index') }}" method="GET" id="files-search-form"
                              enctype="multipart/form-data">
                            <div class="form-group">
                                <div class="input-group">
                                    <input title="search"
                                           type="text"
                                           class="form-control"
                                           placeholder="Search"
                                    />

                                    <span class="input-group-btn">
                                    <button class="btn btn-primary"
                                            type="submit">@lang('photo.search')</button>
                                </span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- Files search -->
                    <div class="modalInner">
                        <div class="folders-block">
                            <div class="folders" data-url="{{ route('admin.photos.index') }}">
                                <a class="folder selected rootFolder" data-id="0">
                                    <img src="{{ asset('images/folder.png') }}" alt="@lang('photo.all_photos')">
                                    <span style="font-weight: bolder;">@lang('photo.all_photos')</span>
                                </a>
                                <a class="folder commonFolder" data-id="-1">
                                    <img src="{{ asset('images/folder.png') }}" alt="@lang('photo.common_folder')">
                                    <span style="font-weight: bolder;">@lang('photo.common_folder')</span>
                                </a>
                                @if(Auth::user()->isAdmin())
                                    <a class="folder deletedImagesFolder" data-id="-2" data-url="{{ route('admin.photos.trash') }}">
                                        <img src="{{ asset('images/trash-folder.png') }}" alt="@lang('photo.deleted_images_folder')">
                                        <span style="font-weight: bolder;">@lang('photo.deleted_images_folder')</span>
                                    </a>
                                @endif
                                <hr>
                                @foreach($folders as $folder)
                                    @include('backend.photos._folder', ['folder' => $folder])
                                @endforeach
                            </div>
                        </div>
                        <div class="files-list pt-10"></div>
                    </div>
                </div>
                <!-- Modal body -->

                <div class="modal-footer">

                    <div class="manage-folder active">
                        <button class="btn btn-success btnCreateFolder" data-url="{{ route('admin.photos.create-folder') }}" type="button" data-upload="@lang('photo.create_folder')" data-uploading="@lang('photo.creating')">
                            <i class="fa fa-plus"></i>
                        </button>

                        <div class="folder-buttons">
                            <button class="btn btn-warning btnUpdateFolder" data-url="{{ route('admin.photos.update-folder') }}" type="button" data-upload="@lang('photo.update_folder')" data-uploading="@lang('photo.updating')">
                                <i class="fa fa-edit"></i>
                            </button>

                            <button class="btn btn-danger btnDeleteFolder" data-url="{{ route('admin.photos.delete-folder') }}" type="button" data-upload="@lang('photo.delete_folder')" data-uploading="@lang('photo.deleting')">
                                <i class="fa fa-trash"></i>
                            </button>
                        </div>

                        <div class="upload-images">
                            <input
                                type="file"
                                id="upload-images-field"
                                multiple="multiple"
                                accept=".jpg,.jpeg,.png,.gif,.jfif,.webp"
                                hidden
                                data-url="{{ route('admin.photos.create') }}"
                                data-with-logo="0"
                            />

                            <button class="btn btn-primary btnUploadWithoutWatermark" type="button" data-upload="@lang('photo.upload_photo')" data-uploading="@lang('photo.uploading')">
                                <i class="fa fa-image"></i> @lang('photo.upload_photo')
                            </button>
                        </div>

                        <div class="manage-images">

                            <?php if ($mode == 'gallery'): ?>
                            <button type="button"
                                    class="btn btn-info btn-add-image"
                                    data-url="{{ route('admin.photos.render-gallery') }}"
                            >@lang('photo.add')</button>
                            <?php else: ?>
                            <button type="button" class="btn btn-info btn-attach-image">
                                @lang('photo.add')
                            </button>
                            <?php endif; ?>
                            <button type="button"
                                    class="btn btn-danger btn-delete-image"
                                    data-url="{{ route('admin.photos.softDelete') }}"
                            >@lang('photo.delete')</button>
                        </div>
                    </div>

                    <div class="manage-trash">
                        <button type="button" class="btn btn-info btn-restore-image"
                                data-url="{{ route('admin.photos.restore') }}">
                            @lang('photo.restore')
                        </button>
                        <button type="button"
                                class="btn btn-danger btn-force-delete-image"
                                data-url="{{ route('admin.photos.delete') }}"
                        >@lang('photo.delete')</button>
                    </div>
                </div>
                <!-- Modal footer -->
            </div>
        </div>
    </div>
    <!-- End photomanager -->

    <!-- Modal Cropper -->
    <div class="modal fade" id="photo-crop" role="dialog">
        <div class="modal-dialog modal-simple modal-center">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">@lang('photo.crop_photo')</h4>

                    <button type="button" class="close" data-dismiss="modal">
                        <i class="icon md-close"></i>
                    </button>
                </div>

                <div class="modal-body">
                    <div>
                        <img alt="photo" class="img-fluid img-to-crop" src=""/>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="btn btn-info btn-save-crop" type="button"
                            data-url="{{ route('admin.photos.crop') }}"
                    >@lang('photo.save')</button>
                    <button class="btn btn-secondary" data-dismiss="modal">@lang('photo.cancel')</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Cropper -->

    <!-- Modal Update -->
    <div class="modal fade" id="photo-update" role="dialog">
        <div class="modal-dialog modal-simple modal-center">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <!-- End Update -->

    @if($mode == 'gallery')
        <!-- Modal Update -->
        <div class="modal fade modal-slide-in-right" id="photo-sizes" role="dialog">
            <div class="modal-dialog modal-simple modal-center">
                <div class="modal-content">
                    <form class="photo-sizes-form" action="{{ route('admin.photos.size.submit') }}" method="POST">
                        <div class="modal-header">
                            <h4 class="modal-title">@lang('photo.size_prices')</h4>

                            <button type="button" class="close" data-dismiss="modal">
                                <i class="icon md-close"></i>
                            </button>
                        </div>

                        <div class="modal-body">
                            <input name="id" id="size-image-id" type="hidden" value="" required>
                            <div class="size-group"></div>
                        </div>

                        <div class="modal-footer">
                            <button class="btn btn-success" type="submit">@lang('photo.save')</button>
                            <button class="btn btn-secondary"
                                    data-dismiss="modal">@lang('photo.cancel')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- End Update -->
    @endif
@endsection
