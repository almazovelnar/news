<ul class="lang">
    @if(count($locales = LaravelLocalization::getSupportedLanguagesKeys()) > 1)
        @foreach($locales as $locale)
            @if(app()->getLocale() != $locale)
                @if(isset($translatedLinks))
                    @if(array_key_exists($locale, $translatedLinks))
                        <li><a href="{{ $translatedLinks[$locale] }}">{{ strtoupper($locale) }}</a></li>
                    @else
                        <li><a href="{{ LaravelLocalization::localizeURL(url()->route('home'), $locale) }}">{{ strtoupper($locale) }}</a></li>
                    @endif
                @else
                    <li><a href="{{ LaravelLocalization::localizeURL(url()->current(), $locale) }}">{{ strtoupper($locale) }}</a></li>
                @endif
            @endif
        @endforeach
    @endif
</ul>
