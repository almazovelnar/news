<?php

namespace Database\Factories\Post;

use App\Models\Category;
use App\Models\Post\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    protected $model = Post::class;

    public function definition()
    {
        $lang = $this->faker->randomElement(\LaravelLocalization::getSupportedLanguagesKeys());

        return [
            'language'     => $lang,
            'category_id'  => Category::query()->where('language', $lang)->inRandomOrder()->first()->id,
            'chosen'       => (rand(1, 10) < 3),
            'user_id'      => 1,
            'title'        => ($title = $this->faker->realTextBetween(15, 35)),
            'description'  => ($description = $this->faker->realText(2000)),
            'views'        => $this->faker->numberBetween(0, 1000),
            'likes'        => $this->faker->numberBetween(0, 1000),
            'dislikes'     => $this->faker->numberBetween(0, 1000),
            'image'        => 'test.jpg',
            'status'       => true,
            'is_published' => true,
            'is_slider'    => $this->faker->boolean,
            'meta'         => [
                'title'       => $title,
                'description' => $description,
                'keywords'    => $title,
            ],
            'published_at' => $this->faker->dateTime,
            'slug'         => \Str::slug($title, '-', $lang),
        ];
    }
}
