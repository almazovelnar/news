<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{
    protected $model = Category::class;

    public function definition()
    {
        return [
            'chosen'      => rand(0, 1),
            'language'    => $this->faker->randomElement(\LaravelLocalization::getSupportedLanguagesKeys()),
            'status'      => true,
            'title'       => ($title = $this->faker->text(20)),
            'description' => ($description = $this->faker->realText(2000)),
            'slug'        => \Str::slug($title),
            'meta'        => [
                'title'       => $title,
                'description' => $description,
                'keywords'    => $title,
            ],
        ];
    }
}
