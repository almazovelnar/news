<?php

namespace Database\Seeders;

use App\Models\Config\Config;
use Illuminate\Database\Seeder;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list = $this->getConfigList();

        foreach ($list as $key => $item) {
            if (Config::query()->where('param', $key)->exists()) {
                continue;
            }

            $model = new Config();
            $model->param = $key;
            $model->value = '';
            $model->default = '#';
            $model->label = $item;
            $model->type = 'string';

            $model->save();
        }
    }

    public function getConfigList()
    {
        return [
            'FACEBOOK'           => 'Facebook profile link',
            'WHATSAPP'           => 'Whatsapp number',
            'TELEGRAM'           => 'Telegram page',
            'YOUTUBE'            => 'Youtube profile link',
            'TWITTER'            => 'Twitter profile link',
            'INSTAGRAM'          => 'Instagram page link',
            'VKONTAKTE'          => 'Vkontakte profile link',
            'TIKTOK'             => 'Tiktok profile link',
        ];
    }
}
