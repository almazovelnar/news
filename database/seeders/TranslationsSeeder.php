<?php

namespace Database\Seeders;

use App\Models\LanguageLine\Translate;
use Arr;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TranslationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach ($this->getTranslations() as $group => $translates) {
            foreach ($translates as $prefix => $translations) {
                $withPrefix = Arr::isAssoc($translations);
                if (!$withPrefix) {
                    $translations = [$translations];
                }

                foreach ($translations as $key => $text) {
                    $translate = DB::table('language_lines')
                        ->where('group', $group)
                        ->where('key', $withPrefix ? "$prefix.$key" : $prefix);


                    $text = json_encode([
                        'az' => $text[0],
                        'ru' => $text[1]
                    ]);

                    if (!$translate->first()) {
                        $translate->insert([
                            'group' => $group,
                            'key'   => $withPrefix ? "$prefix.$key" : $prefix,
                            'text'  => $text
                        ]);
                    }

                }
            }
        }

        // clear cache
        Translate::first()->save();
    }

    private function getTranslations(): array
    {
        return [
            'site' => [
                'search_not_found_result' => ['Axtarişa uyğun nəticə tapılmadı', 'По вашему запросу ничего не найдено'],
                'layout'                  => [
                    'all_rights_reserved'   => ['Bütün hüquqlar qorunur :year', 'Все права защищены :year'],
                    'for_partnership'       => ['Əməkdaşlıq üçün', 'Для партнерствар'],
                    'for_general_inquiries' => ['Ümumi sorğular üçün', 'По общим вопросам'],
                    'load_more'             => ['Daha çox', 'Больше'],
                    'full_story'            => ['Tam versiya', 'Полная версия'],
                    'live'                  => ['Canlı', 'Прямой эфир'],
                    'search'                => ['Axtarış', 'Поиск'],
                    'whatsapp'              => ['Whatsapp', 'Whatsapp'],
                    'telegram'              => ['Telegram', 'Telegram'],
                    'subscribe'             => ['Abunə ol', 'Подписаться'],
                    'select_source'         => ['Mənbəni seçin', 'Выбрать источник'],
                    '404_not_found'         => ['Səhifə tapılmadı', 'Страница не найдена'],
                    'footer_text'           => [
                        'Baku.tv 2018-ci ilin fevral ayında yaranıb. Baku.Tv ölkədə və dünyada baş verən ən aktual və maraqlı hadisələr barədə operativ videosujetlər hazırlayaraq auditoriyaya təqdim edir.',
                        'Baku.tv 2018-ci ilin fevral ayında yaranıb. Baku.Tv ölkədə və dünyada baş verən ən aktual və maraqlı hadisələr barədə operativ videosujetlər hazırlayaraq auditoriyaya təqdim edir.'
                    ],
                ],
                'sidebar'                 => [
                    'home'     => ['Ana səhifə', 'Главная'],
                    'live'     => ['Canlı', 'Прямой эфир'],
                    'channels' => ['Verlişlər', 'Каналы'],
                    'shorts'   => ['Shorts', 'Shorts'],
                    'show'     => ['Göstər', 'Показать'],
                    'hide'     => ['Gizlət', 'Спрятать'],
                    'explore'  => ['Kəşf et', 'Исследовать'],
                ],
                'home'                    => [
                    'shorts'     => ['Shorts', 'Shorts'],
                    'top_search' => ['Ən çox axtarılan', 'Топ запросы'],
                    'main_posts' => ['Önəmli xəbərlər', 'Важные новости'],
                ],
                'post'                    => [
                    'view'            => [':views görüntü', ':views просмотров'],
                    'choose_player'   => ['Pleyeri seçin:', 'Выбрать плеер:'],
                    'baku_tv_player'  => ['baku tv', 'baku tv'],
                    'youtube_player'  => ['youtube', 'youtube'],
                    'chosen_posts'    => ['Seçilmişlər', 'Выбранные'],
                    'link_copied'     => ['Kopyalandı', 'Скопировано'],
                    'read_more'       => ['Ətraflı', 'Подробнее'],
                    'posts_not_found' => ['Heç bir xəbər tapılmadı', 'Новостей не найдено'],
                    'comments'        => ['Şərhlər', 'Комментарии'],
                ],
                'static'                  => [
                    'contact_us' => ['Bizimlə əlaqə', 'Свяжитесь с нами'],
                ],
                'contact'                 => [
                    'name'                      => ['Ad, Soyad', 'Имя, Фамилия'],
                    'name_placeholder'          => ['Ad, Soyad', 'Имя, Фамилия'],
                    'email'                     => ['E-mail', 'Эл.адрес'],
                    'email_placeholder'         => ['E-mail', 'Эл.адрес'],
                    'message'                   => ['Mesaj', 'Сообщение'],
                    'message_placeholder'       => ['Mesaj', 'Сообщение'],
                    'send'                      => ['Göndər', 'Отправить'],
                    'request_sent_successfully' => ['Sorğu uğurla göndərildi', 'Запрос успешно отправлен'],
                    'request_couldnt_send'      => ['Sorğu göndərilə bilmədi', 'При отправке запроса произошла ошибка'],
                ],
                'page'                    => [
                    'chosens'       => ['Seçilmiş xəbərlər', 'Выбранные новости'],
                    '404_not_found' => ['Səhifə tapılmadı', 'Страница не найдена'],
                    'shorts'        => ['Shorts', 'Shorts'],
                    'explore'       => ['Kəşf et', 'Выбранные новости'],
                    'live'          => ['Canlı', 'Прямой эфир'],
                ],
                'validation'              => [
                    'email'    => ['E-mail ünvanı düzgün deyil', 'Адрес эл. почты неверен'],
                    'required' => ['Bu sahəni doldurun', 'Заполните поле'],
                    'min'      => ['Ən az :min simvoldan ibarət olmalıdır', 'Должно быть не менее :min символов'],
                ],
            ],
        ];
    }
}
