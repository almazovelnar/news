<?php

namespace Database\Seeders;

use App\Models\MainInfo\MainInfo;
use Illuminate\Database\Seeder;

class MainInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!MainInfo::first()) {

            $model = new MainInfo();

            $data = [];
            foreach (\LaravelLocalization::getSupportedLanguagesKeys() as $lang) {
                $data[$lang]['title'] = 'Title';
                $data[$lang]['description'] = 'Description';
                $data[$lang]['address'] = 'Address';
                $data[$lang]['meta'] = [
                    'title' => 'Title',
                    'description' => 'Description',
                    'keywords' => 'Keywords'
                ];
            }

            $model->name  = 'Name';
            $model->email = ['test@email.com'];
            $model->phone = ['+994000000000'];

            $model->fill($data);
            $model->save();
        }
    }
}
