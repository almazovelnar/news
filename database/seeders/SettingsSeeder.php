<?php

namespace Database\Seeders;

use App\Enum\SettingType;
use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getList() as $type => $items) {
            foreach ($items as $key => $value) {
                if (Setting::query()->where('name', $key)->exists()) {
                    continue;
                }
                $model = new Setting();

                $model->name = $key;
                $model->type = $type;
                if ($type == SettingType::JSON) {
                    $model->value = json_encode($value);
                } else {
                    $model->value = $value;
                }


                $model->save();
            }
        }
    }

    public function getList()
    {
        return [
            SettingType::NUMERIC => [
                'categories_count'          => 8,
                'chosen_tags_count'         => 8,
                'home_page_posts_count'     => 12,
                'search_page_posts_count'   => 12,
                'category_page_posts_count' => 12,
                'tag_page_posts_count'      => 12,
                'slider_posts_count'        => 5,
                'fixed_posts_count'         => 1,
                'related_posts_count'       => 6,
                'breaking_posts_count'      => 6,
            ],
            SettingType::JSON    => [
                'chosen_tags' => [],
            ]
        ];
    }
}
