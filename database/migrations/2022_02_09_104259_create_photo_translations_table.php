<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhotoTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photo_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('photo_id')->index();
            $table->string('locale', 16)->index();
            $table->string('title')->nullable();
            $table->string('description')->nullable();

            $table->unique(['photo_id', 'locale']);
            $table->foreign('photo_id')
                ->references('id')
                ->on('photos')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photo_translations');
    }
}
