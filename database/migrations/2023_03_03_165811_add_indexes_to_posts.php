<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexesToPosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->index('chosen');
            $table->index('is_slider');
            $table->index('on_band');
            $table->index('is_published');
            $table->index('title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropIndex('chosen');
            $table->dropIndex('is_slider');
            $table->dropIndex('on_band');
            $table->dropIndex('is_published');
            $table->dropIndex('title');
        });
    }
}
