<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     *
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('group_id')->nullable();
            $table->char('language', 16);
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('user_id');
            $table->enum('type', array_keys(\App\Enum\PostType::getList()))
                ->default(\App\Enum\PostType::NORMAL);
            $table->enum('priority', array_keys(\App\Enum\PostPriority::getList()))
                ->default(\App\Enum\PostPriority::NORMAL);
            $table->string('title');
            $table->text('body')->nullable();
            $table->longText('description')->nullable();
            $table->string('slug');
            $table->json('meta')->nullable();
            $table->json('marked_words')->nullable();
            $table->string('path')->nullable();
            $table->string('image')->nullable();
            $table->tinyInteger('status')->default(\App\Enum\PostStatus::ACTIVE);
            $table->boolean('is_published')->default(false);
            $table->boolean('is_slider')->default(false);
            $table->boolean('on_band')->default(true);
            $table->boolean('chosen')->default(false);
            $table->unsignedInteger('views')->default(0);
            $table->unsignedInteger('likes')->default(0);
            $table->unsignedInteger('dislikes')->default(0);
            $table->string('audio_url')->nullable();
            $table->unsignedInteger('lock_version')->default(0);
            $table->dateTime('published_at')->useCurrent();
            $table->timestamps();
            $table->softDeletes();

            // indexes
            $table->index('id');
            $table->index('language');
            $table->index('group_id');
            $table->index('slug');
            $table->index('published_at');

            $table->foreign('category_id', 'fk_posts_category_id')
                ->references('id')
                ->on('categories')
                ->restrictOnDelete();

            $table->foreign('user_id', 'fk_posts_user_id')
                ->references('id')
                ->on('users')
                ->restrictOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
