<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('config_id')->index();
            $table->string('locale', 16)->index();
            $table->text('value')->nullable();

            $table->unique(['config_id', 'locale']);
            $table->foreign('config_id')
                ->references('id')
                ->on('configs')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_translations');
    }
}
