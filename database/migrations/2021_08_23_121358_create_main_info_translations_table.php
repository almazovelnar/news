<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMainInfoTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_info_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('main_info_id')->index();
            $table->string('locale', 16)->index();
            $table->string('title');
            $table->text('description');
            $table->text('address')->nullable();
            $table->json('meta')->nullable();

            $table->unique(['main_info_id', 'locale']);
            $table->foreign('main_info_id')
                ->references('id')
                ->on('main_info')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_info');
    }
}
