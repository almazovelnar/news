<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Kalnoy\Nestedset\NestedSet;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('old_id')->nullable();
            $table->unsignedInteger(NestedSet::LFT)->default(0);
            $table->unsignedInteger(NestedSet::RGT)->default(0);
            $table->unsignedInteger(NestedSet::PARENT_ID)->nullable();
            $table->unsignedInteger('group_id')->nullable();
            $table->char('language', 16);
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('slug');
            $table->json('meta')->nullable();
            $table->json('seo')->nullable();
            $table->string('image')->nullable();
            $table->string('template')->nullable();
            $table->boolean('status')->default(\App\Enum\Status::ACTIVE);
            $table->boolean('on_menu')->default(false);
            $table->boolean('chosen')->default(false);
            $table->boolean('hidden')->default(false);
            $table->dateTime('deleted_at')->nullable();
            $table->timestamps();


            $table->index('title');
            $table->index('slug');
            $table->index('language');
            $table->index(NestedSet::getDefaultColumns());
            $table->foreign('parent_id', 'category_parent_key')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
