<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameColumnsTranslationsKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('config_translations', function (Blueprint $table) {
            $table->renameColumn('locale', 'language');
        });
        Schema::table('main_info_translations', function (Blueprint $table) {
            $table->renameColumn('locale', 'language');
        });
        Schema::table('page_translations', function (Blueprint $table) {
            $table->renameColumn('locale', 'language');
        });
        Schema::table('photo_translations', function (Blueprint $table) {
            $table->renameColumn('locale', 'language');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('config_translations', function (Blueprint $table) {
            $table->renameColumn('language', 'locale');
        });
        Schema::table('main_info_translations', function (Blueprint $table) {
            $table->renameColumn('language', 'locale');
        });
        Schema::table('page_translations', function (Blueprint $table) {
            $table->renameColumn('language', 'locale');
        });
        Schema::table('photo_translations', function (Blueprint $table) {
            $table->renameColumn('language', 'locale');
        });
    }
}
