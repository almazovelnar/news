<?php

use Carbon\Carbon;

if (!function_exists('grid')) {
    function grid($config)
    {
        return (new \App\Components\Grid\Grid($config))->render();
    }
}

if (!function_exists('get_preview')) {
    function get_preview(int $id)
    {
        return "https://oxu.az/post/preview/" . $id;
    }
}

if (!function_exists('site_info')) {
    function site_info(string $key, $default = null)
    {
        return \App\Components\Information::instance()->get($key, $default);
    }
}

if (!function_exists('site_config')) {
    function site_config(string $key)
    {
        return \App\Components\Config::instance()->get($key);
    }
}

if (!function_exists('getParsedDate')) {
    function getParsedDate($date, string $format = 'd F Y, H:i'): string
    {
        if (is_a($date, Carbon::class)) {
            return $date->translatedFormat($format);
        }
        return Carbon::parse($date)->translatedFormat($format);
    }
}

if (!function_exists('get_image')) {
    function get_image($filename, $folder = '', ?string $size = null): string
    {
        $storageOriginal = Storage::disk('postPhoto');
        $storage  = Storage::disk('public');

        if (empty($filename)) {
            return asset('images/thunk.png');
        }

        if (!is_file($original = $storageOriginal->path(($folder ? $folder . '/' : '') . $filename))) {
            $original = $storage->path('old/video/pic/' . ($folder ? $folder . '/' : '') . $filename);
        }

        $sizes    = config('image-sizes.template');
        $path     = $storage->path('posts/' . $size . ($folder ?  '/' . $folder . '/' : '/'));

        $width = $sizes[$size]['w'];
        $height = $sizes[$size]['h'];

        if (!is_file($original)) {
            return asset('images/thunk.png');
        }

        if (!is_file($path . $filename)) {
            ini_set('memory_limit', '512M');

            if (!is_dir($path)) {
                $storage->createDir('posts/' . $size . '/' . $folder);
            }

            try {
                \Intervention\Image\Facades\Image::make($original)
                    ->fit($width, $height, function ($constraint) {
                        $constraint->upsize();
                    })
                    ->save($path . $filename);
            } catch (\Exception $exception) {
                return asset('images/thunk.svg');
            }
        }

        return asset('storage/posts/' . ($size == 'o' ? '' : $size . '/') . ($folder ?  $folder . '/' : '') . $filename);
    }
}

if (!function_exists('getPostLangClass')) {
    function getPostLangClass($postId, $group_id, $lang): ?string
    {
        $query = \App\Models\Post\Post::query()
            ->where('language', $lang);
            $query->where('group_id', $group_id);
            $query->where('group_id','!=', null);
        if (!$query->exists()) {
            return 'notHave';
        }
        return null;
    }
}

if (!function_exists('getCategoryLangClass')) {
    function getCategoryLangClass($catId, $group_id, $lang): ?string
    {
        if (!\App\Models\Category::query()
            ->where('group_id', $group_id)
            ->where('language', $lang)->exists()) {

            return 'notHave';
        }

        return null;
    }
}

if (!function_exists('user_image')) {
    function user_image($filename): string
    {
        if (empty($filename)) {
            return asset('images/user.png');
        }

        return asset('storage/users/' . $filename);
    }
}

if (!function_exists('get_logo')) {
    function get_logo(?string $mode = 'default'): string
    {
        if (empty($logo = site_info('logo'))) {
            return asset('assets/frontend/img/logo.svg');
        }

        return asset('storage/main-info/' . $logo);
    }
}

if (!function_exists('get_favicon')) {
    function get_favicon(): string
    {
        if (empty($favicon = site_info('favicon'))) {
            return asset('assets/frontend/img/favicon.png');
        }

        return asset('storage/main-info/' . $favicon);
    }
}

if (!function_exists('get_colored_title')) {
    function get_colored_title($title, $markedWords = null): string
    {
        if (empty($markedWords)) {
            return $title;
        }

        foreach ($markedWords as $markedWord){
            $title = str_replace($markedWord, "<i class='marked-word'>{$markedWord}</i>", $title);
        }
        return $title;
    }
}

if (!function_exists('get_views_counter_image')) {
    function get_views_counter_image(int $id, string $lang): string
    {
        return asset('/images/views.png') . "?id={$id}&lang={$lang}&x=" . microtime(true);
    }
}

if (!function_exists('get_stream')) {
    function get_stream(): string
    {
        return "https://rtmp.baku.tv/streams/bakutv.m3u8";
    }
}
