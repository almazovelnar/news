$(document).ready(function () {
    let currentUrl = window.location.protocol + '//' + window.location.host + window.location.pathname;

    $(".nav-treeview .nav-link, .nav-link").each(function () {
        if (currentUrl.indexOf($(this).attr('href')) !== -1) {
            $(this).addClass('active');
            $(this).parent().parent().parent().addClass('menu-is-opening menu-open');
        }
    });

    $('#langSelector').change(function () {
        let currentLangValue = $(this).val();
        let w = window.location;
        w.href = w.pathname + '?filters%5Blanguage%5D=' + currentLangValue;
    });

    $('.inputCreator').click(function() {
        let makeType = $(this).data('make-type');
        let makeName = $(this).data('make-name');
        let newElement = $('<input />', {
            type: makeType,
            name: makeName,
            class: 'form-control new-input'
        }).appendTo($('.' + makeType + '-inputs'));
    });

    $(".v-block").on("mouseover", function() {
        $(this).find('video').get(0).play();

    }).on('mouseleave', function() {
        $(this).find('video').get(0).load();
    });

    $('.select-title').on('click', function () {
        var text = "";
        if (window.getSelection) {
            text = window.getSelection().toString();
        } else if (document.selection && document.selection.type != "Control") {
            text = document.selection.createRange().text;
        }
        text = text.trim();
        if (text.length > 0){
            $('.marked_words').append("<option value='"+text+"' selected>"+text+"</option>");
            $('.marked_words').trigger('change');
        }
    })

    tinymce.init({
        language: 'az',
        plugins: 'link addWidget addText addGallery image',
        selector: '.tinymce',
        height: 600,
        toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | outdent indent | link | addWidget | addText | addGallery | image',
    	mobile: {
          menubar: true
        }
    });

    $('.open-menu').on('click', function () {
        $('.main-sidebar').toggleClass('open')
    })

    $('#youtube').on('keyup', function (e) {
        e.preventDefault();
        let id = $(this).val();
        let matches = id.match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#");

        if (id.length >= 7) {
            $('#videoBlock').html('<iframe width="100%" height="300" src="https://www.youtube.com/embed/' + (matches!=null ? matches[0] : id) + '?controls=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>')
        } else {
            $('#videoBlock').html('')
        }
    });

    $('#fixed').on('change', function (e) {
        $('.fixed_until_block').toggleClass('active')
    })

    if ($(".file_uploader").length) {
        $(".file_uploader").each(function () {
            let fileUploader = $(this)
            let fileUploaderOptions = {};
            if ((fileUploaderValue = fileUploader.attr('value'))){
                fileUploaderOptions.initialPreview = fileUploaderValue
            }
            fileUploaderOptions.initialPreviewShowDelete = false;
            if ((fileUploaderDeleteThumbUrl = fileUploader.data('delete')) ){
                fileUploaderOptions.deleteUrl = fileUploaderDeleteThumbUrl
                fileUploaderOptions.initialPreviewShowDelete = true;
            }
            fileUploaderOptions.showRemove = false;
            fileUploaderOptions.maxFileCount= 1;
            fileUploaderOptions.initialPreviewDownloadUrl = fileUploaderValue;
            fileUploaderOptions.initialPreviewAsData = true;
            fileUploaderOptions.overwriteInitial = false;
            fileUploaderOptions.showUpload = false;
            fileUploaderOptions.showCancel = false;
            fileUploaderOptions.showClose = false;
            fileUploaderOptions.initialPreviewConfig = [{
                type: fileUploader.data('preview-file-type'),
                filetype: fileUploader.data('preview-file-type')=='video' ? 'video/mp4' : false
            },]

            fileUploader.fileinput(fileUploaderOptions);
        })
    }


    /*------------------  Select 2 ---------------------*/

    $('select:not(#langSelector)').on('change', function () {
        $('#grid_view_filters_form').submit()
    })

    $('.select2').select2({
        tags: true,
        sortResults: data => data.sort((a, b) => a.text.localeCompare(b.text)),
        templateSelection: function (data, container) {
            if (data.element) {
                $(container).attr('data-id',data.id);
            }

            return data.text;
        }
    })

    // if ($('.tags').length) {
    //     $('.tags').each(function () {
    //         let tagContainer = $(this);
    //         tagContainer.select2({
    //             tags: true,
    //             allowClear: true,
    //             minimumInputLength: 3,
    //             createTag: function (params) {
    //                 var term = $.trim(params.term);
    //
    //                 if (term === '') {
    //                     return null;
    //                 }
    //
    //                 return {
    //                     id: term,
    //                     text: term.replace('New tag: ', ''),
    //                     newTag: true
    //                 }
    //             },
    //             insertTag: function (data, tag) {
    //                 tag.text = 'New tag: ' + tag.text;
    //                 data.push(tag);
    //             },
    //             ajax: {
    //                 type: 'GET',
    //                 url: tagContainer.data('url'),
    //                 dataType: 'json',
    //                 data: function (params) {
    //                     return {lang: tagContainer.closest('.tab-pane').data('lang'), q: params.term};
    //                 },
    //                 processResults: function (data) {
    //                     return {
    //                         results: $.map(data.results, function (item) {
    //                             return {
    //                                 text: item.name,
    //                                 id: item.name
    //                             }
    //                         })
    //                     };
    //                 },
    //             }
    //         });
    //     })
    // }

    if ($('.tags').length) {
        $('.tags').each(function () {
            let tagContainer = $(this);
            tagContainer.select2({
                tags: true,
                allowClear: true,
                minimumInputLength: 3,
                templateSelection: function (data, container) {
                    if (data.element) {
                        $(container).attr('data-id',data.id);
                    }

                    return data.text;
                },
                createTag: function (params) {
                    var term = $.trim(params.term);

                    if (term === '') {
                        return null;
                    }

                    return {
                        id: term,
                        text: term.replace('New tag: ', ''),
                        newTag: true
                    }
                },
                insertTag: function (data, tag) {
                    tag.text = 'New tag: ' + tag.text;
                    data.push(tag);
                },
                ajax: {
                    type: 'GET',
                    url: tagContainer.data('url'),
                    dataType: 'json',
                    data: function (params) {
                        return {lang: tagContainer.closest('.tab-pane').data('lang'), q: params.term};
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data.results, function (item) {
                                return {
                                    id: item.id,
                                    text: item.name
                                }
                            })
                        };
                    },
                }
            });
        })
    }

    if ($('.sortable-select2').length) {
        $('.sortable-select2').each(function () {
            var that = $(this);

            that.parent().find(" ul.select2-selection__rendered").sortable({
                containment: 'parent',
                update: function () {
                    that.parent().find("ul.select2-selection__rendered").children("li[title]").each(function(i, obj){
                        var element = that.children("option[value="+$(obj).data('id')+"]");
                        var parent = element.parent();
                        element.detach();
                        parent.append(element);
                    });
                }
            });

        })
    }

    $("select").on("select2:select", function (evt) {
        var $element = $(evt.params.data.element);

        $element.detach();
        $(this).append($element);
    });

    $('.confirm-btn').click(function () {
        let confirmMessage = $(this).data('confirm');
        let res = confirm(confirmMessage);
        if (!res) return false;

        location.href = $(this).data('action');
    });

    if ($('.select2-with-results').length) {
        $('.select2-with-results').each(function () {
            var that = $(this);
            that.select2({
                tags: false,
                allowClear: true,
                minimumInputLength: 3,
                ajax: {
                    type: 'GET',
                    url: that.data('url'),
                    dataType: 'json',
                    data: function (params) {
                        return {lang: $('.tab-pane').data('lang'), q: params.term, chosen: that.val()};
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data.results, function (item) {
                                return {
                                    text: item.title,
                                    id: item.id
                                }
                            })
                        };
                    },
                }
            });
        })
    }

    let _alert = $(".alert:not(.visible)");
    if (_alert.length) {
        _alert.animate({opacity: 1.0}, 3000).fadeOut("slow");
    }
})
