const managerModal = $('#photo-manager');
const uploadedImagesField = $('#upload-images-field');
const filesList = $('.files-list');
const searchForm = $('#files-search-form');

const sizeModal = $('#photo-sizes');

const updateModal = $('#photo-update');

const cropModal = $('#photo-crop');

let searchQuery = '';

managerModal.find('.toggle-search').on('click', function () {
    // noinspection JSValidateTypes
    managerModal.find('.files-search').slideToggle(150);
});

managerModal.find('.upload-images .btn').on('click', function () {
    uploadedImagesField.data('with-logo', $(this).hasClass('btnUploadWithWatermark'));

    managerModal.find('.upload-images input').trigger('click');
});

managerModal.on('show.bs.modal', function () {
    loadList(managerModal.data('url'));
});

// Infinite scroll
let ajaxRunning = false;
let scrollSum = 0;
let photosFinished = false;
$("#photo-manager .modal-body .files-list").on("scroll", function () {
    scrollSum = Math.ceil($(this).scrollTop() + $(this).innerHeight());
    if (scrollSum >= $(this)[0].scrollHeight && !ajaxRunning && !photosFinished) {
        request(
            managerModal.data('url'),
            'GET',
            {
                id: filesList.find('.files-list-item:last').attr('data-id'),
                q: searchQuery,
                folderId: $('.folder.selected').data('id')
            },
            res => {
                ajaxRunning = false;
                if (res === '') {
                    photosFinished = true;
                }
                filesList.append(res.photos);
            },
            {
                beforeSend: () => ajaxRunning = true
            }
        );
    }
});

filesList.on('click', '.files-list-item', function (e) {
    var _self = $(this);

    if (e.shiftKey) {
        selectRangeOfImages(_self);
    } else if (e.ctrlKey) {
        selectMultipleImages(_self);
    } else {
        selectSingleImage(_self);
    }
});
$(document).on('click', function (e) {
    if ($(e.target).is('.files-list')) {
        unselectAllImages();
    }
});

function selectSingleImage(element) {
    $('.files-list .files-list-item').removeClass('selected');
    element.addClass('selected');
}

function selectMultipleImages(element) {
    element.addClass('selected');
}

function selectRangeOfImages(element) {
    element.addClass('selected');

    if ($('.files-list-item.selected').length >= 2) {
        var firstSelect = $('.files-list-item.selected').get(0);
        $(firstSelect).nextUntil('.files-list-item.selected').addClass('selected');
    }
}

function unselectAllImages() {
    $('.files-list .files-list-item').removeClass('selected');
}

filesList.on('dblclick', '.files-list-item', function (e) {
    $.magnificPopup.open({
        items: {
            src: $(this).find('img').attr('src').replace('_194', '')
        },
        type: 'image'
    }, 0);
});

$(document).on('click', '.trigger-update', function () {
    request($(this).attr('data-url'), 'GET', null, res => {
        updateModal.find('.modal-content').html(res);
        updateModal.modal('show');
        $('.datetimepicker').datetimepicker({
            icons: {time: 'far fa-clock'},
            format: 'YYYY-MM-DD HH:mm:ss',
            pickDate: false,
            pickSeconds: false,
            pick12HourFormat: false,
            ignoreReadonly: true
        });
    });
});

$(document).on('click', '.btnCreateFolder', function () {
    request($(this).attr('data-url'), 'GET', null, res => {
        updateModal.find('.modal-content').html(res);
        updateModal.modal('show');
    });
});

$(document).on('click', '.btnUpdateFolder', function () {
    let $folder = $('.folder.selected');
    request($(this).attr('data-url'), 'GET', {id: $folder.data('id')}, res => {
        updateModal.find('.modal-content').html(res);
        updateModal.modal('show');
    });
});

$(document).on('click', '.btnDeleteFolder', function () {
    if (!confirm('Вы действительно хотите удалить выбранную папку?')) {
        return false;
    }
    let $folder = $('.folder.selected');
    request($(this).data('url'), 'GET', {id: $folder.data('id')}, res => {
        if (res.success) {
            $folder.remove();
            $('.folder.rootFolder').trigger('click');
        } else {
            showMessage('В папке содержатся файлы', 'error');
        }
    });
});

$(document).on('click', '.trigger-crop', function () {
    const item = $(this).closest('.files-list-item');

    // noinspection HtmlRequiredAltAttribute,RequiredAttributes
    const img = $('<img>', {
        'class': 'img-default img-to-crop',
        'src': item.attr('data-src') + '?' + (new Date()).getTime(),
        'data-id': item.attr('data-id')
    });

    cropModal.find('.modal-body > div').empty().append(img).end().modal('show');

    cropModal.on('shown.bs.modal', function () {
        img.cropper({
            checkOrientation: false,
            checkCrossOrigin: false,
            resizable: true,
            responsive: true,
            aspectRatio: 16 / 9
        });
    })
});

$(document).on('click', '.btn-save-crop', function () {
    const img = cropModal.find('.img-default');
    const cropData = img.cropper('getData');
    const btn = $(this);

    request(
        btn.data('url') + '?' + $.param({id: img.attr('data-id')}),
        'POST',
        {
            'x': cropData['x'],
            'y': cropData['y'],
            'width': cropData['width'],
            'height': cropData['height']
        },
        res => {
            if (res.success) {
                const old = $('.files-list-item[data-id=' + img.attr('data-id') + '] img');
                old.attr('src', old.attr('src') + '?' + (new Date()).getTime());
                cropModal.modal('hide');
            } else {
                // noinspection JSValidateTypes
                alert(res.errors);
            }
        },
        {
            dataType: 'json',
            async: true,
            cache: false,
        }
    );
});

$(document).on('click', '.btn-add-image', function () {
    const selected = $('.files-list-item.selected');
    const initialInp = $('input#initial');
    const btn = $(this);

    if (selected.length < 1) {
        return false;
    }

    const data = [];
    for (let item of selected) {
        let photo = $(item);
        if ($('.post-images input[value="' + photo.attr('data-id') + '"]').length <= 0) {
            data.push({
                name: initialInp.attr('name'),
                mainName: initialInp.attr('data-main'),
                value: photo.attr('data-id'),
                path: photo.find('img').attr('data-path'),
                filename: photo.find('img').attr('data-filename'),
            });
        } else {
            managerModal.modal('hide');
        }
    }

    request(btn.data('url'), 'POST', {data}, res => {
        $('.post-images').append(res);
        const main = $(document).find('.main-radio');
        if (!main.is(':checked')) {
            main.first().prop('checked', true);
        }
        managerModal.modal('hide');
    });
});

$(document).on('click', '.btn-attach-image', function () {
    const selected = $('.selected');

    if (selected.length < 1) return false;

    const photo = selected.length > 1 ? selected[0] : selected;

    $('#photo-input').val(photo.attr('data-filename'));
    $('#photo-img').attr('src', photo.attr('data-src'));
    managerModal.modal('hide');
});

$(document).on('submit', '.update-photo-form', function (e) {
    e.preventDefault();
    const form = $(this);
    request(
        form.attr('action'),
        'POST',
        form.serializeArray(),
        (res) => {
            if (res.success) {
                updateModal.modal('hide');
            } else {
            }
        }
    );
});

$(document).on('submit', '.photo-sizes-form', function (e) {
    e.preventDefault();
    const form = $(this);
    request(
        form.attr('action'),
        'POST',
        form.serializeArray(),
        (res) => {
            if (res.success) {
                sizeModal.modal('hide');
            } else {
            }
        }
    );
});

$(document).on('submit', '.create-folder-form', function (e) {
    e.preventDefault();
    const form = $(this);
    request(
        form.attr('action'),
        'POST',
        form.serializeArray(),
        (res) => {
            if (res.success) {
                updateModal.modal('hide');
                $('.folders .rootFolder').siblings('hr').after(res.folder);
            } else {
            }
        }
    );
});

$(document).on('submit', '.update-folder-form', function (e) {
    e.preventDefault();
    const form = $(this);
    request(
        form.attr('action'),
        'POST',
        form.serializeArray(),
        (res) => {
            if (res.success) {
                updateModal.modal('hide');
                let tFolder = $(res.folder).addClass('selected');
                $('.folders .folder.selected').replaceWith(tFolder);
            } else {
            }
        }
    );
});

$(document).on('click', '.folder:not(.deletedImagesFolder)', function (e) {
    e.preventDefault();
    var that = $(this);
    var folderId = that.data('id');

    request(
        that.closest('.folders').data('url'),
        'GET',
        {folderId: folderId},
        (res) => {
            if (res.success) {
                that.addClass('selected').siblings().removeClass('selected');
                $('.files-list').html(res.photos);


                $('.modal-footer .manage-trash').removeClass('active');
                $('.modal-footer .manage-folder').addClass('active');

                if (folderId > 0) {
                    $('.modal-footer .folder-buttons').addClass('active');
                } else {
                    $('.modal-footer .folder-buttons').removeClass('active');
                }
            } else {
            }
        }
    );
});

$(document).on('click', '.deletedImagesFolder', function (e) {
    e.preventDefault();
    var that = $(this);

    request(
        that.data('url'),
        'GET',
        {},
        (res) => {
            if (res.success) {
                that.addClass('selected').siblings().removeClass('selected');
                $('.files-list').html(res.photos);

                $('.modal-footer .manage-folder').removeClass('active');
                $('.modal-footer .manage-trash').addClass('active');
            } else {
            }
        }
    );
});

$(document).on('click', '.btn-delete-image', function () {
    const selected = filesList.find('.selected');
    const btn = $(this);

    if (selected.length < 1) return false;

    if (!confirm('Вы действительно хотите удалить выбранные фотографии?')) {
        return false;
    }

    for (let item of selected) {
        const photo = $(item);
        // noinspection JSUnusedGlobalSymbols
        request(
            btn.data('url') + '?' + $.param({id: photo.attr('data-id')}),
            'POST',
            {},
            () => {
                btn.prop('disabled', false);
                photo.remove();
                let inputImage = $('.post-images input[value="' + photo.attr('data-id') + '"]');
                if (inputImage.length) {
                    inputImage.closest('.photo-thumb-wrapper').remove();
                    $('.post-images .photo-thumb-wrapper:first-child .main-radio').prop('checked', true);
                }
            },
            {
                beforeSend: () => btn.prop('disabled', true),
            }
        )
    }
});

$(document).on('click', '.btn-force-delete-image', function () {
    const selected = filesList.find('.selected');
    const btn = $(this);

    if (selected.length < 1) return false;

    if (!confirm('Вы действительно хотите удалить выбранные фотографии НАВСЕГДА?')) {
        return false;
    }

    for (let item of selected) {
        const photo = $(item);
        // noinspection JSUnusedGlobalSymbols
        request(
            btn.data('url') + '?' + $.param({id: photo.attr('data-id')}),
            'POST',
            {},
            () => {
                btn.prop('disabled', false);
                photo.remove();
                let inputImage = $('.post-images input[value="' + photo.attr('data-id') + '"]');
                if (inputImage.length) {
                    inputImage.closest('.photo-thumb-wrapper').remove();
                    $('.post-images .photo-thumb-wrapper:first-child .main-radio').prop('checked', true);
                }
            },
            {
                beforeSend: () => btn.prop('disabled', true),
            }
        )
    }
});

$(document).on('click', '.btn-restore-image', function () {
    const selected = filesList.find('.selected');
    const btn = $(this);

    if (selected.length < 1) return false;

    if (!confirm('Вы действительно хотите восстановить выбранные фотографии?')) {
        return false;
    }

    for (let item of selected) {
        const photo = $(item);
        // noinspection JSUnusedGlobalSymbols
        request(
            btn.data('url') + '?' + $.param({id: photo.attr('data-id')}),
            'GET',
            {},
            () => {
                btn.prop('disabled', false);
                photo.remove();
                let inputImage = $('.post-images input[value="' + photo.attr('data-id') + '"]');
                if (inputImage.length) {
                    inputImage.closest('.photo-thumb-wrapper').remove();
                    $('.post-images .photo-thumb-wrapper:first-child .main-radio').prop('checked', true);
                }
            },
            {
                beforeSend: () => btn.prop('disabled', true),
            }
        )
    }
});

// event: drag & drop files to box
$('#photo-manager').on('dragover dragenter', function (e) {
    e.preventDefault();
    e.stopPropagation();
}).on('drop', function (e) {
    if (e.originalEvent.dataTransfer) {
        if (e.originalEvent.dataTransfer.files.length) {
            e.preventDefault();
            e.stopPropagation();

            uploadSelectedPhotos(e.originalEvent.dataTransfer.files, false);
        }
    }
});

uploadedImagesField.on('change', function (e) {
    const withLogo = !!uploadedImagesField.data('with-logo');

    uploadSelectedPhotos(e.target.files, withLogo);
});

function uploadSelectedPhotos(files, withLogo = false) {
    let $btnUpload = withLogo ? $('.btnUploadWithWatermark') : $('.btnUploadWithoutWatermark');
    $btnUpload.text($btnUpload.data('uploading'));
    $('.upload-images button').prop('disabled', true);
    let selectedFolder = $('.folder.selected');
    let folderId = 0;

    if (selectedFolder.length && selectedFolder.data('id') > 0) {
        folderId = selectedFolder.data('id');
    }

    for (let file of files) {
        let data = new FormData();
        data.append('file', file);
        data.append('withLogo', withLogo ? '1' : '0');
        data.append('folderId', folderId);

        request(
            uploadedImagesField.attr('data-url'),
            'POST',
            data,
            res => {
                if (res.success) {
                    // noinspection JSUnresolvedVariable
                    let photo = $(res.photo);
                    photo.addClass('selected');
                    filesList.prepend(photo);
                } else {
                    // noinspection JSValidateTypes
                    alert(res.error);
                }

                $('.upload-images button').prop('disabled', false);
                $btnUpload.text($btnUpload.data('upload'));
            },
            {
                dataType: 'json',
                async: true,
                cache: false,
                contentType: false,
                processData: false,
            }
        );
    }
}

searchForm.on('submit', function (e) {
    e.preventDefault();
    searchQuery = $.trim(searchForm.find('input').val());
    loadList(searchForm.attr('action'), {q: searchQuery});
});

$(document).on('click', '.btn-remove-card', function () {
    const card = $(this).closest('.photo-thumb-wrapper');
    if (card.find('.main-radio').prop('checked')) {
        $('#empty-main').prop('checked', true);
    }
    card.remove();
});

$(document).on('click', '.btn-size', function () {


    alert()
    sizeModal.find('#size-image-id').val($(this).attr('data-id'));
    request(
        $(this).attr('data-url'),
        'POST',
        {id: $(this).attr('data-id')},
        (res) => {
            if (res.success) {
                sizeModal.find('.modal-body .size-group').html(res.html);
                sizeModal.modal('show');
            } else {
            }
        }
    );
});

$('.post-images').sortable({
    items: '.photo-thumb-wrapper',
    handle: '.sort',
}).disableSelection();

const loadList = (url, data = {}) => {
    request(url, 'GET', data, res => {
        $('.folder.rootFolder').addClass('selected').siblings().removeClass('selected');
        filesList.html(res.photos)
    });
}

const request = (url, method, data, onSuccess, opts) => {
    $.ajax({
        type: method,
        url,
        data,
        ...opts
    }).done(function (response) {
        onSuccess(response);
    }).fail(function (error) {
        console.log(error.message);
    });
}


$(document).on('click', '.btn-remove-card', function () {
    let photo = $(this).parents('.photo-thumb').find('input[type=hidden]').val();
    let postContent = CKEDITOR.instances['postContent'];
    let token=generatePhotoToken(photo)
    text = postContent.getData().replaceAll(token, " ");
    postContent.setData(text);
});



function generatePhotoToken(photoID) {
    // return '{{photo-token=' + photoID +'}} ';
    return '{{photo-token-' + photoID+'}}';
}

$(document).on('click', '.btn-insert', function () {
    let photoID = $(this).attr('data-id')
    sizeModal.find('#size-image-id').val(photoID);

    (function () {
            tinymce.activeEditor?.execCommand('mceInsertContent', false, generatePhotoToken(photoID));
    })();

});
