<?php

return [
    'index' => 'posts',
    'host' => env('MEILISEARCH_HOST', 'meilisearch:7700'),
    'key' => env('MEILISEARCH_KEY', 'masterKey'),
];
