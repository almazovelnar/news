<?php

return [
    'template' => [
        'list'  => [
            'w' => 160,
            'h' => 88
        ],
        'large' => [
            'w' => 553,
            'h' => 310
        ],
    ]
];
