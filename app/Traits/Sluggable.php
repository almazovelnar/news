<?php

namespace App\Traits;

trait Sluggable
{
    public string $slugName = 'slug';

    public function generateSlug(string $slug, ?int $id = null): void
    {
        if ($this->id) {
            $id = $this->id;
        }

        $slug = \Str::slug($slug, '-', request()->lang ?? config('app.locale'));
        $query = self::query()->where($this->slugName, $slug);

        if ($id) {
            $query->where('id', '!=', $id);
        } else {
            $id = self::query()->latest()->first()?->id + 1;
        }

        if ($query->exists()) {
            $slug = $slug . ($id ? "-$id" : '');
        }

        $this->{$this->slugName} = $slug;
    }

    public function generateFields()
    {
        $fields = $this->getTranslatedFields();
        foreach ($fields as $locale => $field) {
            $fields[$locale][$this->slugName] = \Str::slug($field[$this->slugName] ?? $field['title'], '-', $locale);
        }

        return $fields;
    }
}

