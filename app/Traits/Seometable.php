<?php

namespace App\Traits;

use Str;

trait Seometable
{
    private function collectMeta()
    {
        $request = request();
        $meta = $request->meta;
        $meta['title'] = $meta['title'] ?? $request->title;
        $meta['description'] = $meta['description'] ?? ($request->description ? Str::words(strip_tags($request->description)) : '');

        return $meta;
    }
}
