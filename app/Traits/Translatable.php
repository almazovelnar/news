<?php
declare(strict_types=1);

namespace App\Traits;

use LaravelLocalization;

trait Translatable
{
    protected array $translations = [];

    /**
     * @return array
     */
    public function getTranslatedFields(): array
    {
        return $this->translations;
    }

    protected function generateTranslationsForFields(array $fields, $request = null): void
    {
        $request = $request ?? request();

        foreach (LaravelLocalization::getSupportedLanguagesKeys() as $locale) {
            foreach ($fields as $field) {
                if (!isset($request->{$field}[$locale])) {
                    continue;
                }
                $this->translations[$locale][$field] = $request->{$field}[$locale];
            }
        }
    }
}
