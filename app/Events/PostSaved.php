<?php
declare(strict_types=1);

namespace App\Events;

use App\Models\Post\Post;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PostSaved
{
    use Dispatchable, SerializesModels;

    public function __construct(private Post $post)
    {
        //
    }

    public function getPost(): Post
    {
        return $this->post;
    }
}
