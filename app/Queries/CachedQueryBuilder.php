<?php

namespace App\Queries;

use Kalnoy\Nestedset\QueryBuilder;
use GeneaLabs\LaravelModelCaching\Traits\Caching;
use GeneaLabs\LaravelModelCaching\Traits\Buildable;
use GeneaLabs\LaravelModelCaching\Traits\BuilderCaching;

class CachedQueryBuilder extends QueryBuilder
{
    use Buildable;
    use BuilderCaching;
    use Caching;
}
