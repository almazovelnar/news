<?php

namespace App\Helpers;

class CodeGenerator
{
    public static function photo(int $id): string
    {
        return str_repeat('0', 7 - strlen("{$id}")) . $id;
    }

    public static function sizePhoto($author_code, $code, $type): string
    {
        return $author_code . '0' . $type . '00000000' . $code;
    }
}
