<?php

namespace App\Helpers;

use App\Models\Audit;

class LogHelper
{
    const EVENT_CREATED = 'created';
    const EVENT_UPDATED = 'updated';
    const EVENT_DELETED = 'deleted';

    const MODEL_NAME_KEYS = [
        'App\\Models\\User' => 'name',
        'App\\Models\\Post\\Post' => 'title',
        'App\\Models\\Category' => 'title',
        'App\\Models\\Tag' => 'name',
    ];

    public static function getEventMessage($event): string
    {
        switch ($event) {
            case self::EVENT_CREATED:
                $message = 'created_message'; break;
            case self::EVENT_UPDATED:
                $message = 'updated_message'; break;
            case self::EVENT_DELETED:
                $message = 'deleted_message'; break;
            default:
                $message = 'log_message';
        }

        return trans('admin.log.' . $message);
    }

    public static function getModelName($model)
    {

        return substr($model, strrpos($model, "\\") + 1);
    }
}
