<?php

namespace App\Helpers;

final class PostHelper
{
    /**
     * @param string $content
     * @return string
     */
    public static function clearContent(string $content): string
    {
        return self::clearTokens((strip_tags($content)));
    }

    public static function clearTokens(string $content): string
    {
        $content = preg_replace('/({{photo-token-[0-9]+}})/i', '', $content);
        return preg_replace('/({{photo-token-[0-9]+}})/i', '', $content);
    }

}
