<?php
declare(strict_types=1);

namespace App\Helpers;

final class WidgetTypeDetector
{

    public const YOUTUBE = 'youtube';
    public const FACEBOOK = 'facebook';
    public const TWITTER = 'twitter';
    public const INSTAGRAM = 'instagram';
    public const VKONTAKTE = 'vk';
    public const PLAYBUZZ = 'playbuzz';
    public const OTHER = 'other';

    /**
     * @return array
     */
    public static function getList(): array
    {
        return [
            self::YOUTUBE,
            self::FACEBOOK,
            self::TWITTER,
            self::INSTAGRAM,
            self::VKONTAKTE,
            self::PLAYBUZZ,
            self::OTHER
        ];
    }

    /**
     * @param string $type
     * @return bool
     */
    public static function hasType(string $type): bool
    {
        return in_array($type, self::getList());
    }

    /**
     * @param string $content
     * @return string
     */
    public static function getTypeByContent(string $content): string
    {
        if (
            strpos($content, 'iframe') !== false
            && strpos($content, 'youtube.com') !== false
        ) {
            return self::YOUTUBE;
        }
        if (
            strpos($content, '<blockquote class="twitter-tweet"') !== false
            || strpos($content, '<blockquote class="twitter-video"') !== false
        ) {
            return self::TWITTER;
        }
        if (
            strpos($content, '<blockquote class="instagram-media"') !== false
            || strpos($content, '<blockquote data-instgrm-captioned data-instgrm-permalink') !== false
        ) {
            return self::INSTAGRAM;
        }
        if (strpos($content, 'https://www.facebook.com/') !== false) {
            return self::FACEBOOK;
        }
        if (strpos($content, '<div id="vk_post_') !== false) {
            return self::VKONTAKTE;
        }
        if (
            strpos($content, 'playbuzz-bp-full-screen') !== false
            || strpos($content, 'class="playbuzz"') !== false
        ) {
            return self::VKONTAKTE;
        }

        return self::OTHER;
    }

}
