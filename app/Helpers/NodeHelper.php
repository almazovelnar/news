<?php

namespace App\Helpers;

final class NodeHelper
{
    protected $obj;

    public function __construct($nodeObj)
    {
        $this->obj =  new $nodeObj();
    }

    public function map(?int $excludeId = null): array
    {
        $filters = [];
        if ($excludeId) {
            $filters[] = ['id', '<>', $excludeId];
        }

        $nodes = $this->obj->where($filters)->defaultOrder()->get();
        $list = [];
        foreach ($nodes as $node) {
            $list[$node->id] = $node->title;
        }
        return $list;
    }

    public static function titleWithPrefix(string $title, int $depth): string
    {
        $prefix = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', 0);
        return $prefix . $title;
    }

}
