<?php

namespace App\Helpers;

use Illuminate\Support\Collection;

class SelectListHelper
{
    /**
     * @param Collection $allItems
     * @param Collection $selectedItems
     * @return mixed
     */
    public static function getItemsWithChosen($allItems, $selectedItems)
    {
        return $selectedItems->toBase()->merge($allItems)->unique('id');
    }
}
