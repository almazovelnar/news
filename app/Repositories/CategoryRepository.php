<?php

namespace App\Repositories;

use App\Enum\Status;
use App\Models\Category;
use App\Models\Post\Post;
use Illuminate\Database\Eloquent\Model;

class CategoryRepository
{
    public function getById(int $id)
    {
        return $this->categoryQuery()
            ->where('id', $id)
            ->where('language', app()->getLocale())
            ->firstOrFail();
    }

    public function getMenuItems(int $limit)
    {
        return $this->categoryQuery()
            ->whereNull('parent_id')
            ->where('on_menu', true)
            ->where('language', app()->getLocale())
            ->defaultOrder()
            ->take($limit)
            ->get();
    }

    public function getOthers(int $limit = 24)
    {
        return $this->categoryQuery()
            ->whereNull('parent_id')
            ->where('language', app()->getLocale())
            ->defaultOrder()
            ->take($limit)
            ->get();
    }

    public function getAll(int $limit = 12)
    {
        return $this->categoryQuery()
            ->where('language', app()->getLocale())
            ->whereNull('parent_id')
            ->defaultOrder()->take($limit)->get();
    }

    public function all()
    {
        return $this->categoryQuery()
            ->where('language', app()->getLocale())
            ->defaultOrder()
            ->get();
    }

    public function allByLang(string $lang)
    {
        return $this->categoryQuery()
            ->defaultOrder()
            ->where('language', $lang)
            ->get();
    }

    public function allParents(string $language)
    {
        return $this->categoryQuery()
            ->where('language', $language)
            ->defaultOrder()
            ->whereNull('parent_id')
            ->get();
    }

    public function getBySlug(string $slug): Category|Model
    {
        return $this->categoryQuery()
            ->where('language', app()->getLocale())
            ->where('slug', $slug)
            ->firstOrFail();
    }

    public function getToPostBySlug(string $slug): Category|Model
    {
        return Category::query()
            ->where('language', app()->getLocale())
            ->where('slug', $slug)
            ->firstOrFail();
    }

    public function categoryQuery()
    {
        return Category::query()
            ->whereNotNull('parent_id')
            ->where('status', Status::ACTIVE);
    }

    public function getChosen(?int $limit = 24)
    {
        return $this->categoryQuery()
            ->whereHas('posts')
            ->where('language', app()->getLocale())
            ->defaultOrder()
            ->where('chosen', true)
            ->take($limit)
            ->get();
    }

    public function getMenuChosens(?int $limit = 24)
    {
        return $this->categoryQuery()
            ->where('on_menu', true)
            ->where('language', app()->getLocale())
            ->defaultOrder()
            ->where('chosen', true)
            ->take($limit)
            ->get();
    }

    public function getMenuOthers(?int $limit = 24)
    {
        return $this->categoryQuery()
            ->where('on_menu', true)
            ->where('language', app()->getLocale())
            ->defaultOrder()
            ->where('chosen', false)
            ->take($limit)
            ->get();
    }

    public function translatedCats($groupID, $catID)
    {
        return Category::query()
            ->where('group_id', $groupID)
            ->where('group_id', '<>', null)
            ->where('id', '!=', $catID)
            ->get();
    }

    public function totalCount(): int
    {
        return Category::count();
    }

    public function getByQuery(string $query, string $language, ?int $chosen = null)
    {
        $existingTranslations = [$language];
        if (!empty($chosen)) {
            $existingTranslations[] = Post::whereIn('id', $chosen)->pluck('language')->toArray();
        }
        return Category::query()->where('title', 'like', "%$query%")
            ->whereNotIn('language', $existingTranslations)
            ->get();
    }

    public function getActiveCategoryCount(): int
    {
        return Category::where('status', 1)->count();
    }

    public function getList(string $language): array
    {
        return $this->categoryQuery()
        ->where('language', $language)
        ->defaultOrder()
        ->pluck('title', 'id')
        ->toArray();
    }

    public function getTranslateByLocale(int $groupId, string $locale)
    {
        return $this->categoryQuery()
            ->where('group_id', $groupId)
            ->where('language', $locale)
            ->first();
    }
}
