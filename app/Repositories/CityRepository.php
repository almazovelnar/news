<?php

namespace App\Repositories;

use App\Enum\Status;
use App\Models\Category;
use App\Models\City\City;

class CityRepository
{
    public function get($id): ?Category
    {
        return City::find($id);
    }

    public function all()
    {
        return $this->cityQuery()
            ->defaultOrder()
            ->where('status', Status::ACTIVE)
            ->get();
    }

    public function allParents()
    {
        return $this->cityQuery()
            ->defaultOrder()
            ->whereNull('parent_id')
            ->where('status', Status::ACTIVE)
            ->get();
    }

    public function getBySlug(string $slug): City
    {
        return $this->cityQuery()->where('slug', $slug)->firstOrFail();
    }

    public function cityQuery()
    {
        return City::where('status', Status::ACTIVE);
    }

    public function getChosen(?int $limit = 12)
    {
        return $this->cityQuery()
            ->defaultOrder()
            ->where('chosen', true)
            ->take($limit)
            ->get();
    }

    public function totalCount(): int
    {
        return City::count();
    }
}
