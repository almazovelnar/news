<?php

namespace App\Repositories;

use App\Enum\Status;
use App\Models\Page\Page;
use Illuminate\Database\Eloquent\Builder;

class PageRepository
{
    public function get($id)
    {
        return Page::find($id);
    }

    public function all(?int $limit = 12)
    {
        return $this->pageQuery()
            ->whereNull('parent_id')
            ->defaultOrder()
            ->take($limit)
            ->get();
    }

    public function getById(int $id)
    {
        return $this->pageQuery()
            ->where('id', $id)
            ->firstOrFail();
    }

    public function pageQuery(): Builder
    {
        return Page::where('status', Status::ACTIVE);
    }

    public function totalCount()
    {
        return Page::count();
    }
}
