<?php

namespace App\Repositories;

use App\Models\Photo\Photo;

class PhotoRepository
{
    public function get($id)
    {
        return Photo::query()->find($id);
    }
}
