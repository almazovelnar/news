<?php

namespace App\Repositories;

use DB;
use App\Enum\Status;
use App\Enum\PostType;
use App\Models\Post\Post;
use Illuminate\Database\Eloquent\Builder;

class OldPostRepository
{
    public function get($id)
    {
        return $this->postQuery()
            ->where('posts.id', $id)
            ->first();
    }

    public function all(?int $limit = 24)
    {
        return $this->postQuery()
            ->orderByDesc('published_at')
            ->paginate($limit);
    }

    public function getBreakingPosts(?int $limit = 10)
    {
        return $this->postQuery()
            ->where('chosen', true)
            ->orderByDesc('published_at')
            ->paginate($limit);
    }

    public function getRelatedPosts(int $catId, int $postId, $limit = 24, ?string $lastPub = null)
    {
        $query = $this->postQuery()
            ->where('posts.category_id', $catId)
            ->where('posts.id', '<>', $postId)
            ->orderByDesc('posts.published_at');

        if ($lastPub) {
            $query->where('published_at', '<', $lastPub);
        }

        return $query->limit($limit)->get();
    }

    public function getById(int $id)
    {
        return Post::query()
            ->select(['posts.*', 'c.id as category_id', 'c.title as category_title'])
            ->leftJoin('categories as c', 'category_id', '=', 'c.id')
            ->where('posts.is_published',true)
            ->where('c.language', app()->getLocale())
            ->where('posts.language', app()->getLocale())
            ->where('posts.id', $id)
            ->withTrashed()
            ->firstOrFail();
    }

    public function getPostsCount()
    {
        $query = Post::query()
            ->where('type', '!=', PostType::PRIVATE)
            ->where('is_published', true);

        return $query->count();
    }

    public function getByCategory(int $catId, ?int $limit = 12)
    {
        return $this->postQuery()
            ->whereHas('categories', function ($q) use ($catId) {
                $q->where('category_id', $catId);
            })
            ->latest('published_at')
            ->paginate($limit);
    }

    public function getByTag(int $tagId, ?int $limit = 24)
    {
        return $this->postQuery()
            ->whereHas('tags', function(Builder $q) use ($tagId) {
                $q->where('tags.id', $tagId);
            })
            ->latest('published_at')
            ->paginate($limit);
    }

    public function postQuery()
    {
        return Post::query()
            ->where('posts.is_published',true)
            ->where('posts.language', app()->getLocale());
    }

    public function getByQuery(string $q, string $lang, $chosen)
    {
        $oldTrans = [];
        if (!empty($chosen)) {
            $oldTrans = Post::query()
                ->where('posts.is_published',true)
                ->whereIn('posts.id', $chosen)
                ->pluck('language')
                ->toArray();
        }
        array_push($oldTrans, $lang);

        return Post::query()
            ->where('posts.type', '!=', PostType::PRIVATE)
            ->where('posts.is_published',true)
            ->where('posts.title', 'like', "%$q%")
            ->whereNotIn('posts.language', $oldTrans)
            ->get();
    }

    public function translatedPosts($groupID, $postID)
    {
        return Post::query()
            ->select(['posts.*', 'c.slug as category_slug', 'c.title as category_title'])
            ->join('categories as c', 'category_id', '=', 'c.id')
            ->where('posts.type', '!=', PostType::PRIVATE)
            ->where('posts.group_id', $groupID)
            ->where('posts.group_id','<>', null)
            ->where('posts.id', '!=', $postID)
            ->get();
    }

    public function getActivePostsCount(?string $language): int
    {
        $query = Post::query()
            ->where('type', '!=', PostType::PRIVATE)
            ->where('is_published', true);

        if ($language) {
            $query->where('language', $language);
        }

        return $query->count();
    }

    public function getByRange(int $offset, int $limit, string $language): array
    {
        return DB::table('posts as p')
            ->select(['p.id', 'p.slug', 'p.updated_at', 'c.slug as category_slug'])
            ->join('categories as c', 'p.category_id', '=', 'c.id')
            ->where('p.type', '!=', PostType::PRIVATE)
            ->where('p.is_published', true)
            ->where('p.language', $language)
            ->offset($offset)
            ->limit($limit)
            ->oldest('p.id')
            ->get()
            ->toArray();
    }
}
