<?php

namespace App\Repositories;

use App\Models\LanguageLine\Translate;

class TranslateRepository
{
    public function get($id)
    {
        return Translate::find($id);
    }
}
