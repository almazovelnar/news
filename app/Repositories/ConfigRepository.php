<?php

namespace App\Repositories;

use App\Models\Config\Config;

class ConfigRepository
{
    public function get($id)
    {
        return Config::get($id);
    }

    public function getSocials()
    {
        return Config::query()->where('type', 'social')->get();
    }

    public function getConfigs()
    {
        return Config::query()->where('type', '!=','social')->get();
    }
}
