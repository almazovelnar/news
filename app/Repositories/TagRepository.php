<?php

namespace App\Repositories;

use App\Models\Tag;

class TagRepository
{

    public function getTagsCount(?string $language = null): int
    {
        $language = $language ?: \App::getLocale();
        return Tag::where('language', $language)->where('count', '>', 2)->count('id');
    }

    public function getByRange(int $offset, int $limit, string $language): array
    {
        return Tag::where('language', $language)
            ->offset($offset)
            ->limit($limit)
            ->where('count', '>', 2)
            ->oldest('id')
            ->get()
            ->toArray();
    }

    public function getBySlug(string $slug, string $lang): ?Tag
    {
        return Tag::where('slug', $slug)->where('language', $lang)->first();
    }

    public function getById(int $id)
    {
        return Tag::query()->where('id', $id)->firstOrFail();
    }

    public function getByName(string $name, string $lang)
    {
        return Tag::query()
            ->where('name', $name)
            ->where('language', $lang)
            ->first();
    }

    public function getByNameOrId(string $q, string $lang)
    {
        return Tag::query()
            ->where(function ($query) use ($q) {
                $query->where('name', $q)
                    ->orWhere('id', $q);
            })
            ->where('language', $lang)
            ->first();
    }

    public function getByQuery(string $q, string $lang)
    {
        return Tag::query()
            ->where('name', 'like', "%$q%")
            ->where('language', $lang)
            ->get();
    }

    public function getByPost(int $postId)
    {
        return Tag::query()
            ->select(['tags.*'])
            ->join('post_tag as pt', 'pt.tag_id', '=', 'tags.id')
            ->where('pt.post_id', $postId)
            ->where('language', app()->getLocale())
            ->get();
    }

    public function getPopulars(string $lang, ?int $limit = 20)
    {
        return Tag::query()
            ->where('language', $lang)
            ->take($limit)
            ->orderByDesc('count')
            ->get();
    }

    public function getChosens(string $lang, ?int $limit = 20)
    {
        return Tag::query()
            ->where('language', $lang)
            ->where('chosen', true)
            ->take($limit)
            ->orderByDesc('count')
            ->get();
    }

    public function getMainTags(string $lang, ?int $limit = 20)
    {
        return Tag::query()
            ->where('language', $lang)
            ->where('is_main', true)
            ->take($limit)
            ->get();
    }
}
