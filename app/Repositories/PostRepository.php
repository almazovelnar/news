<?php

namespace App\Repositories;

use DB;
use App\Enum\Status;
use App\Enum\PostType;
use App\Models\Post\Post;
use Illuminate\Database\Eloquent\Builder;

class PostRepository
{
    public function get($id)
    {
        return Post::query()
            ->where('posts.id', $id)
            ->withTrashed()
            ->firstOrFail();
    }

    public function translatedPosts($groupID, $postID)
    {
        return Post::query()
            ->select(['posts.*', 'c.slug as category_slug', 'c.title as category_title'])
            ->join('categories as c', 'category_id', '=', 'c.id')
            ->where('posts.type', '!=', PostType::PRIVATE)
            ->where('posts.group_id', $groupID)
            ->where('posts.group_id','<>', null)
            ->where('posts.id', '!=', $postID)
            ->get();
    }

    public function getReferences($postId)
    {
        return Post::query()
            ->select(['posts.title', 'posts.id', 'r.title as r_title', 'r.id as r_id'])
            ->join('post_references as pr', 'posts.id', '=', 'pr.post_id')
            ->join('posts as r', 'pr.reference_id', '=', 'r.id')
            ->where('posts.type', '!=', PostType::PRIVATE)
            ->where('posts.id', $postId)
            ->get();
    }

    public function getActivePostsCount(?string $language): int
    {
        $query = Post::query()
            ->where('type', '!=', PostType::PRIVATE)
            ->where('is_published', true);

        if ($language) {
            $query->where('language', $language);
        }

        return $query->count();
    }

    public function getByRange(int $offset, int $limit, string $language): array
    {
        return DB::table('posts as p')
            ->select(['p.id', 'p.slug', 'p.updated_at', 'c.slug as category_slug'])
            ->join('categories as c', 'p.category_id', '=', 'c.id')
            ->where('p.type', '!=', PostType::PRIVATE)
            ->where('p.is_published', true)
            ->where('p.language', $language)
            ->offset($offset)
            ->limit($limit)
            ->oldest('p.id')
            ->get()
            ->toArray();
    }


    public function getByTag(int $tagId, ?int $limit = 24)
    {
        return Post::query()
            ->where('posts.is_published',true)
            ->where('posts.language', app()->getLocale())
            ->whereHas('tags', function(Builder $q) use ($tagId) {
                $q->where('tags.id', $tagId);
            })
            ->latest('published_at')
            ->paginate($limit);
    }

    public function getTranslate(string $q, string $lang, $chosen)
    {
        $oldTrans = [];
        if (!empty($chosen)) {
            $oldTrans = Post::query()
                ->where('posts.is_published',true)
                ->whereIn('posts.id', $chosen)
                ->pluck('language')
                ->toArray();
        }

        array_push($oldTrans, $lang);

        return Post::query()
            ->where('posts.is_published',true)
            ->whereNotIn('posts.language', $oldTrans)
            ->where(function ($query) use ($q) {
                $query->where('posts.title', 'like', "%$q%")
                    ->orWhere('id', $q);
            })
            ->orderByDesc('published_at')
            ->limit(10)
            ->get();
    }

    public function getReference(string $q, string $lang, $chosen, $excludeId)
    {
        $query = Post::query()
            ->where('is_published',true)
            ->where('language', $lang);

        if (!empty($chosen)) {
            $query->whereNotIn('id', $chosen);
        }

        if (!empty($excludeId)) {
            $query->where('id', '!=', $excludeId);
        }

        return $query
            ->where(function ($query) use ($q) {
                $query->where('posts.title', 'like', "%$q%")
                    ->orWhere('id', $q);
            })
            ->orderByDesc('published_at')
            ->limit(10)
            ->get();
    }
}
