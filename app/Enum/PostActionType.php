<?php
declare(strict_types=1);

namespace App\Enum;

class PostActionType
{
    const PENDING   = 'pending';
    const PUBLISHED = 'published';
    const DELETED   = 'deleted';

    public static function getList(): array
    {
        return [
            self::PENDING   => trans('admin.action-type.' . self::PENDING),
            self::PUBLISHED => trans('admin.action-type.' . self::PUBLISHED),
            self::DELETED   => trans('admin.action-type.' . self::DELETED),
        ];
    }

    public static function get(int $status): string
    {
        if (array_key_exists($status, self::getList())) {
            return self::getList()[$status];
        }

        return '';
    }
}
