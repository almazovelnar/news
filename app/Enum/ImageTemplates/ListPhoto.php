<?php

namespace App\Enum\ImageTemplates;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class ListPhoto implements FilterInterface
{
    const folderName = 'list';

    public function applyFilter(Image $image)
    {
        $sizes = [350, 400, 450, 500, 550, 600];
        $size  = array_rand($sizes);

        if ($image->getHeight() > $image->getWidth()) {
            $height = $sizes[$size];
        } else {
            $width = $sizes[$size];
            $height = null;
        }

        return $image->resize($width ?? null, $height ?? null, function ($constraint) {
            $constraint->aspectRatio();
        });
    }
}
