<?php

namespace App\Enum\ImageTemplates;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class MediumPhoto implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(575, 380);
    }
}
