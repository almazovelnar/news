<?php
declare(strict_types=1);

namespace App\Enum;

class PostType
{
    const NORMAL  = 'normal';
    const PRIVATE = 'private';
    const PHOTO   = 'photo';
    const VIDEO   = 'video';
    const OPINION = 'opinion';

    public static function getList(): array
    {
        return [
            self::NORMAL  => trans('admin.type.' . self::NORMAL),
            self::PRIVATE => trans('admin.type.' . self::PRIVATE),
            self::PHOTO   => trans('admin.type.' . self::PHOTO),
            self::VIDEO   => trans('admin.type.' . self::VIDEO),
            self::OPINION => trans('admin.type.' . self::OPINION),
        ];
    }

    public static function get(string $type): string
    {
        if (array_key_exists($type, self::getList())) {
            return self::getList()[$type];
        }

        return '';
    }
}
