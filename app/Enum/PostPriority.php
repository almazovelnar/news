<?php
declare(strict_types=1);

namespace App\Enum;

class PostPriority
{
    const NORMAL    = 'normal';
    const IMPORTANT = 'important';
    const URGENT    = 'urgent';

    public static function getList(): array
    {
        return [
            self::NORMAL    => trans('admin.priority.' . self::NORMAL),
            self::IMPORTANT => trans('admin.priority.' . self::IMPORTANT),
            self::URGENT    => trans('admin.priority.' . self::URGENT),
        ];
    }

    public static function getListWithColor(): array
    {
        return [
            self::NORMAL    => trans('admin.priority.color_' . self::NORMAL),
            self::IMPORTANT => trans('admin.priority.color_' . self::IMPORTANT),
            self::URGENT    => trans('admin.priority.color_' . self::URGENT),
        ];
    }

    public static function getLabel(string $priority)
    {
        $labels = [
            self::NORMAL    => 'secondary',
            self::IMPORTANT => 'success',
            self::URGENT    => 'danger',
        ];

        return array_key_exists($priority, $labels) ? $labels[$priority] : 'secondary';
    }

    public static function get(string $type): string
    {
        if (array_key_exists($type, self::getList())) {
            return self::getList()[$type];
        }

        return '';
    }
}
