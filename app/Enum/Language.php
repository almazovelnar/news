<?php
declare(strict_types=1);

namespace App\Enum;

use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class Language
{
    public static function getList(): array
    {
        $langs = [];
        foreach (array_values(LaravelLocalization::getSupportedLanguagesKeys()) as $lang) {
            $langs[$lang] = $lang;
        }
        return $langs;
    }

    public static function getAnotherList($langTR): array
    {
        $langs = [];
        foreach (array_values(LaravelLocalization::getSupportedLanguagesKeys()) as $lang) {
            if ($langTR!=$lang){
                $langs[$lang] = $lang;
            }
        }
        return $langs;
    }



}
