<?php
declare(strict_types=1);

namespace App\Enum;

class PostStatus
{
    const ACTIVE = 1;
    const INACTIVE = 0;
    const PENDING = 2;

    public static function getList(): array
    {
        return [
            self::INACTIVE => trans('admin.status.inactive'),
            self::ACTIVE => trans('admin.status.published'),
            self::PENDING => trans('admin.status.pending'),
        ];
    }

    public static function get(int $status): string
    {
        if (array_key_exists($status, self::getList())) {
            return self::getList()[$status];
        }

        return '';
    }
}
