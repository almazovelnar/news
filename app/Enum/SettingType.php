<?php
declare(strict_types=1);

namespace App\Enum;

class SettingType
{
    const JSON    = 'json';
    const STRING  = 'string';
    const NUMERIC = 'numeric';

    public static function getList(): array
    {
        return [
            self::JSON    => trans('admin.type.json'),
            self::STRING  => trans('admin.type.string'),
            self::NUMERIC => trans('admin.type.numeric'),
        ];
    }

    public static function get(int $type): string
    {
        if (array_key_exists($type, self::getList())) {
            return self::getList()[$type];
        }

        return '';
    }
}
