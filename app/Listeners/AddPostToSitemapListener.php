<?php
declare(strict_types=1);

namespace App\Listeners;

use App\Events\PostSaved;
use App\Services\Sitemap\Sources\PostSource;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;

class AddPostToSitemapListener implements ShouldQueue
{

    use InteractsWithQueue;

    public function __construct(private PostSource $postSource)
    {
        //
    }

    public function handle(PostSaved $event): void
    {
        if ($event->getPost()->is_active) {
            $this->postSource->updateOrCreatePage($event->getPost()->language);
        }
    }
}
