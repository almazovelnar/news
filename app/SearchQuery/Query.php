<?php

namespace App\SearchQuery;

interface Query
{

    public function search($filters = []);

}
