<?php

namespace App\SearchQuery;

use App\Models\Translation;

class TranslationSearchQuery implements Query
{

    public function search($filters = [])
    {
        $query = Translation::query()->orderByDesc('id');

//        if (!empty($filters['text'])) {
//            $filters['text'] = mb_strtolower(StringHelper::clearSearchQuery($filters['text']));
//            $query->whereRaw("text LIKE '%{$filters['text']}%'");
//        }

        return $query;
    }

}
