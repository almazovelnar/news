<?php

namespace App\SearchQuery;

use App\Enum\PostActionType;
use App\Enum\PostStatus;
use App\Models\Post\Post;

class PostSearchQuery implements Query
{

    public function search($filters = [], $actionType = null)
    {
        $language = session()->get('current-lang') ?? config('app.admin_locale');

        $query = Post::query()
            ->with('users')
            ->with('categories')
            ->where('posts.language', $language);


        if ($actionType == PostActionType::DELETED) {
            $query
                ->withTrashed()
                ->whereNotNull('deleted_at')
                ->orderByDesc('posts.deleted_at');
        } else {
            $query->orderByDesc('posts.published_at');
        }


        if ($actionType == PostActionType::PUBLISHED) {
            $query->where('status', PostStatus::ACTIVE);
        }
        if ($actionType == PostActionType::PENDING) {
            $query->where('status', PostStatus::PENDING);
        }

        if (!empty($filters['category'])) {
            $cat = $filters['category'];
            $query->whereHas('categories', function ($q) use($cat) {
                $q->where('categories.id', $cat);
            });
        }
        if (!empty($filters['id'])) {
            $query->where('posts.id', $filters['id']);
        }
        if (!empty($filters['title'])) {
            $query->where('posts.title', 'like', "%{$filters['title']}%");
        }
        if (!empty($filters['published_at'])) {
            $query->where('posts.published_at', 'like', "%{$filters['published_at']}%");
        }
        if (!empty($filters['priority'])) {
            $query->where('posts.priority', $filters['priority']);
        }

        return $query;
    }

}
