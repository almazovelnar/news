<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'link'        => route('category', $this->slug),
            'language'    => $this->language,
            'title'       => $this->title,
            'description' => $this->description,
            'image'       => $this->image ? asset('storage/categories/' . $this->image) : null,
        ];
    }
}
