<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $mainCategory = $this->mainCategory;

        return [
            'id'             => $this->id,
            'user_id'        => $this->user_id,
            'link'           => route('post', ['category' => $this->slug, 'slug' => $this->slug]),
            'category'       => CategoryResource::collection($this->categories),
            'category_id'    => $mainCategory->id,
            'category_title' => $mainCategory->title,
            'title'          => $this->title,
            'description'    => $this->description,
            'slug'           => $this->slug,
            'image'          => get_image($this->image, $this->path, 'large'),
            'chosen'         => (bool)$this->chosen,
            'views'          => (int)$this->views,
            'likes'          => (int)$this->likes,
            'dislikes'       => (int)$this->dislikes,
            'date'           => Carbon::parse($this->published_at)->toDateTimeString(),
        ];
    }
}
