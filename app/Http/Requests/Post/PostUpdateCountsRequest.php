<?php

namespace App\Http\Requests\Post;

class PostUpdateCountsRequest extends PostRequest
{
    public function rules(): array
    {
        return [
            'views'    => ['max:10000'],
            'likes'    => ['max:10000'],
            'dislikes' => ['max:10000'],
        ];
    }
}
