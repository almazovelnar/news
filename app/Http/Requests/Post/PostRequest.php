<?php

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'image.required'       => trans('admin.validation.image_required'),
            'slug.unique'          => trans('admin.validation.unique'),
            'meta.title.required'  => trans('admin.validation.required'),
            'meta.title.string'    => trans('admin.validation.string'),
            'title.required'       => trans('admin.validation.required'),
            'description.required' => trans('admin.validation.required'),
            'slug.required'        => trans('admin.validation.required'),
            '*.required'           => trans('admin.validation.required'),
            '*.min'                => trans('admin.validation.minimum', ['count' => ':min']),
        ];
    }

    public function attributes()
    {
        return [
            'marked_words.*' => trans('admin.post.tags'),
            'title'          => trans('admin.title'),
            'meta.title'     => trans('admin.title'),
            'description'    => trans('admin.post.description'),
            'category'       => trans('admin.post.category'),
            'story'          => trans('admin.post.story'),
            'city'           => trans('admin.post.city'),
            'author'         => trans('admin.post.author'),
            'type'           => trans('admin.post.type'),
            'image'          => trans('admin.image'),
        ];
    }

}
