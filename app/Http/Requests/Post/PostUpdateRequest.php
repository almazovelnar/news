<?php

namespace App\Http\Requests\Post;

class PostUpdateRequest extends PostRequest
{
    public function rules(): array
    {
        $rules = [
            'title'       => ['required', 'min:3'],
            'description' => ['required'],
            'slug'        => ['required', 'min:3'],
            'category'    => ['required'],
            'tags'        => ['array'],
            'tags.*'      => ['array'],
            //            'image'        => 'required'
        ];

        return $rules;
    }
}
