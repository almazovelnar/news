<?php

namespace App\Http\Requests\Author;


class AuthorCreateRequest extends AuthorRequest
{
    public function rules(): array
    {
        return [
            'name'  => ['string', 'required', 'min:3'],
            'image' => ['required', 'image', 'mimes:' . config('app.admin_image_extensions'), 'max:' . config('app.admin_image_max_size')]
        ];
    }
}
