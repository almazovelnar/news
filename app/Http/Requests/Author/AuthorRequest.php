<?php

namespace App\Http\Requests\Author;

use Illuminate\Foundation\Http\FormRequest;

class AuthorRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'title.min'      => trans('admin.validation.minimum', ['count' => 3]),
            'title.required' => trans('admin.validation.required'),
            'slug.unique'    => trans('admin.validation.unique'),
            'slug.required'  => trans('admin.validation.required'),
            'slug.min'       => trans('admin.validation.minimum', ['count' => 3]),
            'image.required' => trans('admin.validation.image_required'),
        ];
    }

    public function attributes()
    {
        return [
            'title' => trans('admin.title'),
        ];
    }

}
