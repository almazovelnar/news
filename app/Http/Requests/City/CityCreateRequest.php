<?php

namespace App\Http\Requests\City;

use Illuminate\Foundation\Http\FormRequest;

class CityCreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'title'     => ['array'],
            'title.*'   => ['required', 'min:3'],
            'slug'      => ['array'],
            'slug.*'    => ['required', 'min:3'],
            'parent_id' => ['integer', 'nullable'],
        ];
    }
}
