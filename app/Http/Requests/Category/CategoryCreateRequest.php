<?php

namespace App\Http\Requests\Category;


class CategoryCreateRequest extends CategoryRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'title' => ['string', 'required', 'min:3'],
            'slug'  => ['string', 'required', 'min:3'],
            'image' => ['image', 'mimes:' . config('app.admin_image_extensions'), 'max:' . config('app.admin_image_max_size')]
        ];
    }
}
