<?php

namespace App\Http\Requests\Category;


use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'image.required'      => trans('admin.validation.image_required'),
            'slug.unique'         => trans('admin.validation.unique'),
            'meta.title.required' => trans('admin.validation.required'),
            'meta.title.string'   => trans('admin.validation.string'),
            'title.required'      => trans('admin.validation.required'),
            'slug.required'       => trans('admin.validation.required'),
            'slug.min'            => trans('admin.validation.minimum', ['count' => 3]),
            'title.min'           => trans('admin.validation.minimum', ['count' => 3]),
        ];
    }

    public function attributes()
    {
        return [
            'title'      => trans('admin.title'),
            'meta.title' => trans('admin.title'),
        ];
    }

}
