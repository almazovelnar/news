<?php

namespace App\Http\Requests\Category;


class CategoryUpdateRequest extends CategoryRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'title' => ['required', 'string', 'min:3'],
            'slug'  => ['required', 'string', 'min:3'],
            'image' => ['image', 'mimes:' . config('app.admin_image_extensions'), 'max:' . config('app.admin_image_max_size')]
        ];
    }
}
