<?php

namespace App\Http\Middleware;

use Auth;
use Cookie;
use Closure;
use Illuminate\Http\Request;

class SiteMiddleware
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        return $response;
    }
}
