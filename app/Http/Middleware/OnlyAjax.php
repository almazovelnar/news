<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;

class OnlyAjax
{
    /**
     * @param $request
     * @param Closure $next
     * @return Application|ResponseFactory|Response|mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->ajax())
            return response('Forbidden.', 403);

        return $next($request);
    }
}
