<?php

namespace App\Http\Middleware;

use App\Enum\PostActionType;
use Closure;
use Illuminate\Http\Request;
use Spatie\Permission\Exceptions\UnauthorizedException;

class PostAuthorize
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = \Auth::user();

        if ($request->type == PostActionType::DELETED && !$user->canAny(['posts/deleted', 'posts/*'])) {
            throw UnauthorizedException::forRolesOrPermissions([]);
        } else if ($request->type == PostActionType::PUBLISHED && !$user->canAny(['posts/published', 'posts/*'])) {
            throw UnauthorizedException::forRolesOrPermissions([]);
        } else if ($request->type == PostActionType::PENDING && !$user->canAny(['posts/pending', 'posts/*'])) {
            throw UnauthorizedException::forRolesOrPermissions([]);
        }

        return $next($request);
    }
}
