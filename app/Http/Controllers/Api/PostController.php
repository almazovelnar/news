<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\PostResource;
use App\Http\Resources\TagResource;
use App\Repositories\OldPostRepository;
use App\Repositories\TagRepository;

class PostController
{
    public function __construct(
        private OldPostRepository $postRepository,
        private TagRepository $tagRepository
    )
    {
    }

    public function index()
    {
        return response()->json([
            'data' => PostResource::collection($this->postRepository->all())
        ]);
    }

    public function view(int $id)
    {
        $post = $this->postRepository->getById($id);
        return response()->json([
            'post'         => new PostResource($post),
            'relatedPosts' => PostResource::collection($this->postRepository->getRelatedPosts($post->category_id, $post->id, 6)),
            'tags'         => TagResource::collection($this->tagRepository->getByPost($id))
        ]);
    }

    public function breaking()
    {
        return response()->json([
            'data' => PostResource::collection($this->postRepository->getBreakingPosts())
        ]);
    }
}
