<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\PostResource;
use App\Http\Resources\TagResource;
use App\Repositories\OldPostRepository;
use App\Repositories\TagRepository;

class TagController
{
    public function __construct(
        private TagRepository $tagRepository,
        private OldPostRepository $postRepository
    )
    {
    }

    public function index()
    {
        return response()->json([
            'data' => TagResource::collection($this->tagRepository->getChosens(app()->getLocale()))
        ]);
    }

    public function view(int $id)
    {
        $tag = $this->tagRepository->getById($id);

        return response()->json([
            'data' => new TagResource($tag),
            'posts' => PostResource::collection($this->postRepository->getByTag($id))
        ]);
    }
}
