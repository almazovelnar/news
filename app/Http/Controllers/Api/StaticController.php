<?php
declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ConfigResource;
use App\Http\Resources\MainInfoResource;
use App\Http\Resources\PageResource;
use App\Repositories\ConfigRepository;
use App\Repositories\MainInfoRepository;
use App\Repositories\PageRepository;

class StaticController extends Controller
{
    public function __construct(
        private PageRepository $pageRepository,
        private ConfigRepository $configRepository,
        private MainInfoRepository $mainInfoRepository,
    )
    {
    }

    public function pages()
    {
        return response()->json([
            'data' => PageResource::collection($this->pageRepository->all())
        ]);
    }

    public function pageView(int $id)
    {
        $page = $this->pageRepository->getById($id);

        return response()->json([
            'data' => new PageResource($page)
        ]);
    }

    public function socials()
    {
        return response()->json([
            'data' => ConfigResource::collection($this->configRepository->getSocials())
        ]);
    }

    public function configs()
    {
        return response()->json([
            'data' => ConfigResource::collection($this->configRepository->getConfigs())
        ]);
    }

    public function mainInfo()
    {
        return response()->json([
            'data'    => new MainInfoResource($this->mainInfoRepository->getFirst()),
            'socials' => ConfigResource::collection($this->configRepository->getSocials()),
        ]);
    }


    public function languages()
    {
        return response()->json([
            'list'    => \LaravelLocalization::getSupportedLanguagesKeys(),
            'current' => app()->getLocale()
        ]);
    }

    public function live()
    {
        return response()->json([
            'data' => get_stream(),
        ]);
    }

    public function advertising()
    {
        $page = $this->pageRepository->getById(2);

        return response()->json([
            'data' => new PageResource($page),
        ]);
    }

    public function about()
    {
        $page = $this->pageRepository->getById(1);

        return response()->json([
            'data'      => new PageResource($page),
            'main-info' => new MainInfoResource($this->mainInfoRepository->getFirst()),
        ]);
    }
}
