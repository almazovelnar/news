<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CategoryResource;
use App\Http\Resources\PostResource;
use App\Repositories\CategoryRepository;
use App\Repositories\OldPostRepository;

class CategoryController
{
    public function __construct(
        private CategoryRepository $categoryRepository,
        private OldPostRepository $postRepository
    )
    {
    }

    public function index()
    {
        return response()->json([
            'data' => CategoryResource::collection($this->categoryRepository->all())
        ]);
    }

    public function view(int $id)
    {
        $category = $this->categoryRepository->getById($id);

        return response()->json([
            'data' => new CategoryResource($category),
            'posts' => PostResource::collection($this->postRepository->getByCategory($id))
        ]);
    }
}
