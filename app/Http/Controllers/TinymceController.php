<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\manager\WidgetService;

class TinymceController extends Controller
{
    public function __construct(
        private WidgetService $widgetService
    )
    {
    }

    public function addWidget(Request $request)
    {
        $code = $request->input('code');

        try {
            $widget = $this->widgetService->create($code);
            return $this->widgetService->generateWidget($widget->id);
        } catch (\Exception $exception) {
            return false;
        }
    }
}
