<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Cacheable\{CacheablePostRepository, CacheableTagRepository};

class TagController extends Controller
{
    const ITEMS_COUNT = 12;

    public function __construct(
        private CacheableTagRepository  $cacheableTagRepository,
        private CacheablePostRepository $cacheablePostRepository
    )
    {
    }

    public function index(Request $request, string $slug)
    {
        if (empty($tag = $this->cacheableTagRepository->getBySlug($slug, app()->getLocale()))) {
            abort(404);
        }

        if ($request->ajax()) {
            $posts = $this->cacheablePostRepository->getByTag((int)$tag->id, self::ITEMS_COUNT, $request->lastDate);

            return response()->json([
                'success'      => true,
                'html'         => view('frontend.posts.partials._posts', [
                    'posts' => $posts,
                ])->render(),
                'limitReached' => $posts->count() < self::ITEMS_COUNT
            ]);
        }

        $this->generateMetaTag($tag->name, $tag->description);
        return view('frontend.posts.tag', [
            'tag'        => $tag,
            'posts'      => $this->cacheablePostRepository->getByTag($tag->id, self::ITEMS_COUNT),
            'postsCount' => self::ITEMS_COUNT
        ]);
    }
}
