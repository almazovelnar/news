<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SiteController extends Controller
{
    public function __construct(
    )
    {
    }

    public function index(Request $request)
    {
        return view('frontend.home', [
        ]);
    }
}
