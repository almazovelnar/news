<?php
//declare(strict_types=1);
//
//namespace App\Http\Controllers\Frontend;
//
//use Illuminate\Http\Request;
//use App\Helpers\StringHelper;
//use App\Http\Controllers\Controller;
//use App\Repositories\ElasticSearchPostRepository;
//
//class SearchController extends Controller
//{
//    public const ITEMS_COUNT = 12;
//
//    public function __construct(
//        private ElasticSearchPostRepository $searchPostRepository
//    )
//    {
//    }
//
//    public function search(Request $request)
//    {
//        $query = StringHelper::clearSearchQuery($request->get('query'));
//
//        if ($request->ajax()) {
//            $posts = $this->searchPostRepository->getPosts(StringHelper::searchableString($query), self::ITEMS_COUNT, $request->lastDate);
//
//            return response()->json([
//                'success'      => true,
//                'html'         => view('frontend.posts.partials._search-posts', [
//                    'posts' => $posts
//                ])->render(),
//                'limitReached' => count($posts) < self::ITEMS_COUNT
//            ]);
//        }
//
//        $this->generateMetaTag($query);
//
//        return view('frontend.search', [
//            'posts'      => $this->searchPostRepository->getPosts(StringHelper::searchableString($query), self::ITEMS_COUNT),
//            'query'      => $query,
//            'postsCount' => self::ITEMS_COUNT,
//        ]);
//    }
//}
