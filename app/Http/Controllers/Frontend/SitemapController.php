<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;

use App;
use App\Http\Controllers\Controller;
use App\Services\Sitemap\SitemapFactory;
use Cache;
use Illuminate\Http\Response;

class SitemapController extends Controller
{

    public function __construct(
        private SitemapFactory $sitemapFactory
    )
    {
    }

    public function index(): Response
    {
        $cacheKey = sprintf('sitemap-%s.xml', App::getLocale());
        if (!Cache::has($cacheKey)) {
            $this->sitemapFactory
                ->createCommonBuilder()
                ->setLocale(App::getLocale())
                ->generateQuick();
        }
        return $this->xmlResponse($cacheKey);
    }

    public function posts(int $page): Response
    {
        $cacheKey = sprintf('sitemap-posts-%d-%s.xml', $page, App::getLocale());
        if (Cache::has($cacheKey)) {
            return $this->xmlResponse($cacheKey);
        }
        abort(404);
    }

    public function tags(int $page): Response
    {
        $cacheKey = sprintf('sitemap-tags-%d-%s.xml', $page, App::getLocale());
        if (Cache::has($cacheKey)) {
            return $this->xmlResponse($cacheKey);
        }
        abort(404);
    }

    public function categories(): Response
    {
        return $this->xmlResponse(sprintf('sitemap-categories-%s.xml', App::getLocale()));
    }

    public function pages(): Response
    {
        return $this->xmlResponse(sprintf('sitemap-pages-%s.xml', App::getLocale()));
    }

    public function channels(): Response
    {
        return $this->xmlResponse(sprintf('sitemap-channels-%s.xml', App::getLocale()));
    }

    private function xmlResponse(string $cacheKey): Response
    {
        return response(Cache::get($cacheKey), 200)->header('Content-Type', 'application/xml');
    }
}
