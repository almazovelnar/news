<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;

use App\Decorators\PostDecorator;
use App\Http\Controllers\Controller;
use App\Models\Post\Post;
use App\Repositories\Cacheable\{CacheableCategoryRepository, CacheablePostRepository, CacheableTagRepository};
use App\Services\PostRead;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class PostController extends Controller
{
    const RELATED_POSTS_COUNT = 10;
    const POSTS_COUNT = 12;
    const VIEW_CHOSEN_POSTS_COUNT = 6;

    public function __construct(
        private PostRead                    $postRead,
        private PostDecorator               $postDecorator,
        private CacheablePostRepository     $cacheablePostRepository,
        private CacheableCategoryRepository $cacheableCategoryRepository,
        private CacheableTagRepository      $cacheableTagRepository
    )
    {
    }

    public function view(Request $request, string $categorySlug, $postSlug)
    {
        try {
            $post = $this->cacheablePostRepository->getBySlug($postSlug);
        } catch (ModelNotFoundException $e) {
            if (is_numeric($postSlug)) {
                $post = $this->cacheablePostRepository->getByOldId((int)$postSlug);
                return redirect()->route('post', ['category' => $post->category_slug, 'slug' => $post->slug], 301);
            } else {
                throw new $e;
            }
        }

        if ($post->category_slug != $categorySlug) {
            return redirect()->route('post', ['category' => $post->category_slug, 'slug' => $post->slug], 301);
        }

        $category = $this->cacheableCategoryRepository->getToPostBySlug($categorySlug);

        if ($request->ajax() && $request->has('lastDate')) {
            $chosenPosts = $this->cacheablePostRepository->getChosenPostsByCategoryWithExclude($category->id, $post->id, self::VIEW_CHOSEN_POSTS_COUNT, $request->lastDate);

            return response()->json([
                'success'      => true,
                'html'         => view('frontend.posts.partials._posts', [
                    'posts' => $chosenPosts
                ])->render(),
                'limitReached' => $chosenPosts->count() < self::VIEW_CHOSEN_POSTS_COUNT
            ]);
        }

        $this->postDecorator->decorate($post);

        $this->createTranslatedLinks($post);
        $this->generateMetaTag($post->title, $post->description, $post->meta, true, get_image($post->image, $post->path, 'large'));
        return view('frontend.posts.view', [
            'post'             => $post,
            'posts'            => $this->cacheablePostRepository->getRelatedPosts($category->id, $post->id, self::RELATED_POSTS_COUNT),
            'chosenPosts'      => $this->cacheablePostRepository->getChosenPostsByCategoryWithExclude($category->id, $post->id, self::VIEW_CHOSEN_POSTS_COUNT),
            'category'         => $category,
            'chosenPostsCount' => self::VIEW_CHOSEN_POSTS_COUNT,
            'tags'             => $this->cacheableTagRepository->getByPost($post->id)
        ]);
    }

    public function shortView(Request $request, int $id)
    {
        $post = $this->cacheablePostRepository->getById($id);
        return redirect()->route('post', ['category' => $post->category_slug, 'slug' => $post->slug], 301);
    }

    public function embedView(Request $request, int $id)
    {
        $post = $this->cacheablePostRepository->getById($id);
        return view('frontend.posts.embed', [
            'post' => $post,
        ]);
    }

    public function explore(Request $request, ?int $page = 1)
    {
        if ($request->ajax()) {
            $posts = $this->cacheablePostRepository->getPosts(self::POSTS_COUNT, $request->lastDate);

            return response()->json([
                'success'      => true,
                'html'         => view('frontend.posts.partials._posts', [
                    'posts' => $posts,
                ])->render(),
                'limitReached' => $posts->count() < self::POSTS_COUNT
            ]);
        }

        $this->generateMetaTag(trans('site.page.explore'));
        return view('frontend.posts.explore', [
            'posts'      => $this->cacheablePostRepository->getPosts(self::POSTS_COUNT, null, $page),
            'postsCount' => self::POSTS_COUNT,
            'page'       => $page
        ]);
    }

    private function createTranslatedLinks(Post $post): void
    {
        View::share('translatedLinks', []);
    }
}
