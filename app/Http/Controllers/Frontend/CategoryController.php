<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;

use View;
use App\Models\Category;
use LaravelLocalization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\CategoryRepository;
use App\Repositories\Cacheable\{CacheableCategoryRepository, CacheablePostRepository};

class CategoryController extends Controller
{
    const ITEMS_COUNT = 12;
    const CHOSEN_POSTS_COUNT = 3;

    public function __construct(
        private CacheableCategoryRepository $cacheableCategoryRepository,
        private CacheablePostRepository     $cacheablePostRepository,
        private CategoryRepository          $categoryRepository
    )
    {
    }

    public function index(Request $request, string $slug)
    {
        $category = $this->cacheableCategoryRepository->getBySlug($slug);

        if ($request->ajax()) {
            $posts = $this->cacheablePostRepository->getLatestByCategory($category->id, self::ITEMS_COUNT, $request->lastDate);

            return response()->json([
                'success'      => true,
                'html'         => view('frontend.posts.partials._posts', [
                    'posts' => $posts,
                ])->render(),
                'limitReached' => $posts->count() < self::ITEMS_COUNT
            ]);
        }

        $lastPost = $this->cacheablePostRepository->getLastPostByCategory($category->id);
        $chosenPosts = $this->cacheablePostRepository->getChosenByCategory($category->id, self::CHOSEN_POSTS_COUNT);
        $categoryPosts = $this->cacheablePostRepository->getLatestByCategoryWithExclude($category->id, $lastPost?->id, self::ITEMS_COUNT);

        $this->createTranslatedLinks($category);
        $this->generateMetaTag($category->title, $category->description, $category->meta);

        return view('frontend.category', [
            'category'         => $category,
            'post'             => $lastPost,
            'chosenPosts'      => $chosenPosts,
            'categoryPosts'    => $categoryPosts,
            'postsCount'       => self::ITEMS_COUNT,
            'chosenPostsCount' => self::CHOSEN_POSTS_COUNT,
        ]);
    }

    public function chosenPosts(Request $request, string $slug)
    {
        $category = $this->categoryRepository->getBySlug($slug);

        if ($request->ajax()) {
            $posts = $this->cacheablePostRepository->getChosenByCategory($category->id, self::CHOSEN_POSTS_COUNT, $request->lastDate);

            return response()->json([
                'success'      => true,
                'html'         => view('frontend.posts.partials._posts', [
                    'posts' => $posts,
                ])->render(),
                'limitReached' => $posts->count() < self::CHOSEN_POSTS_COUNT
            ]);
        }
    }

    private function createTranslatedLinks(Category $category): void
    {
        if ($category->group_id) {
            $translatedLinks = [];
            foreach (LaravelLocalization::getSupportedLanguagesKeys() as $locale) {
                if ($translate = $this->cacheableCategoryRepository->getTranslateByLocale($category->group_id, $locale)) {
                    $translatedLinks[$locale] = LaravelLocalization::localizeUrl(
                        route('category', $translate->slug),
                        $locale
                    );
                }
            }

            View::share('translatedLinks', $translatedLinks);
        }
    }
}
