<?php
declare(strict_types=1);

namespace App\Http\Controllers\Frontend;

use App\Services\ContactService;
use App\Http\Controllers\Controller;
use App\Http\Requests\Page\ContactUsRequest;
use App\Repositories\Cacheable\CacheablePageRepository;

class StaticController extends Controller
{

    public function __construct(
        private CacheablePageRepository $cacheablePageRepository,
        private ContactService $contactService
    )
    {
    }

    public function view(string $slug)
    {
        $page = $this->cacheablePageRepository->getBySlug($slug);
        $this->generateMetaTag($page->title, \Str::words(strip_tags($page->description), 150));

        return view('frontend.static', [
            'page' => $page
        ]);
    }

    public function contactSubmit(ContactUsRequest $request)
    {
        try {
            $this->contactService->sendEmail($request->all());
            $message = trans('site.contact.request_sent_successfully');
        } catch (\Exception $e) {
            \Log::error($e);
            $message = trans('site.contact.request_couldnt_send');
        }

        return redirect()->back()->with('message', $message);
    }
}
