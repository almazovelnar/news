<?php

namespace App\Http\Controllers\Backend\Settings;

use App\Http\Controllers\Controller;
use App\Http\Requests\Settings\SettingRequest;
use App\Models\Setting;
use App\Services\manager\SettingService;
use Exception;
use Illuminate\Http\Request;
use Itstructure\GridView\DataProviders\EloquentDataProvider;
use function redirect;
use function trans;
use function view;

class SettingController extends Controller
{
    public function __construct(
        private SettingService $settingService
    )
    {
    }

    public function index()
    {
        $dataProvider = new EloquentDataProvider(Setting::query());

        return view('backend.settings.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function edit(Setting $setting)
    {
        return view('backend.settings.' . $this->getView($setting->name), [
            'setting' => $setting
        ]);
    }

    public function update(SettingRequest $request, Setting $setting)
    {
        try {
            $this->settingService->update($request, $setting);
            return redirect()->route('admin.settings.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.settings.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function updateChosenTags(Request $request, int $id)
    {
        $setting = Setting::find($id);

        try {
            $this->settingService->updateChosenTags($request, $setting);
            return redirect()->route('admin.settings.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.settings.index')
                ->with('error', trans('form.error.save'));
        }
    }

    private function getView(string $name): string
    {
        return view()->exists('backend.settings.' . $name) ? $name : 'update';
    }
}
