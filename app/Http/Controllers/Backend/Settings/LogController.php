<?php

namespace App\Http\Controllers\Backend\Settings;

use App\Models\Audit;
use App\Http\Controllers\Controller;
use Itstructure\GridView\DataProviders\EloquentDataProvider;

class LogController extends Controller
{
    public function index()
    {
        $dataProvider = new EloquentDataProvider(Audit::query());
        return view('backend.logs.index', [
            'dataProvider' => $dataProvider
        ]);
    }
}
