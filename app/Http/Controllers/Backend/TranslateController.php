<?php

namespace App\Http\Controllers\Backend;

use App\Components\DataProvider;
use App\Http\Controllers\Controller;
use App\Http\Requests\Translate\TranslateRequest;
use App\SearchQuery\TranslationSearchQuery;
use App\Services\manager\TranslateService;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class TranslateController extends Controller
{
    private $translateService;

    public function __construct(TranslateService $translateService)
    {
        $this->translateService = $translateService;
    }

    /**
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $filters = $request->filters;
        $filters['language'] = $filters['language'] ?? \Session::get('current-lang') ?? \App::getLocale();
        if ($filters['language'] != \Session::get('current-lang')) {
            \Session::put('current-lang', $filters['language']);
        }

        $dataProvider = new DataProvider((new TranslationSearchQuery())->search($filters), [
            'key',
            'group'
        ]);

        return view('backend.translates.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('backend.translates.create');
    }

    /**
     * @param TranslateRequest $request
     * @return RedirectResponse|void
     */
    public function store(TranslateRequest $request)
    {
        try {
            $this->translateService->create($request);
            return redirect()->route('admin.translates.index')
                ->with('success', trans('form.success.save'));
        }
        catch (Exception $e) {}
    }

    /**
     * @param LanguageLine $translate
     * @return Application|Factory|View
     */
    public function edit(LanguageLine $translate)
    {
        return view('backend.translates.update', [
            'translate' => $translate
        ]);
    }

    /**
     * @param TranslateRequest $request
     * @param LanguageLine $translate
     * @return RedirectResponse|void
     */
    public function update(TranslateRequest $request, LanguageLine $translate)
    {
        try {
            $this->translateService->update($request, $translate);
            return redirect()->route('admin.translates.index')
                ->with('success', trans('form.success.save'));
        }
        catch (Exception $e) {}
    }

    /**
     * @param LanguageLine $translate
     * @return RedirectResponse|void
     */
    public function destroy(LanguageLine $translate)
    {
        try {
            $this->translateService->remove($translate);
            return redirect()->route('admin.translates.index');
        }
        catch (Exception $e) {}
    }


    public function view()
    {
        return view('backend.translates.view');
    }

    public function load()
    {
        return view('backend.translates.view');
    }
}
