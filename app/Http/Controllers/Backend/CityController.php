<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend;

use App\Helpers\NodeHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\City\CityCreateRequest;
use App\Http\Requests\City\CityUpdateRequest;
use App\Models\City\City;
use App\Services\manager\CityService;
use Exception;
use Illuminate\Http\RedirectResponse;
use Itstructure\GridView\DataProviders\EloquentDataProvider;
use RuntimeException;

class CityController extends Controller
{
    private $cityService;

    public function __construct(CityService $cityService)
    {
        $this->cityService = $cityService;
    }

    public function index()
    {
        $query = City::query()->withDepth()->defaultOrder();
        $dataProvider = new EloquentDataProvider($query);

        return view('backend.cities.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        $parents = (new NodeHelper(City::class))->map();

        return view('backend.cities.create', compact('parents'));
    }

    public function store(CityCreateRequest $request)
    {
        try {
            $this->cityService->create($request);
            return redirect()->route('admin.cities.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.cities.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function show(City $category)
    {
        return view('backend.cities.view', [
            'category' => $category
        ]);
    }

    public function edit(City $city)
    {
        $parents = (new NodeHelper($city))->map();

        return view('backend.cities.update', compact('city', 'parents'));
    }

    public function update(CityUpdateRequest $request, City $city)
    {
        try {
            $this->cityService->update($request, $city);
            return redirect()->route('admin.cities.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.cities.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function destroy(City $city): RedirectResponse
    {
        try {
            $this->cityService->remove($city);
            return redirect()->route('admin.cities.index');
        } catch (RuntimeException $e) {
            return redirect()->route('admin.cities.index')->with('error', $e->getMessage());
        }
    }

    public function move(int $id, string $type): RedirectResponse
    {
        $category = City::findOrFail($id);
        ($type == 'up') ? $category->up() : $category->down();

        return redirect()->back();
    }
}
