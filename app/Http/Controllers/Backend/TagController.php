<?php

namespace App\Http\Controllers\Backend;

use Exception;
use App\Models\Tag;
use RuntimeException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\manager\TagService;
use App\Repositories\PostRepository;
use Illuminate\Http\RedirectResponse;
use Itstructure\GridView\DataProviders\EloquentDataProvider;
use App\Http\Requests\Tag\{TagCreateRequest, TagUpdateRequest};

class TagController extends Controller
{
    public function __construct(
        private TagService $tagService,
        private PostRepository $postRepository
    )
    {
    }

    public function index(Request $request)
    {
        $dataProvider = new EloquentDataProvider(Tag::query()->orderBy('name'));

        return view('backend.tags.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        return view('backend.tags.create');
    }

    public function store(TagCreateRequest $request)
    {
        try {
            $this->tagService->create($request);
            return redirect()->route('admin.tags.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.tags.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function edit(Tag $tag)
    {
        return view('backend.tags.update', [
            'tag' => $tag,
            'posts' => $this->postRepository->getByTag($tag->id)
        ]);
    }

    public function update(TagUpdateRequest $request, Tag $tag)
    {
        try {
            $this->tagService->update($request, $tag);
            return redirect()->route('admin.tags.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.tags.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function merge()
    {
        try {
            $mergedCount = $this->tagService->merge();
            return redirect()->route('admin.tags.index')
                ->with('success', trans('form.success.tag_merge', ['count' => $mergedCount]));
        } catch (Exception $e) {
            return redirect()->route('admin.tags.index')
                ->with('error', trans('form.error.tag_merge'));
        }
    }

    public function destroy($id): RedirectResponse
    {
        try {
            $this->tagService->remove($id);
            return redirect()->route('admin.tags.index')
                ->with('success', 'form.success.delete');
        } catch (RuntimeException $e) {
            return redirect()->route('admin.tags.index')
                ->with('error', 'form.error.delete');
        }
    }
}
