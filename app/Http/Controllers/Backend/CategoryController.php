<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend;

use DB;
use Exception;
use RuntimeException;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Repositories\CategoryRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Services\manager\CategoryService;
use Itstructure\GridView\DataProviders\EloquentDataProvider;
use App\Http\Requests\Category\{CategoryCreateRequest, CategoryUpdateRequest};

class CategoryController extends Controller
{

    public function __construct(
        private CategoryRepository $categoryRepository,
        private CategoryService $categoryService
    )
    {
    }

    public function index()
    {
        $lang = request()->input('filters.language');
        if ($lang) {
            session()->put('current-lang', $lang);
        }
        $lang  = (session()->get('current-lang') ?? $lang) ?? config('app.admin_content_locale');
        $query = Category::query()
            ->where('language', $lang)
            ->defaultOrder();

        $dataProvider = new EloquentDataProvider($query);

        return view('backend.categories.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        if ($lang = request()->get('lang')) {
            if ($id = request()->get('id')) {
                $category = Category::findOrFail($id);
                if ($translatedCategory = Category::where('group_id', $category->group_id)->where('language', $lang)->first()) {
                    return redirect()->route('admin.categories.edit', $translatedCategory);
                }
            }

            return view('backend.categories.create', [
                'lang'                 => $lang,
                'translatedCategoryId' => request('id'),
            ]);
        }

        return redirect()->route('admin.categories.index');
    }

    public function store(CategoryCreateRequest $request)
    {
        try {
            $this->categoryService->create($request);
            return redirect()->route('admin.categories.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.categories.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function show(Category $category)
    {
        return view('backend.categories.view', [
            'category' => $category
        ]);
    }

    public function edit(Category $category)
    {
        return view('backend.categories.update', [
            'category'             => $category,
            'translatedCategories' => $this->categoryRepository->translatedCats($category->group_id, $category->id),
        ]);
    }

    public function update(CategoryUpdateRequest $request, Category $category)
    {
        try {
            $this->categoryService->update($request, $category);
            return redirect()->route('admin.categories.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.categories.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function destroy(Category $category): RedirectResponse
    {
        try {
            $this->categoryService->remove($category);
            return redirect()->route('admin.categories.index')
                ->with('success', trans('form.success.delete'));
        } catch (RuntimeException $e) {
            return redirect()->route('admin.categories.index')
                ->with('error', trans('form.error.delete'));
        }
    }

    public function move(int $id, string $type): RedirectResponse
    {
        try {
            /** @var Category $category */
            $category = Category::findOrFail($id);
            ($type == 'up') ? $category->up() : $category->down();
        } catch (Exception $e) {
            \Log::debug($e);
        }

        return redirect()->route('admin.categories.index', ['page' => \request('page'), '_debug_' => true]);
    }

    public function translateList(Request $request)
    {
        $posts = $this->categoryRepository->getByQuery($request->q, $request->lang, $request->chosen)->toArray();

        return response()->json(['results' => array_values($posts)]);
    }

}
