<?php

namespace App\Http\Controllers\Backend;

use App\Enum\PostStatus;
use App\Models\Post\Post;
use App\Repositories\CategoryRepository;
use Illuminate\Support\Facades\Cookie;

class SiteController extends BaseController
{
    public function __construct(
        private CategoryRepository $categoryRepository
    )
    {
    }

    public function index()
    {
        $postQuery = Post::query()->where('status', PostStatus::ACTIVE);

        if (($language = Cookie::get('admin_locale'))) {
            $postQuery = $postQuery->where('language', $language);
        }

        $activePostCount = $postQuery->count();
        $activeCategoryCount = $this->categoryRepository->getActiveCategoryCount();

        return view('backend.site.index',
            compact('activePostCount', 'activeCategoryCount'));
    }

    public function changeLocale(string $locale)
    {
        \Cookie::queue(\Cookie::make('admin_locale', $locale));
        return redirect()->route('admin.home');
    }
}
