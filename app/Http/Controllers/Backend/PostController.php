<?php

namespace App\Http\Controllers\Backend;

use Exception;
use Illuminate\Support\Facades\Cookie;
use RuntimeException;
use App\Enum\PostType;
use Illuminate\Http\Request;
use App\Enum\PostActionType;
use App\Components\DataProvider;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\SearchQuery\PostSearchQuery;
use App\Services\manager\PostService;
use Illuminate\Http\RedirectResponse;
use App\Models\{Photo\Photo, Post\Post};
use App\Repositories\{CategoryRepository, PostRepository, TagRepository};
use App\Http\Requests\Post\{PostCreateRequest, PostUpdateCountsRequest, PostUpdateRequest};

class PostController extends Controller
{
    private string $language;

    public function __construct
    (
        private PostService $postService,
        private PostRepository $postRepository,
        private CategoryRepository $categoryRepository,
        private TagRepository $tagRepository,
    )
    {
        $this->checkAuthorize();
    }

    public function index(Request $request, $actionType = null)
    {
        $this->setLanguage();
        $this->setPage();
        $query        = new PostSearchQuery();
        $dataProvider = new DataProvider($query->search($request->filters, $actionType),
            ['status', 'type', 'category_id'], 'p', false);

        return view('backend.posts.index', [
            'actionType'     => $actionType ?? 'index',
            'dataProvider'   => $dataProvider,
            'categoriesList' => $this->categoryRepository->getList($this->language)
        ]);
    }

    public function setPage(): void
    {
        Cookie::queue(Cookie::make('page', request()->get('page') ?? 1));
    }

    public function show(int $id)
    {
        $post = $this->postRepository->get($id);

        return view('backend.posts.view', [
            'post' => $post
        ]);
    }

    public function create()
    {
        $lang   = \request()->get('lang');
        $photos = [];
        if (old('photos')) {
            foreach (old('photos') as $sort => $id) {
                $photos[$sort] = Photo::query()->where('id', $id)->first();
            }
        }

        $main['value'] = '';
        if (old('image')) {
            $oldImgId      = old('image');
            $oldMainImg    = Photo::query()->where('id', $oldImgId)->first();
            $main['value'] = $oldMainImg->filename;
        }

        if ($lang) {
            if ($id = request()->get('id')) {
                $post = Post::findOrFail($id);
                if ($translatedPost = Post::where('group_id', $post->group_id)->where('language', $lang)->first()) {
                    return redirect()->route('admin.post.edit', $translatedPost);
                }
            }

            return view('backend.posts.create', [
                'lang'             => $lang,
                'categories'       => $this->categoryRepository->allByLang($lang),
                'types'            => PostType::getList(),
                'translatedPostId' => request('id'),
                'photos'           => $photos,
                'main'             => $main,
            ]);
        }

        return redirect()->route('admin.post.index', [
            'page' => Cookie::get('page')
        ]);
    }

    public function store(PostCreateRequest $request)
    {
        $params = [
            'type'              => PostActionType::PENDING,
            'filters[language]' => $request->lang,
            'page'              => Cookie::get('page')
        ];

        try {
            $this->postService->create($request);
            return redirect()->route('admin.posts.list', $params)
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            \Log::debug($e);
            return redirect()->route('admin.posts.list', $params)
                ->with('error', trans('form.error.save'));
        }
    }

    public function edit(int $id)
    {
        $post = $this->postRepository->get($id);

        if ($this->postService->detectOpenedPost($post)) {
            return redirect()->route('admin.posts.list', [
                'type' => PostActionType::PENDING,
                'page' => Cookie::get('page')
            ])->with('error', trans('form.error.user_in_post'));
        }

        $photos = [];
        if (old('photos')) {
            foreach (old('photos') as $sort => $id) {
                $photos[$sort] = Photo::query()->where('id', $id)->first();
            }
        }

        $main['value'] = '';
        if (old('image')) {
            $oldImgId      = old('image');
            $oldMainImg    = Photo::query()->where('id', $oldImgId)->first();
            $main['value'] = $oldMainImg->filename;
        }
        if (empty($photos)) {
            $photos        = $post->photos;
            $main['value'] = $post->image;
        }

        return view('backend.posts.update', [
            'post'            => $post,
            'translatedPosts' => $this->postRepository->translatedPosts($post->group_id, $post->id),
            'referencePosts'  => $this->postRepository->getReferences($post->id),
            'categories'      => $this->categoryRepository->allByLang($post->language),
            'types'           => PostType::getList(),
            'photos'          => $photos,
            'main'            => $main,
        ]);
    }

    public function update(PostUpdateRequest $request, int $id)
    {
        $post = $this->postRepository->get($id);

        try {
            $this->postService->update($request, $post);
            $this->postService->removeUserFromOpenedPost($post);
            return redirect()->route('admin.posts.list', [
                'type' => $post->isActive() ? PostActionType::PUBLISHED : PostActionType::PENDING,
                'page' => Cookie::get('page')
            ])->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            \Log::debug($e);
            return redirect()->route('admin.posts.list', [
                'type' => PostActionType::PENDING,
                'page' => Cookie::get('page')
            ])->with('error', trans('form.error.save'));
        }
    }

    public function cancelForm(int $id)
    {
        $post = $this->postRepository->get($id);

        try {
            $this->postService->removeUserFromOpenedPost($post);
            return redirect()->route('admin.posts.list', [
                'type' => $post->isActive() ? PostActionType::PUBLISHED : PostActionType::PENDING,
                'page' => Cookie::get('page')
            ]);
        } catch (Exception $e) {
            \Log::debug($e);
            return redirect()->route('admin.posts.list', [
                'type' => PostActionType::PENDING,
                'page' => Cookie::get('page')
            ]);
        }
    }

    public function updateCounts(PostUpdateCountsRequest $request, int $id)
    {
        $post = $this->postRepository->get($id);

        try {
            $this->postService->updateCounts($request, $post);
            return redirect()->route('admin.post.show', $id)->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            \Log::debug($e);
            return redirect()->route('admin.post.show', $id)->with('error', trans('form.success.error'));
        }
    }

    public function restore(int $id)
    {
        try {
            $this->postService->restore($id);
            return redirect()->route('admin.posts.list', ['type' => PostActionType::PENDING])
                ->with('success', trans('form.success.restore'));
        } catch (Exception $e) {
            \Log::debug($e);
            return redirect()->route('admin.posts.list', ['type' => PostActionType::DELETED])
                ->with('error', trans('form.error.restore'));
        }
    }

    public function publish(int $id)
    {
        try {
            $this->postService->publish($id);
            return redirect()->route('admin.posts.list', ['type' => PostActionType::PUBLISHED])
                ->with('success', trans('form.success.publish'));
        } catch (Exception $e) {
            \Log::debug($e);
            return redirect()->route('admin.posts.list', ['type' => PostActionType::PENDING])
                ->with('error', trans('form.error.publish'));
        }
    }

    public function reject(int $id)
    {
        try {
            $this->postService->reject($id);
            return redirect()->route('admin.posts.list', ['type' => PostActionType::PENDING])
                ->with('success', trans('form.success.reject'));
        } catch (Exception $e) {
            \Log::debug($e);
            return redirect()->route('admin.posts.list', ['type' => PostActionType::PUBLISHED])
                ->with('error', trans('form.error.reject'));
        }
    }

    public function destroy($id): RedirectResponse
    {
        try {
            $this->postService->remove($id);
            return redirect()->route('admin.posts.list', [
                'type' => PostActionType::DELETED,
                'page' => Cookie::get('page')
            ])->with('success', trans('form.success.delete'));
        } catch (RuntimeException $e) {
            \Log::debug($e);
            return redirect()->route('admin.posts.list', [
                'type' => PostActionType::DELETED,
                'page' => Cookie::get('page')
            ])->with('error', trans('form.error.delete'));
        }
    }

    public function tagList(Request $request): JsonResponse
    {
        $posts = $this->tagRepository->getByQuery($request->q, $request->lang)->toArray();
        return response()
            ->json($this->getResults($posts));
    }

    public function translateList(Request $request): JsonResponse
    {
        $posts = $this->postRepository->getTranslate($request->q, $request->lang, $request->chosen)->toArray();
        return response()
            ->json($this->getResults($posts));
    }

    public function referenceList(Request $request): JsonResponse
    {
        $posts = $this->postRepository->getReference($request->q, $request->lang, $request->chosen, $request->id)->toArray();
        return response()
            ->json($this->getResults($posts));
    }

    private function getResults(array $data): array
    {
        return ['results' => empty($data)
            ? ['id' => '', 'name' => '']
            : array_values($data)];
    }

    private function setLanguage()
    {
        $language       = request()->input('filters.language');
        $langFromCache  = session()->get('current-lang');
        $this->language = $language ?? ($langFromCache ?? config('app.admin_content_locale'));

        if ($this->language != $langFromCache) {
            session()->put('current-lang', $this->language);
        }
    }

    public function deleteReason(Request $request)
    {
        try {
            $this->postService->createRemoveReason($request->postId, $request->reason);
            return response()->json(['success' => true]);
        } catch (Exception $e) {
            \Log::debug($e);
            return response()->json(['success' => false]);
        }
    }

    public function checkAuthorize()
    {
        $this->middleware(['can_any:post/*|post/reject'])->only(['reject']);
        $this->middleware(['can_any:post/*|post/publish'])->only(['publish']);
        $this->middleware(['can_any:post/*|post/restore'])->only(['restore']);
    }
}
