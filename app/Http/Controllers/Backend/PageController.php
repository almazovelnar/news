<?php
declare(strict_types=1);

namespace App\Http\Controllers\Backend;

use App\Helpers\NodeHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Page\{PageCreateRequest, PageUpdateRequest};
use App\Models\Page\Page;
use App\Services\manager\PageService;
use Exception;
use Illuminate\Http\RedirectResponse;
use Itstructure\GridView\DataProviders\EloquentDataProvider;
use RuntimeException;

class PageController extends Controller
{
    private $pageService;

    public function __construct(PageService $pageService)
    {
        $this->pageService = $pageService;
    }

    public function index()
    {
        $query = Page::query()->defaultOrder();
        $dataProvider = new EloquentDataProvider($query);

        return view('backend.pages.index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function create()
    {
        $parents = (new NodeHelper(Page::class))->map();

        return view('backend.pages.create', compact('parents'));
    }

    public function store(PageCreateRequest $request)
    {
        try {
            $this->pageService->create($request);
            return redirect()->route('admin.pages.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.pages.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function show(Page $page)
    {
        return view('backend.pages.view', [
            'page' => $page
        ]);
    }

    public function edit(Page $page)
    {
        $parents = (new NodeHelper(Page::class))->map($page->id);

        return view('backend.pages.update', compact('page', 'parents'));
    }

    public function update(PageUpdateRequest $request, Page $page)
    {
        try {
            $this->pageService->update($request, $page);
            return redirect()->route('admin.pages.index')
                ->with('success', trans('form.success.save'));
        } catch (Exception $e) {
            return redirect()->route('admin.pages.index')
                ->with('error', trans('form.error.save'));
        }
    }

    public function destroy(Page $page): RedirectResponse
    {
        try {
            $this->pageService->remove($page);
            return redirect()->route('admin.pages.index');
        } catch (RuntimeException $e) {
            return redirect()->route('admin.pages.index');
        }
    }

    public function move(int $id, string $type): RedirectResponse
    {
        $page = Page::findOrFail($id);
        ($type == 'up') ? $page->up() : $page->down();

        return redirect()->back();
    }

    public function imageDelete(int $id)
    {
        try {
            $this->pageService->removeImage($id);
            return response()->json(['success' => true]);
        } catch (RuntimeException $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ]);
        }
    }
}
