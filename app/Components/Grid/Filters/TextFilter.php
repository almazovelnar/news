<?php

namespace App\Components\Grid\Filters;

use Itstructure\GridView\Traits\Attributable;

class TextFilter extends \Itstructure\GridView\Filters\TextFilter
{
    use Attributable;

    public string $className = 'form-control';

    /**
     * @return string
     */
    public function render(): string
    {
        return view('grid_view::filters.text', [
            'name'           => $this->getName(),
            'value'          => $this->getValue(),
            'className'      => $this->getClassName(),
            'htmlAttributes' => $this->buildHtmlAttributes()
        ]);
    }

    /**
     * @return string
     */
    public function getClassName(): string
    {
        return $this->className;
    }
}
