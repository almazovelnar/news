<?php
declare(strict_types=1);

namespace App\Components;

use App;
use App\Models\MainInfo\MainInfo;
use Arr;

final class Information
{
    /** @var self */
    private static $instance;
    /** @var MainInfo */
    private $config;

    public function __construct() {}
    public function __wakeup() {}
    public function __clone() {}

    public static function instance(): self
    {
        if (empty(self::$instance)) {
            self::$instance = new self();
            self::$instance->loadConfig();
        }
        return self::$instance;
    }

    public function loadConfig(): void
    {
        $this->config = MainInfo::first();
    }

    public function get(string $key, $default = null)
    {
        $attributes = array_merge($this->config->getTranslation(App::getLocale())->getAttributes(), $this->config->getAttributes());

        if (!array_key_exists($key, $attributes)) {
            return $default;
        }

        $value = $attributes[$key];
        if (is_array($value) && Arr::isAssoc($value) && !empty($value[App::getLocale()])) {
            return $value[App::getLocale()];
        }
        return $value;
    }
}
