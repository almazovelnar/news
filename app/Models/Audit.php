<?php

namespace App\Models;

class Audit extends \OwenIt\Auditing\Models\Audit
{

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
