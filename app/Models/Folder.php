<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Folder extends Model
{
    use Sluggable;

    protected $translatable = ['title'];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => ['title']
            ]
        ];
    }

    public static function create($title, $slug = null)
    {
        $model = new static();

        $model->title = $title;
        $model->slug = $slug;

        return $model;
    }

    public function edit($title)
    {
        $this->title = $title;
    }
}
