<?php

namespace App\Models\MainInfo;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;


class MainInfo extends Model implements TranslatableContract
{
    use Translatable;

    public $table                = 'main_info';
    public $localeKey            = 'language';
    public $translationModel     = '\App\Models\MainInfo\MainInfoTranslation';
    public $translatedAttributes = ['title', 'description', 'meta', 'address'];
    public $timestamps           = false;

    public $casts = [
        'email' => 'array',
        'phone' => 'array',
        'meta'  => 'object',
    ];


    public function edit($name, $email, $phone, $fax, $location)
    {
        $this->name     = $name;
        $this->email    = $email;
        $this->phone    = $phone;
        $this->fax      = $fax;
        $this->location = $location;
    }

    public function attachLogo($logo)
    {
        $this->logo = $logo;
    }

    public function attachFavicon($favicon)
    {
        $this->favicon = $favicon;
    }

    public function detachFavicon()
    {
        $this->favicon = null;
    }
}
