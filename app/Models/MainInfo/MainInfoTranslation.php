<?php

namespace App\Models\MainInfo;

use Illuminate\Database\Eloquent\Model;

class MainInfoTranslation extends Model
{

    public $fillable = ['title', 'description', 'meta', 'address'];
    protected $casts = [
        'meta' => 'object'
    ];
    public $timestamps = false;
}
