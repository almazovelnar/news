<?php
declare(strict_types=1);

namespace App\Models;

use App\Enum\SettingType;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $name
 * @property string $value
 * @property string $type
 */
class Setting extends Model
{
    protected $table = 'settings';

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getValue(?string $lang = null)
    {
        switch ($this->type) {
            case SettingType::NUMERIC:
                return +$this->value;
            case SettingType::JSON:
                return $this->value ?
                    $lang ? (json_decode($this->value, true)[$lang] ?? []) : json_decode($this->value, true) :
                    [];
            default:
                return $this->value;

        }
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }
}
