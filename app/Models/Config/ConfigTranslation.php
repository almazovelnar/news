<?php
declare(strict_types=1);

namespace App\Models\Config;

use Illuminate\Database\Eloquent\Model;

class ConfigTranslation extends Model
{
    public $fillable   = ['value'];
    public $timestamps = false;
}
