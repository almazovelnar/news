<?php
declare(strict_types=1);

namespace App\Models\Config;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Config extends Model implements TranslatableContract
{
    use Translatable;

    public $localeKey            = 'language';
    public $fillable             = ['default'];
    public $translatedAttributes = ['value'];
    public $translationModel     = '\App\Models\Config\ConfigTranslation';
    public $timestamps           = false;

    public static function create($param, $default, $label, $type)
    {

        $model = new static();

        $model->param   = $param;
        $model->default = $default;
        $model->label   = $label;
        $model->type    = $type;

        return $model;
    }

    public function edit($param, $default, $label, $type)
    {
        $this->param   = $param;
        $this->default = $default;
        $this->label   = $label;
        $this->type    = $type;
    }
}
