<?php
declare(strict_types=1);

namespace App\Models\Post;

use Carbon\Carbon;
use DateTime;
use App\Models\Tag;
use App\Models\User;
use App\Enum\PostType;
use App\Enum\PostStatus;
use App\Models\Category;
use App\Traits\Sluggable;
use App\Models\Photo\Photo;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

/**
 * @property int $id
 * @property int $category_id
 * @property int $old_id
 * @property string $title
 * @property string $body
 * @property string $description
 * @property string $language
 * @property string $path
 * @property string $image
 * @property string $slug
 * @property string $audio_url
 * @property bool $is_published
 * @property int $status
 * @property bool $chosen
 * @property bool $is_slider
 * @property bool $on_band
 * @property bool $no_find
 * @property bool $no_index
 * @property int $views
 * @property int $likes
 * @property int $dislikes
 * @property int $user_id
 * @property int $lock_version
 * @property int|null $group_id
 * @property DateTime $published_at
 * @property DateTime $fixed_until
 *
 * @property string $type
 * @property string $priority
 * @property array $meta
 * @property array $marked_words
 *
 * @property Category $category
 * @property Collection<Photo> $photos
 */
class Post extends Model
{
    use SoftDeletes, HasFactory, Sluggable;

    protected $casts = [
        'meta' => 'object',
    ];

    protected $fillable = [
        'group_id', 'is_published', 'slug'
    ];

    public static function create($categoryId, $lang, $user, bool $chosen, bool $isSlider, bool $onBand, ?string $fixedUntil, bool $noFind, bool $noIndex, $type, $priority, string $title, ?string $description, $meta, $slug, $date)
    {
        $model = new static();

        $model->category_id  = $categoryId;
        $model->language     = $lang;
        $model->title        = $title;
        $model->description  = $description;
        $model->chosen       = $chosen;
        $model->is_slider    = $isSlider;
        $model->on_band      = $onBand;
        $model->fixed_until  = $fixedUntil;
        $model->no_find      = $noFind;
        $model->no_index     = $noIndex;
        $model->user_id      = $user;
        $model->type         = $type;
        $model->priority     = $priority;
        $model->meta         = $meta;
        $model->published_at = $date;
        $model->status       = PostStatus::PENDING;
        $model->generateSlug($slug);

        return $model;
    }

    public function edit($categoryId, bool $chosen, bool $isSlider, bool $onBand, ?string $fixedUntil, bool $noFind, bool $noIndex, string $type, string $priority, string $title, $description, $meta, $slug, $date)
    {
        $this->category_id  = $categoryId;
        $this->title        = $title;
        $this->description  = $description;
        $this->chosen       = +$chosen;
        $this->is_slider    = +$isSlider;
        $this->on_band      = +$onBand;
        $this->fixed_until  = $fixedUntil;
        $this->no_find      = +$noFind;
        $this->no_index     = +$noIndex;
        $this->type         = $type;
        $this->priority     = $priority;
        $this->meta         = $meta;
        $this->published_at = $date ?? now();
        $this->generateSlug($slug);
    }

    public function meta()
    {
        return $this->meta;
    }

    public function attachImage($path, $image)
    {
        $this->path  = $path;
        $this->image = $image;
    }

    public function detachImage()
    {
        $this->path  = null;
        $this->image = null;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tags()
    {
        return $this->belongsToMany(
            Tag::class,
            'post_tag'
        );
    }

    public function mainCategory()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function references()
    {
        return $this->belongsToMany(Post::class, 'post_references', 'post_id', 'reference_id');
    }

    public function photos()
    {
        return $this->belongsToMany(Photo::class, 'post_photo')->withTimestamps();
    }

    public function users()
    {
        return $this->belongsTo(User::class);
    }

    public function getContentAttribute()
    {
        return preg_replace(['/{{photo-token-([a-z0-9\-]+)}}/i', '/{{widget-token-([a-z0-9\-]+)}}/i'], '', $this->description);
    }

    public function isHidden(): int
    {
        return (int)($this->type == PostType::PRIVATE);
    }

    public function getStatus(): bool
    {
        return !!$this->status;
    }

    public function isActive(): bool
    {
        return $this->status == PostStatus::ACTIVE;
    }

    public function doPending()
    {
        $this->status       = PostStatus::PENDING;
        $this->is_published = 0;
    }

    public function doPublished()
    {
        $this->status = PostStatus::ACTIVE;
        if ($this->published_at <= now()) {
            $this->is_published = 1;
        }
    }

    public function isPublished(): bool
    {
        return !!$this->is_published;
    }

    public function isToBePublished(): bool
    {
        return $this->isActive() && ($this->published_at > now());
    }

    public function isActivePublished(): bool
    {
        return $this->is_published && $this->isActive();
    }

    public function isChosen(): bool
    {
        return !!$this->chosen;
    }

    public function isFixed()
    {
        return !empty($this->fixed_until);
    }

    public function isSlider(): bool
    {
        return !!$this->is_slider;
    }

    public function isOnBand(): bool
    {
        return !!$this->on_band;
    }

    public function syncCategories($ids)
    {
        $this->categories()->detach();
        $this->categories()->attach($ids);
    }

    public function isDeleted()
    {
        return !empty($this->deleted_at);
    }

    public function isNoIndex(): bool
    {
        return !!$this->no_index;
    }

    public function isNoFind(): bool
    {
        return !!$this->no_find;
    }

    public function getFixedUntil(): string
    {
        return Carbon::parse($this->fixed_until)->toDateString();
    }

    /**
     * @param string $logName
     */
    public function setLogName(string $logName): void
    {
        $this->logName = $logName;
    }

    /**
     * @param string $logDescription
     */
    public function setLogDescription(string $logDescription): void
    {
        $this->logDescription = $logDescription;
    }
}
