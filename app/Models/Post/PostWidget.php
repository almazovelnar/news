<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Model;

class PostWidget extends Model
{
    protected $table = 'post_widgets';

    public static function create(string $code, string $type)
    {
        $model = new static();

        $model->code = $code;
        $model->type = $type;

        return $model;
    }
}
