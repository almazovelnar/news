<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Model;

class PostPhoto extends Model
{
    public $timestamps = true;

    protected $table = 'post_photo';

    public static function create($postId, $photoId, $sortPhoto)
    {
        $model = new static();

        $model->post_id = $postId;
        $model->photo_id = $photoId;
        $model->sort = $sortPhoto;

        return $model;
    }
}
