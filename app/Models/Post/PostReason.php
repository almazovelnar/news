<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Model;

class PostReason extends Model
{
    protected $table = 'post_reasons';

    public static function create($userId, $postId, $reason)
    {
        $model = new static();

        $model->user_id = $userId;
        $model->post_id = $postId;
        $model->reason = $reason;

        return $model;
    }
}
