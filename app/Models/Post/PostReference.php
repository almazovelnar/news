<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Model;

class PostReference extends Model
{
    protected $table = 'post_references';

    public static function create($postId, $refId)
    {
        $model = new static();

        $model->post_id = $postId;
        $model->reference_id = $refId;

        return $model;
    }
}
