<?php
declare(strict_types=1);

namespace App\Models\Photo;

use Illuminate\Database\Eloquent\Model;

class PhotoTranslation extends Model
{
    public $fillable   = ['title', 'description'];
    public $timestamps = false;
}
