<?php

namespace App\Models\Photo;

use App\Models\Post\PostPhoto;
use App\Models\User;
use App\Models\Post\Post;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

/**
 * @property string $filename
 * @property string $slug
 * @property string $original_filename
 * @property string $author
 * @property string $path
 */
class Photo extends Model implements TranslatableContract
{
    use SoftDeletes, Translatable;

    public    $localeKey            = 'language';
    protected $translatedAttributes = ['title', 'description'];
    protected $fillable             = ['code', 'filename', 'original_filename', 'author_id'];

    public static function create($filename, $originalFilename, $author_id)
    {
        $model = new static();

        $model->slug              = strtoupper(uniqid());
        $model->filename          = $filename;
        $model->original_filename = $originalFilename;
        $model->author_id         = $author_id;

        return $model;
    }

    public function edit($is_exclusive, $published_at, $author_id = null)
    {
        $this->is_exclusive = $is_exclusive;
        $this->published_at = $published_at;

        if ($author_id) {
            $this->author_id = $author_id;
        }
    }

    public function post()
    {
        return $this->hasOneThrough(Post::class, PostPhoto::class,
            'photo_id', 'id', 'id', 'post_id')->orderBy('sorting');
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function sizes()
    {
        return $this->hasMany(Size::class);
    }

    public function size(string $type)
    {
        return $this->hasMany(Size::class)->where('type', $type)->first();
    }

    public function image()
    {
        return $this->hasOne(Image::class);
    }
}
