<?php

namespace App\Models;

use App\Enum\Status;
use App\Models\Post\Post;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $language
 * @property string $description
 * @property int $count
 * @property bool $chosen
 * @property bool $is_main
 *
 * @property-read string $withPrefix
 */
class Tag extends Model
{
    protected $fillable = ['count'];

    public static function create(string $name, string $slug, string $lang, ?string $description = null, ?int $count = 0, ?bool $chosen = false): static
    {
        $model = new static();

        $model->name        = $name;
        $model->slug        = $slug;
        $model->language    = $lang;
        $model->count       = $count;
        $model->description = $description;
        $model->chosen      = $chosen;

        return $model;
    }

    public function edit(string $name, string $slug, string $language, ?string $description, ?bool $chosen = false): void
    {
        $this->name        = $name;
        $this->slug        = $slug;
        $this->language    = $language;
        $this->description = $description;
        $this->chosen      = $chosen;
    }

    public function getWithPrefixAttribute(): string
    {
        return '#' . $this->name;
    }

    public function posts(): BelongsToMany
    {
        return $this->belongsToMany(Post::class, 'post_tag')
            ->where('status', Status::ACTIVE);
    }
}
