<?php
declare(strict_types=1);

namespace App\Models;

use stdClass;
use App\Traits\Sluggable;
use App\Models\Post\Post;
use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;


/**
 * @property int $id
 * @property string $language
 * @property string $title
 * @property string $description
 * @property string $slug
 * @property string $image
 * @property bool $status
 * @property bool $chosen
 * @property bool $on_menu
 * @property bool $hidden
 * @property int|null $parent_id
 * @property int|null $group_id
 * @property stdClass|null $meta
 */
class Category extends Model
{
    use NodeTrait, Sluggable, SoftDeletes, HasFactory;

    protected $fillable = ['group_id'];
    protected $casts    = [
        'meta' => 'object'
    ];

    public static function create(
        string $lang,
        string $title,
        ?string $description,
        bool $status,
        bool $chosen,
        bool $onMenu,
        string $slug,
        $meta
    ): static
    {
        $model = new static();

        $model->language    = $lang;
        $model->title       = $title;
        $model->description = $description;
        $model->generateSlug($slug);
        $model->status      = $status;
        $model->chosen      = $chosen;
        $model->on_menu     = $onMenu;
        $model->meta        = $meta;

        return $model;
    }

    public function edit(
        string $title,
        ?string $description,
        bool $status,
        bool $chosen,
        bool $onMenu,
        string $slug,
        $meta
    ): void
    {
        $this->title       = $title;
        $this->description = $description;
        $this->status      = $status;
        $this->chosen      = $chosen;
        $this->on_menu     = $onMenu;
        $this->meta        = $meta;
        $this->generateSlug($slug);
    }

    public function meta(): stdClass
    {
        return $this->meta;
    }

    public function attachImage(string $image): void
    {
        $this->image = $image;
    }

    public function detachImage(): void
    {
        $this->image = null;
    }

    public function getParentId(): int
    {
        return (int)$this->parent_id;
    }

    public function isOnMenu(): bool
    {
        return (bool)$this->on_menu;
    }

    public function isChosen(): bool
    {
        return (bool)$this->chosen;
    }

    public function getStatus(): bool
    {
        return (bool)$this->status;
    }

    public function posts(): BelongsToMany
    {
        return $this->belongsToMany(Post::class, 'category_post');
    }
}
