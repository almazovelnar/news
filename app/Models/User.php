<?php

namespace App\Models;

use App\Models\Post\Post;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $fillable = ['visited_at'];

    const DEFAULT_THUMB = 'user.png';

    public function getFullNameAttribute()
    {
        return "{$this->name}";
    }

    public static function create($name, $email, $status, $password)
    {

        $model = new static();

        $model->name = $name;
        $model->email = $email;
        $model->status = $status;
        $model->password = Hash::make($password);

        return $model;
    }

    public function edit($name, $email, $password)
    {
        $this->name = $name;
        $this->email = $email;

        if ($password) {
            $this->password = Hash::make($password);
        }
    }

    public function attachThumb($thumb)
    {
        $this->thumb = $thumb;
    }

    public function detachThumb()
    {
        $this->thumb = null;
    }

    public function isAdmin() {
        return $this->roles()->where('name', 'admin')->exists();
    }

    public function posts()
    {
        return $this->hasMany(Post::class,'user_id')
            ->orderByDesc('published_at');
    }

    public function getRole()
    {
        return $this->roles()->first();
    }
}
