<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $category_id
 * @property int $post_id
 */
class CategoryPost extends Model
{
    protected $table      = 'category_post';
    public    $timestamps = false;
}
