<?php
declare(strict_types=1);

namespace App\Models\Page;

use Illuminate\Database\Eloquent\Model;

class PageTranslation extends Model
{
    protected $fillable   = ['title', 'description', 'slug'];
    public    $timestamps = false;
}
