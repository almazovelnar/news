<?php
declare(strict_types=1);

namespace App\Models\Page;

use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

/**
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $slug
 * @property bool $status
 * @property bool $on_menu
 * @property bool $with_contacts
 */
class Page extends Model implements TranslatableContract
{
    use NodeTrait, Translatable;

    public $localeKey            = 'language';
    public $translatedAttributes = ['title', 'description', 'slug'];

    public static function create(bool $status, bool $menu)
    {
        $model = new static();

        $model->status  = $status;
        $model->on_menu = $menu;

        return $model;
    }

    public function edit(bool $status, bool $menu)
    {
        $this->status  = $status;
        $this->on_menu = $menu;
    }

    public function attachImage($image)
    {
        $this->image = $image;
    }

    public function detachImage()
    {
        $this->image = null;
    }
}
