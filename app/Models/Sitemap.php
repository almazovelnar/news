<?php

declare(strict_types=1);

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $post_id
 * @property int $page
 * @property string $language
 * @property string $link
 * @property string|Carbon $last_mod
 * @property string|Carbon $created_at
 * @property string|Carbon $updated_at
 */
class Sitemap extends Model
{
    protected $dates = ['last_mod'];

    protected $table = 'sitemap';

    public static function createNewOrUpdate(
        int $postId,
        int $page,
        string $language,
        string $link,
        string $lastMod
    ): void {
        /** @var self|null $item */
        $item = self::query()->where('post_id', $postId)->first();
        if (empty($item)) {
            $item           = new self();
            $item->post_id  = $postId;
            $item->page     = $page;
            $item->language = $language;
            $item->link     = $link;
            $item->last_mod = $lastMod;
            $item->save();
        } elseif (($item->page != $page) || ($item->link != $link) || ($item->last_mod != $lastMod)) {
            $item->page     = $page;
            $item->link     = $link;
            $item->last_mod = $lastMod;
            $item->save();
        }
    }
}
