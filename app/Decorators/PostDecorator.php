<?php

namespace App\Decorators;

use App\Models\Post\Post;
use App\Models\Post\PostWidget;
use App\Repositories\PhotoRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\View;

class PostDecorator
{

    public function __construct(private PhotoRepository $photoRepository)
    {
    }

    public function decorate(Post $post): void
    {
        $post->description = $this->applyPostPhotos($post->description);
        $post->description = $this->applyPostWidgets($post->description);
    }

    public function applyPostPhotos(?string $content): ?string
    {
        if (!$content) {
            return $content;
        }

        preg_match_all('/{{photo-token-([a-z0-9\-]+)}}/i', $content, $matches);
        foreach ($matches[1] as $index => $photoId) {
            $photoId = trim($photoId, '-');
            $photo   = $this->photoRepository->get($photoId);
            $imgBox  = \View::make('frontend.partials._imgBox', compact('photo'))->render();
            $content = str_replace($matches[0][$index], $imgBox, $content);
        }
        return $content;
    }

    public function applyGallery(string $content, Collection $photos): string
    {
        return $content . View::make('frontend.partials._gallery', compact('photos'))->render();
    }

    public function applyPostWidgets(?string $content): ?string
    {
        if (!$content) {
            return $content;
        }

        preg_match_all('/{{widget-token-(?P<widget_id>[0-9]+)}}/i', $content, $match);

        if (!empty($match['widget_id'])) {
            $widgets = PostWidget::find($match['widget_id'])->keyBy('id');

            foreach ($match[1] as $index => $widgetId) {
                if ($widgets->has($widgetId)) {
                    $widgetCode = $widgets->get($widgetId)->code;
                    $widgetContent = "<div class='widget-container'>{$widgetCode}</div>";
                    $content = str_replace($match[0][$index], $widgetContent, $content);
                } else {
                    $content = str_replace($match[0][$index], '', $content);
                }
            }
        }

        return $content;
    }
}
