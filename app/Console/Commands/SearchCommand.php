<?php

namespace App\Console\Commands;

use Meilisearch\Client;
use App\Models\Post\Post;
use App\Services\PostIndexer;
use Illuminate\Console\Command;
use Meilisearch\Endpoints\Indexes;
use Meilisearch\Contracts\DeleteTasksQuery;

class SearchCommand extends Command
{
    const POSTS_LIMIT = 5000;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:search {type?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected Indexes $index;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        private PostIndexer $postIndexer,
        private Client $client,
        private DeleteTasksQuery $deleteTasksQuery
    )
    {
        parent::__construct();
        $this->index = $this->client->index(config('search.index'));
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set('memory_limit', '512M');

        switch ($this->argument('type')) {
            case "create":
                $this->create();
                break;
            case "clear":
                $this->clear();
                break;
            case "clear-tasks":
                $this->clearTasks();
                break;
            default:
                $this->migratePosts();
        }

        return 0;
    }

    public function migratePosts()
    {
        $lastDate = is_file($fileName = storage_path('logs/last_index.txt')) ? file_get_contents($fileName) : null;

        $query = Post::query()
            ->where('is_published', true)
            ->where('no_find', false)
            ->orderBy('id');

        if ($lastDate) {
            $query->where('id', '>', $lastDate);
        }

        $posts = $query->limit(self::POSTS_LIMIT)->get();

        try {
            file_put_contents($fileName, $posts->last()->id);
            $this->postIndexer->batchIndex($posts->toArray());
        } catch (\Exception $e) {
            \Log::debug($e);
        }

        $this->info('Posts migrated successfully!');
    }

    protected function create()
    {
        $this->index->updateSettings([
            'searchableAttributes' => ['title'],
            'filterableAttributes' => ['language'],
            'sortableAttributes'   => ['published_at']
        ]);

        $this->index->updateSortableAttributes(['published_at']);
    }

    public function clear()
    {
        $this->postIndexer->clear();
    }

    public function clearTasks()
    {
        $this->client->deleteTasks($this->deleteTasksQuery->setIndexUids([config('search.index')]));
    }
}
