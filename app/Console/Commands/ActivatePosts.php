<?php

namespace App\Console\Commands;

use App\Enum\Status;
use App\Models\Post\Post;
use Illuminate\Console\Command;


class ActivatePosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'activate:posts';

    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $posts = Post::query()
            ->where('status', Status::ACTIVE)
            ->where('published_at', '<=', now())
            ->where('is_published', '!=', true)
            ->get();

        foreach ($posts as $post) {
            $post->update(['is_published' => true]);
        }

        return Command::SUCCESS;
    }
}
