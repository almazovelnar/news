<?php

namespace App\Console\Commands;

use Redis;
use Illuminate\Console\Command;

class InsertViews extends Command
{
    protected $signature = 'insert:views';
    protected $description = 'Command description';

    public function handle()
    {
        $keyName = 'views_';
        $viewsCollections = [];

        /** @var Redis $redis */
        $redis = \App::get('redis');
        $redis->select(8);
        $redis->setOption(Redis::OPT_PREFIX, '');
        $keys = $redis->keys($keyName . '*');

        foreach ($keys as $key) {
            if (!preg_match('/views\_([0-9]+)\_(az|ru|en)\_(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\_([a-zA-Z0-9]{32})\_[0-9]{10}/', $key, $matches)) {
                continue;
            }

            [$detectedHash, $postId, $lang, $userIp, $md5UA] = $matches;

            $views = (int)$redis->get($key);
            $views = empty($views) ? 1 : $views;

            if (isset($viewsCollections[$postId])) {
                $viewsCollections[$postId] += $views;
            } else {
                $viewsCollections[$postId] = $views;
            }

            $redis->del($key);
        }

        foreach ($viewsCollections as $postId => $viewCount) {
            \DB::update('UPDATE posts SET views=views+:count WHERE id=:id', [(int)$viewCount, (int)$postId]);
        }
    }
}
