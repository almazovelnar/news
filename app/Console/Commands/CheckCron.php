<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CheckCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $filename = 'cron-checker-' . uniqid() . '.txt';
        $fileScan = glob(storage_path('logs/cron-checker-*.txt'));
        if (isset($fileScan[0])) {
            unlink($fileScan[0]);
        }

        if (!file_exists($file = storage_path("logs/{$filename}"))) {
            file_put_contents($file, 'Test');
        }
    }
}
