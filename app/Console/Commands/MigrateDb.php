<?php

namespace App\Console\Commands;

use App\Helpers\WidgetTypeDetector;
use App\Models\Post\PostWidget;
use Str;
use App\Enum\PostType;
use App\Models\Category;
use App\Models\CategoryPost;
use App\Models\Folder;
use App\Models\Photo\Photo;
use App\Models\Post\Post;
use App\Models\Post\PostPhoto;
use App\Models\User;
use App\Enum\PostPriority;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Models\Post\PostReference;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class MigrateDb extends Command
{
    const DB_CONNECTION    = 'pgsql';
    const POSTS_LIMIT      = 1000;
    const REFERENCES_LIMIT = 1000;

    protected $signature   = 'migrate:db {type?}';
    protected $description = 'Command description';

    private $rootCategoryId;
    private $catIds         = [];
    private $photos         = [];
    private $postCategories = [];

    public function handle()
    {
        ini_set('memory_limit', '516M');
        $type = $this->argument('type');

        if ($type == 'post') {
            if (!Category::query()->whereIn('language', ['az', 'ru'])->first() || !User::first()) {
                $this->error('Users or categories not migrated!');
                return Command::FAILURE;
            }
            $count = $this->migratePosts();
            $this->info($count . ' posts successfully migrated!');
        } else if ($type == 'category') {
            $this->migrateCategories();
            $this->info('Categories successfully migrated!');
        } else if ($type == 'reference') {
            $this->migrateReferences();
            $this->info('References successfully migrated!');
        } else if ($type == 'user') {
            $this->migrateUsers();
            $this->info('Users successfully migrated!');
        } else if ($type == 'clear-data') {
            $this->clearData();
            $this->info('Data is cleared successfully migrated!');
        } else if ($type == 'clear-posts-data') {
            $this->clearPostsData();
            $this->info('Posts data is cleared successfully migrated!');
        } else {
            $this->error('Please write correct type of data (post, category)');
            return Command::FAILURE;
        }

        \Artisan::call('app:fix');
        return 0;
    }

    private function migratePosts()
    {
        $this->loadCategories();

        \Storage::createDir('photos');

        $lastDate = is_file($fileName = storage_path('logs/posts.txt')) ? file_get_contents($fileName) : null;
        $posts    = $this->getPosts($lastDate);

        $count = 0;
        foreach ($posts as $post) {
            $newPost = $this->createPost($post);

            if (!$newPost) {
                file_put_contents($fileName, $post->id);
                continue;
            }

            try {
                if ($newPost->save()) {
                    $this->migratePostCategories($newPost);
                    $this->syncPhotos($newPost);
                    $this->syncGallery($newPost);

                    $count++;
                    file_put_contents($fileName, $post->id);
                }
            } catch (\Exception $e) {
                \Log::debug($e);
            }
        }
        return $count;
    }

    private function migratePostCategories($post)
    {
        $cats = $this->postCategories[$post->id];
        foreach ($cats as $cat) {
            $postCategory              = new CategoryPost();
            $postCategory->category_id = $this->catIds[$cat]['id'];
            $postCategory->post_id     = $post->id;
            $postCategory->save();
        }
    }

    private function createPost($post)
    {
        $newPost = new Post();

        if (empty($postTitle = stripslashes(strip_tags($post->title)))) {
            return null;
        }

        $newPost->id       = $post->id;
        $newPost->group_id = $post->translated_article_id ?? $post->id;
        $newPost->title    = $postTitle;
        $this->generateSlug($newPost, $post);
        $newPost->body         = $post->description;
        $newPost->user_id      = $post->user_id;
        $newPost->is_published = $post->published;
        $newPost->is_slider    = $post->featured;
        $newPost->chosen       = $post->featured;
        $newPost->on_band      = $post->band;
        $newPost->views        = $post->hits;
        $newPost->likes        = $post->likes;
        $newPost->dislikes     = $post->dislikes;
        $newPost->published_at = $post->published_at;
        $newPost->audio_url    = $post->audio_url;
        $newPost->lock_version = $post->lock_version;
        $newPost->priority     = $this->getPriorities()[$post->review_priority];
        $newPost->type         = PostType::NORMAL;
        $newPost->setCreatedAt($post->created_at);
        $newPost->setUpdatedAt($post->updated_at);

        $this->setMeta($newPost);
        $this->cleanUpContent($newPost, $post->body);
        $this->extractWidgetsFromContent($newPost);

        if (!$this->extractCategoryAndLocale($newPost)) {
            return null;
        }

        if ($mainPhoto = $this->extractMainPhoto($post->image)) {
            $newPost->attachImage($mainPhoto->path, $mainPhoto->original_filename);
        }

        return $newPost;
    }

    private function cleanUpContent(Post $newPost, $content)
    {
        if ($content) {
            $content = stripslashes($content);
//            $content = preg_replace('{(<br[\\s]*(>|\/>)\s*){2,}}i', '$1', $content);
//            $content = preg_replace("/<p[^>]*>(\s{0,}(<[a-z]*[^>]*>){0,}\s{0,}){0,}<\/p>/", '', $content);
            $content = preg_replace("/&nbsp;/", '', $content);
        }

        $newPost->description = $content;
        return $newPost;
    }

    private function getPosts($lastDate)
    {
        $posts = \DB::connection(self::DB_CONNECTION)
            ->table('articles as n')
            ->select(['n.*', 'ph.image_uid as image', 'ph.created_at as image_created', 'ph.updated_at as image_updated'])
            ->leftJoin('photos as ph', 'ph.id', '=', 'n.primary_photo_id')
            ->where('deleted', false)
            ->orderBy('n.id');

        if ($lastDate) {
            $posts->where('n.id', '>', $lastDate);
        }

        return $posts
            ->limit(self::POSTS_LIMIT)
            ->get();
    }


    /**********************/

    private function createPhoto(string $filePath)
    {
        $pathInfo = pathinfo($filePath);
        [$folder, $fileName] = [$pathInfo['dirname'] ?? '', $pathInfo['basename']];

        $folderSlug = Str::slug($folder);
        if (!Folder::query()->where('slug', $folderSlug)->exists()) {
            Folder::create($folder, $folderSlug);
        }

        $photo = Photo::query()->firstOrCreate([
            'filename' => $fileName
        ],
            [
                'original_filename' => $fileName,
                'author_id'         => 1
            ]
        );

        $photo->path = $folder;

        if ($photo->save()) {
            return $photo;
        }

        return null;
    }

    private function extractMainPhoto(?string $image)
    {
        if (!empty($image)) {
            $photo = $this->createPhoto(sprintf('%s', rtrim($image, '/')));

            if ($photo) {
                $this->photos[] = $photo->id;
                return $photo;
            }
        }

        return null;
    }

    private function syncPhotos(Post $post): void
    {
        foreach ($this->photos as $sortPhoto => $photoId) {
            PostPhoto::create($post->id, (int)$photoId, $sortPhoto)->save();
        }
        $this->photos = [];
    }

    private function syncGallery(Post $post): void
    {
        $photos = \DB::connection(self::DB_CONNECTION)
            ->table('photos as ph')
            ->where('ph.document_id', $post->id);

        if ($photos->exists()) {
            foreach ($photos->get() as $key => $photo) {
                $newPhoto = $this->createPhoto($photo->image_uid);
                PostPhoto::create($post->id, (int)$newPhoto->id, $key)->save();
            }
        }
    }

    /**********************/

    private function migrateReferences()
    {
        $lastPostId = is_file($fileName = storage_path('logs/references.txt')) ? file_get_contents($fileName) : null;

        $refs = \DB::connection(self::DB_CONNECTION)
            ->table('references as r')
            ->orderBy('r.article_id');

        if ($lastPostId) {
            $refs->where('r.article_id', '>', $lastPostId);
        }

        $refs = $refs->limit(self::REFERENCES_LIMIT)->get();

        foreach ($refs as $ref) {
            try {
                $newRef = PostReference::create($ref->article_id, $ref->referenced_article_id);
                $newRef->setCreatedAt($ref->created_at);
                $newRef->setUpdatedAt($ref->updated_at);
                $newRef->save();
                file_put_contents($fileName, $ref->article_id);
            } catch (\Exception $exception) {
                file_put_contents($fileName, $ref->article_id);
                continue;
            }
        }
    }

    private function getPriorities()
    {
        return array_keys(PostPriority::getList());
    }

    private function setMeta($post)
    {
        $desc = $post->description ?? $post->body;

        $post->meta = [
            'title'       => $post->title,
            'description' => $desc ? \Str::words(strip_tags($desc), 150) : null,
            'keywords'    => null
        ];
    }

    private function extractCategoryAndLocale($post)
    {
        $categories = \DB::connection(self::DB_CONNECTION)
            ->table('publications')
            ->where('article_id', $post->id)
            ->orderBy('position')
            ->get();

        $cat = $categories->where('position', 0)->first() ?? $categories->first();
        if (!isset($this->catIds[$cat->page_id])) {
            return false;
        }

        $post->category_id               = $this->catIds[$cat->page_id]['id'];
        $post->language                  = $this->catIds[$cat->page_id]['language'];
        $this->postCategories[$post->id] = $categories->pluck('page_id')->toArray();

        return true;
    }

    private function generateSlug($post, $oldPost)
    {
        $slug       = trim(Str::slug($post->title, '-'));
        $existPost  = Post::query()->where('slug', $slug)->exists();
        $post->slug = !$existPost ? $slug : ($slug . '-' . $oldPost->id);
    }

    /**********************/

    private function migrateUsers()
    {
        $existingEmails = [];
        $users          = \DB::connection(self::DB_CONNECTION)
            ->table('users')
            ->whereNotIn('id', ['1'])
            ->get();


        foreach ($users as $user) {
            $newUser     = new User();
            $newUser->id = $user->id;
            if (in_array($user->email, $existingEmails)) {
                $newUser->email = Str::slug($user->fullname, '_') . '@oxu.az';
            } else {
                $newUser->email = $user->email;
            }
            $newUser->name     = $user->fullname;
            $newUser->password = Hash::make('Oxu.azPassword!');
            $newUser->status   = $user->active;
            $newUser->save();

            $oldRole = $user->role_id == 5 ? 2 : $user->role_id;

            $role = Role::query()->where('id', $oldRole)->first();
            $newUser->assignRole($role);

            $existingEmails[] = $user->email;
        }
    }

    /**********************/

    private function loadCategories()
    {
        $records = Category::query()
            ->select(['id', 'old_id', 'language'])
            ->get();

        foreach ($records as $record) {
            $this->catIds[$record->old_id]['id']       = $record->id;
            $this->catIds[$record->old_id]['language'] = $record->language;
        }
    }

    private function migrateCategories()
    {
        if (!Category::first()) {
            $rootCategory         = $this->createRootCategory();
            $this->rootCategoryId = $rootCategory->id;
        }

        $categories = \DB::connection(self::DB_CONNECTION)
            ->table('pages')
            ->whereNotNull(['parent_id', 'locale'])
            ->where('locale', '!=', '')
            ->get();

        foreach ($categories as $category) {
            $this->createCategory($category);
        }
    }

    private function createRootCategory()
    {
        $rootCategory = new Category();

        $rootCategory->title   = 'ROOT';
        $rootCategory->status  = false;
        $rootCategory->on_menu = false;
        $rootCategory->hidden  = true;
        $rootCategory->saveAsRoot();

        return $rootCategory;
    }

    private function createCategory($category)
    {
        $localeCategories = [10, 11, 12];

        $newCategory             = new Category();
        $newCategory->old_id     = $category->id;
        $newCategory->language   = $category->locale;
        $newCategory->title      = $category->title;
        $newCategory->slug       = $category->slug;
        $newCategory->on_menu    = $category->in_menu;
        $newCategory->chosen     = rand(0, 1);
        $newCategory->status     = true;
        $newCategory->created_at = $category->created_at;
        $newCategory->updated_at = $category->updated_at;
        $newCategory->parent_id  = in_array($category->parent_id, $localeCategories) ? $this->rootCategoryId : null;

        $newCategory->meta = [
            'title'       => $category->title,
            'description' => $category->body,
            'keywords'    => null
        ];

        if ($newCategory->save()) {
            $newCategory->update(['group_id' => $newCategory->id]);
        }
    }

    /**********************/

    private function extractWidgetsFromContent($post): void
    {
        $widgets = [];

        preg_match_all('/<iframe.*src=\"(.*)\".*><\/iframe>/isU', $post->description, $matches);
        if (!empty($matches[0])) {
            $content = $post->description;
            foreach ($matches[0] as $iframeCode) {
                $widget = PostWidget::create($iframeCode, WidgetTypeDetector::getTypeByContent($iframeCode));
                $widget->save();
                $content = str_replace($iframeCode, sprintf('{{widget-token-%d}}', $widget->id), $content);
            }

            $post->description = $content;
        }
    }

    private function clearData()
    {
        $this->clearPostsData();

        Category::query()->where('id', '>', '0')->forceDelete();
        DB::statement("ALTER TABLE categories AUTO_INCREMENT = 1");
    }

    private function clearPostsData()
    {
        Post::query()->where('id', '>', '0')->forceDelete();
        DB::statement("ALTER TABLE posts AUTO_INCREMENT = 1");

        PostPhoto::query()->where('id', '>', '0')->forceDelete();
        DB::statement("ALTER TABLE post_photo AUTO_INCREMENT = 1");

        Photo::query()->where('id', '>', '0')->forceDelete();
        DB::statement("ALTER TABLE photos AUTO_INCREMENT = 1");

        \Artisan::call('clear:thumbs');
    }
}
