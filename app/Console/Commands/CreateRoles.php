<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CreateRoles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rbac:create-roles';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Role::query()->whereNotIn('name', $this->getRoles())->delete();
        Permission::query()->whereRaw('1 > 0')->delete();

        foreach ($this->getRoles() as $role)
            Role::findOrCreate($role);
        foreach ($this->getPermissions() as $permission)
            Permission::findOrCreate($permission);

        foreach (Role::all() as $role) {
            if (isset($this->getRules()[$role->name])) {
                $permissions = Permission::whereIn('name', $this->getRules()[$role->name])->pluck('id');
                $role->syncPermissions($permissions);
            }
        }

        User::first()->assignRole(Role::where('name', 'admin')->first());
    }


    private function getRoles()
    {
        return [
            'admin',
            'editor',
            'editor-in-chief',
            'author',
        ];
    }

    private function getPermissions()
    {
        return [
            "*",
            '*/delete',

            'photo/*',
            'photos/*',

            'tags/*',
            'tags/index',

            'pages/*',
            'pages/index',

            'users/*',
            'users/index',

            'translations/*',
            'main-info/*',
            'configs/*',
            'settings/*',
            'logs/*',

            'pages/*',
            'pages/index',

            'categories/*',
            "categories/index",

            "post/publish",
            "post/reject",
            "post/restore",
            "post/*",

            "posts/*",
            "posts/create",
            "posts/store",
            "posts/index",
            "posts/edit",
            "posts/update",
            "posts/show",
            "posts/photos",
            "posts/published",
            "posts/pending",
            "posts/deleted",
        ];
    }

    private function getRules()
    {
        return [
            'admin'           => $this->getPermissions(),
            'editor'          => [
                "post/*",
                "posts/*",
                'photo/*',
                'photos/*',
            ],
            'editor-in-chief' => [
                "post/*",
                "posts/*",
                'photo/*',
                'photos/*',
                'logs/*',
                'main-info/*',
                'tags/*',
                'configs/*',
                'categories/*',
            ],
            'author'          => [
                "post/*",
                "posts/create",
                "posts/store",
                "posts/index",
                "posts/edit",
                "posts/update",
                "posts/show",
                "posts/photos",
                "posts/published",
                "posts/pending",
            ],
        ];
    }
}

