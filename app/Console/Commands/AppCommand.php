<?php

namespace App\Console\Commands;

use App\Models\Category;
use App\Models\Post\Post;
use Illuminate\Console\Command;

class AppCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:fix {name?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        switch ($this->argument('name')) {
            case 'category_tree':
                $this->fixCategoryTree();
                break;
            case 'init':
                $this->initProject();
                break;
            default:
                $this->optimizeApp();
                break;
        }
        return Command::SUCCESS;
    }

    private function optimizeApp()
    {
        \Artisan::call('insert:views');
        \Artisan::call('optimize:clear');
        \Artisan::call('optimize');
        \Artisan::call('route:trans:cache');

        $this->info('App optimized successfully!');
    }

    private function fixCategoryTree()
    {
        $this->createRootCategory();
        Category::fixTree();
        $this->info('Category tree fixed successfully!');
    }

    private function createRootCategory()
    {
        if (!($model = Category::query()->withTrashed()->withoutGlobalScopes()->where('title', 'ROOT')->first())) {
            $model = new Category();

            $model->title   = 'ROOT';
            $model->status  = false;
            $model->hidden  = true;
            $model->on_menu = false;
            $model->chosen  = false;

            $model->makeRoot();

            $model->save();
        }

        $this->makeChildCategories($model);
    }

    private function makeChildCategories($parent)
    {
        $categories = Category::query()->where('title', '!=', 'ROOT')->withTrashed()->withoutGlobalScopes()->get();
        foreach ($categories as $category) {
            $parent->appendNode($category);
        }
    }

    private function initProject()
    {
        \Artisan::call('optimize:clear');
        \Artisan::call('migrate');
        \Artisan::call('db:seed');
        \Artisan::call('rbac:create-roles');
        \Artisan::call('optimize');
        \Artisan::call('route:trans:cache');
    }
}
