<?php

namespace App\Services;

use Cache;

class CacheableDataService
{
//    public function deletePostsCache(string $language)
//    {
//        Cache::forget($this->genCacheKey('posts.explore', $language));
//    }

    public function genCacheKey(...$args): string
    {
        return implode('.', $args);
    }
}
