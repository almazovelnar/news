<?php
declare(strict_types=1);

namespace App\Services\manager;

use App\Helpers\WidgetTypeDetector;
use App\Models\Post\PostWidget;

class WidgetService
{

    public function __construct()
    {
    }

    public function create($code)
    {
        $widget = new PostWidget();
        $widget->code = $code;
        $widget->type = WidgetTypeDetector::getTypeByContent($code);
        $widget->save();

        return $widget;
    }

    public function generateWidget(int $id)
    {
        return "{{widget-token-$id}}";
    }
}
