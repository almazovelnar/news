<?php
declare(strict_types=1);

namespace App\Services\manager;

use App\Http\Requests\Settings\SettingRequest;
use App\Models\Setting;
use App\Models\Tag;
use Illuminate\Http\Request;

class SettingService
{
    public function update(SettingRequest $request, Setting $model): Setting
    {
        $model->setValue($request->value);
        $model->save();

        return $model;
    }

    public function updateChosenTags(Request $request, Setting $model): Setting
    {
        $newSettings = [];

        foreach ($request->tags as $lang => $tags) {
            foreach ($tags as $tagParam) {
                if (!($tag = Tag::find($tagParam))) {
                    $tag = Tag::create($tagParam, \Str::slug($tagParam, '_', $lang), $lang);
                    $tag->save();
                }

                $newSettings[$lang][$tag->id] = $tag->name;
            }
        }
        $model->setValue(json_encode($newSettings));
        $model->save();

        return $model;
    }
}
