<?php

namespace App\Services\manager;

use App\Http\Requests\Config\ConfigRequest;
use App\Models\Config\Config;
use App\Traits\Translatable;

class ConfigService
{
    use Translatable;

    public function create(ConfigRequest $request)
    {
        $model = Config::create(
            $request->param,
            $request->default,
            $request->label,
            $request->type
        );

        $this->generateTranslationsForFields($model->translatedAttributes);
        $model->fill($this->getTranslatedFields());

        $model->save();
        return $model;
    }

    public function update(Config $model, ConfigRequest $request)
    {
        $model->edit(
            $request->param,
            $request->default,
            $request->label,
            $request->type
        );

        $this->generateTranslationsForFields($model->translatedAttributes);
        $model->fill($this->getTranslatedFields());

        $model->save();
        return $model;
    }

    public function remove($model)
    {
        $model->delete();
    }
}
