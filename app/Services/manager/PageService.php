<?php
declare(strict_types=1);

namespace App\Services\manager;

use App\Traits\Sluggable;
use App\Traits\Translatable;
use RuntimeException;
use App\Models\Page\Page;
use App\Services\CacheableDataService;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Page\{PageCreateRequest, PageUpdateRequest};

class PageService
{
    use Translatable, Sluggable;

    public function __construct(
        private CacheableDataService $dataService
    )
    {
    }

    public function create(PageCreateRequest $request): Page
    {
        $model = Page::create(
            (bool) $request->status,
            (bool) $request->on_menu,
        );

        $this->generateTranslationsForFields($model->translatedAttributes);
        $model->fill($this->generateFields());

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file->store('pages');
            $model->attachImage($file->hashName());
        }

        $model->save();

        return $model;
    }

    public function update(PageUpdateRequest $request, Page $model): Page
    {
        $model->edit(
            (bool) $request->status,
            (bool) $request->on_menu,
        );

        $this->generateTranslationsForFields($model->translatedAttributes);
        $model->fill($this->generateFields());

        if ($request->hasFile('image')) {
            if (Storage::exists('pages/' . $model->image)) {
                Storage::delete('pages/' . $model->image);
                $model->detachImage();
            }

            $file = $request->file('image');
            $file->store('pages');
            $model->attachImage($file->hashName());
        }

        $model->save();

        return $model;
    }

    public function remove(Page $model)
    {
        if (!$model->delete()) {
            throw new RuntimeException('Deleting error');
        }
    }

    public function removeImage(int $pageId)
    {
        /** @var Page $model */
        $model = Page::findOrFail($pageId);
        $model->detachImage();
        if ($model->save()) {
            Storage::delete('pages/' . $model->image);
        }

    }
}
