<?php

namespace App\Services\manager;

use App\Http\Requests\Translate\TranslateRequest;
use App\Models\LanguageLine\LanguageLine;
use App\Traits\Translatable;

class TranslateService
{
    use Translatable;

    public function create(TranslateRequest $request) {

        $model = LanguageLine::create($request->group, $request->key);

        $this->generateTranslationsForFields($model->translatedAttributes);
        $model->fill($this->getTranslatedFields());

        $model->save();

        return $model;
    }

    public function update(TranslateRequest $request, LanguageLine $model)
    {
        $model->edit($request->group, $request->key);

        $this->generateTranslationsForFields($model->translatedAttributes);
        $model->fill($this->getTranslatedFields());

        $model->save();

        return $model;
    }

    public function remove(LanguageLine $translate)
    {
        $translate->delete($translate);
    }
}
