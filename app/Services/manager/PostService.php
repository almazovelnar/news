<?php
declare(strict_types=1);

namespace App\Services\manager;

use App\Helpers\LogHelper;
use App\Models\Post\PostReason;
use App\Services\PostIndexer;
use OwenIt\Auditing\Events\AuditCustom;
use Str;
use App\Models\Tag;
use App\Models\Post\Post;
use App\Traits\Seometable;
use App\Models\Photo\Photo;
use App\Models\Post\PostPhoto;
use App\Repositories\TagRepository;
use App\Services\CacheableDataService;
use App\Http\Requests\Post\{PostCreateRequest, PostUpdateCountsRequest, PostUpdateRequest};


class PostService
{
    const POST_VIEW_CACHE = 'admin-post-view';
    use Seometable;

    public function __construct(
        private TagRepository $tagRepository,
        private CacheableDataService $dataService,
        private PostIndexer $postIndexer
    )
    {
    }

    public function create(PostCreateRequest $request): Post
    {
        $model = Post::create(
            $request->category[0],
            $request->lang,
            \Auth::id(),
            (bool)$request->chosen,
            (bool)$request->is_slider,
            (bool)$request->on_band,
            !!$request->fixed ? $request->fixed_until : null,
            (bool)$request->no_find,
            (bool)$request->no_index,
            $request->type,
            $request->priority,
            $request->title,
            $request->description,
            $this->collectMeta(),
            $request->slug ?? $request->title,
            $request->date,
        );

        if ($photo = Photo::find($request->image)) {
            $model->image = $photo->filename;
            $model->path  = $photo->path;
        }

        if ($model->save()) {
            $model->references()->sync($request->reference);
            $model->syncCategories($request->category);
            $model->update(['group_id' => $request->translatedPostId ?? $model->id]);


            if ($request->photos) {
                foreach ($request->photos as $sortPhoto => $photoId) {
                    $postPhoto = PostPhoto::create($model->id, (int)$photoId, $sortPhoto);
                    $postPhoto->save();
                }
            }

            if ($tags = $this->collectTags($request->tags)) {
                $model->tags()->sync($tags);
            }

            $model->save();
        }

        return $model;
    }

    public function update(PostUpdateRequest $request, Post $model): Post
    {
        $oldModel = clone $model;
        $model->edit(
            $request->category[0],
            (bool)$request->chosen,
            (bool)$request->is_slider,
            (bool)$request->on_band,
            !!$request->fixed ? $request->fixed_until : null,
            (bool)$request->no_find,
            (bool)$request->no_index,
            $request->type,
            $request->priority,
            $request->title,
            $request->description,
            $this->collectMeta(),
            $request->slug ?? $request->title,
            $request->date,
        );

        if (!$model->isPublished()) {
            $model->doPending();
        } else {
            if (!$model->isNoFind()) {
                $this->postIndexer->index($model->toArray());
            } else {
                $this->postIndexer->remove($model->id);
            }
        }

        if (!$model->group_id) {
            $model->group_id = $model->id;
        }

        if (!$request->image) {
            $model->detachImage();
        } else {
            if ($photo = Photo::query()->find($request->image)) {
                $model->attachImage($photo->path, $photo->filename);
            }
        }

        if ($request->translate) {
            Post::where('group_id', $model->group_id)
                ->where('id', '<>', $model->id)
                ->update(['group_id' => null]);

            Post::whereIn('id', $request->translate)
                ->update(['group_id' => $model->group_id]);
        } else {
            Post::where('group_id', $model->group_id)
                ->where('id', '<>', $model->id)
                ->update(['group_id' => null]);
        }

        if ($model->save()) {
            $model->syncCategories($request->category);
            $model->references()->sync($request->reference);

            $model->photos()->detach();
            if ($request->photos) {
                foreach ($request->photos as $sortPhoto => $photoId) {
                    $postPhoto = PostPhoto::create($model->id, $photoId, $sortPhoto);
                    $postPhoto->save();
                }
            }

            $model->tags()->sync($this->collectTags($request->tags, $model->tags));
            $model->save();
        }

        return $model;
    }

    public function updateCounts(PostUpdateCountsRequest $request, Post $model): Post
    {
        $model->views    += $request->views;
        $model->likes    += $request->likes;
        $model->dislikes += $request->dislikes;

        if ($model->isDirty()) {
            $model->save();
        }

        return $model;
    }

    public function restore(int $id)
    {
        $post = Post::query()->withTrashed()->findOrFail($id);
        $post->restore();
        $post->doPending();
        $post->save();
    }

    public function publish(int $id)
    {
        if (cache()->get('admin-post-view.' . $id)) {
            throw new \RuntimeException('This post is busy');
        }

        $post = Post::query()->findOrFail($id);
        $post->doPublished();
        if ($post->save() && !$post->isNoFind()) {
            $this->postIndexer->index($post->toArray());
        }
    }

    public function reject(int $id)
    {
        $post = Post::query()->findOrFail($id);
        $post->doPending();
        if ($post->save()) {
            $this->postIndexer->remove($id);
        }
    }

    private function collectTags($inputTags = [], $existingTags = null): array
    {
        $tagsIds = [];
        if (!empty($inputTags)) {
            foreach ($inputTags as $lang => $tagItems) {
                foreach ($tagItems as $tagItem) {
                    $tagSlug = Str::slug($tagItem);

                    if (($tag = $this->tagRepository->getByNameOrId($tagItem, $lang)) != null) {
                        if (empty($existingTags) || !$existingTags->contains($tag->id)) {
                            $tag->count++;
                        }
                    } else {
                        $tag = Tag::create($tagItem, $tagSlug, $lang, null, 1, false);
                    }

                    if ($tag->save()) {
                        $tagsIds[] = $tag->id;
                    }
                }
            }
        }

        return $tagsIds;
    }

    public function detectOpenedPost(Post $post): bool
    {
        $openedPostUser = cache()->get(self::POST_VIEW_CACHE . '.' . $post->id);
        if (!empty($openedPostUser) && $openedPostUser != \Auth::id()) {
            return true;
        }

        if (empty($openedPostUser)) {
            cache()->set('admin-post-view.' . $post->id, \Auth::id(), 3600);
        }

        return false;
    }

    public function removeUserFromOpenedPost($post)
    {
        cache()->delete(self::POST_VIEW_CACHE . '.' . $post->id);
    }

    public function remove($id)
    {
        /** @var Post $model */
        $model = Post::findOrFail($id);
        $model->doPending();
        $model->save();
        $model->delete();
        $this->postIndexer->remove($model->id);
    }

    public function createRemoveReason($postId, $reason)
    {
        $postReason = PostReason::create(\Auth::id(), $postId, $reason);
        $postReason->save();
    }
}

