<?php

namespace App\Services\manager;

use App\Http\Requests\MainInfo\MainInfoRequest;
use App\Models\MainInfo\MainInfo;
use App\Traits\Translatable;
use Cache;
use Illuminate\Support\Facades\Storage;

class MainInfoService
{
    use Translatable;

    public function update(MainInfoRequest $request, MainInfo $model)
    {
        $model->edit(
            $request->name,
            array_filter($request->email),
            array_filter($request->phone),
            $request->fax,
            $request->location
        );

        $this->generateTranslationsForFields($model->translatedAttributes);
        $model->fill($this->getTranslatedFields());

        $model->save();
        return $model;
    }

    public function deleteLogo(int $id, string $lang)
    {
        $model = MainInfo::findOrFail($id);
        Storage::delete('main-info/' . $model->getTranslation('logo', $lang));
        $model->forgetTranslation('logo', $lang);

        $model->save();
    }

    public function deleteFavicon(int $id, string $lang)
    {
        $model = MainInfo::findOrFail($id);
        Storage::delete('main-info/' . $model->getTranslation('favicon', $lang));
        $model->forgetTranslation('favicon', $lang);

        $model->save();
    }
}
