<?php
declare(strict_types=1);

namespace App\Services\manager;

use RuntimeException;
use App\Models\Category;
use App\Traits\Seometable;
use App\Services\CacheableDataService;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Category\{CategoryCreateRequest, CategoryUpdateRequest};

class CategoryService
{
    use Seometable;

    public function __construct(
        private CacheableDataService $dataService
    )
    {
    }

    public function create(CategoryCreateRequest $request): Category
    {
        $model = Category::create(
            $request->lang,
            $request->title,
            $request->description,
            (bool)$request->status,
            (bool)$request->chosen,
            (bool)$request->on_menu,
            $request->slug ?? $request->title,
            $this->collectMeta()
        );

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file->store('categories');
            $model->attachImage($file->hashName());
        }

        if ($model->save()) {
            $model->appendToNode(Category::whereIsRoot()->first());
            $model->update(['group_id' => $request->translatedCategoryId ?? $model->id]);
        }

        return $model;
    }

    public function update(CategoryUpdateRequest $request, Category $model): Category
    {
        $model->edit(
            $request->title,
            $request->description,
            (bool)$request->status,
            (bool)$request->chosen,
            (bool)$request->on_menu,
            $request->slug ?? $request->title,
            $this->collectMeta()
        );

        if ($request->hasFile('image')) {
            if (Storage::exists('categories/' . $model->image)) {
                Storage::delete('categories/' . $model->image);
                $model->detachImage();
            }

            $file = $request->file('image');
            $file->store('categories');
            $model->attachImage($file->hashName());
        }

        if (!$model->group_id) {
            $model->group_id = $model->id;
        }

        if ($request->translate) {
            Category::where('group_id', $model->group_id)
                ->where('id', '<>', $model->id)
                ->update(['group_id' => null]);
            Category::whereIn('id', $request->translate)->update(['group_id' => $model->group_id]);
        } else {
            Category::where('group_id', $model->group_id)->where('id', '<>', $model->id)->update(['group_id' => null]);
        }

        $model->save();

        return $model;
    }

    public function remove(Category $model)
    {
        if (!$model->forceDelete()) {
            throw new RuntimeException('Deleting error');
        }
    }
}
