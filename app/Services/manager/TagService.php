<?php

namespace App\Services\manager;

use App\Helpers\StringHelper;
use App\Http\Requests\Tag\{TagCreateRequest, TagUpdateRequest};
use App\Models\Post\PostTag;
use App\Models\Tag;
use App\Services\CacheableDataService;

class TagService
{
    public function create(TagCreateRequest $request)
    {
        $model = Tag::create(
            $request->name,
            StringHelper::generateSlug($request->slug ?? $request->name, $request->language),
            $request->language,
            $request->description,
            0,
            (bool)$request->chosen
        );

        $model->save();
        return $model;
    }

    public function update(TagUpdateRequest $request, $model)
    {
        $oldModel = clone $model;
        /** @var Tag $model */
        $model->edit(
            $request->name,
            StringHelper::generateSlug($request->slug ?? $request->name, $request->language),
            $request->language,
            $request->description,
            (bool)$request->chosen
        );

        $model->save();
        return $model;
    }

    public function remove(int $id) {
        $model = Tag::findOrFail($id);
        $model->delete();
    }

    public function merge() {
        $tags = \DB::table('tags')
            ->selectRaw('slug, COUNT(id) as countSame')
            ->having('countSame', '>', 1)
            ->groupBy('slug')
            ->get()
            ->take(50)
            ->toArray();

        foreach ($tags as $tag) {
            $tagsBySlug = Tag::query()->where('slug', $tag->slug)->orderByDesc('count')->get();
            $usedCount = 0;
            $idsForDelete = [];
            $newTag = null;

            foreach ($tagsBySlug as $key => $tagBySlug) {
                $usedCount += $tagBySlug->count;
                if ($key != 0) {
                    $idsForDelete[] = $tagBySlug->id;
                }
                if ($key == 0) {
                    $newTag = $tagBySlug;
                }
            }

            if ($newTag) {
                PostTag::query()->where('tag_id', $idsForDelete)->update(['tag_id' => $newTag->id]);
                $newTag->update(['count' => $usedCount]);
                Tag::query()
                    ->where('id', $idsForDelete)
                    ->where('id', '!=', $tagsBySlug->get(0)->id)
                    ->delete();
            }
        }

        return count($tags);
    }
}
