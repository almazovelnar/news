<?php
declare(strict_types=1);

namespace App\Services\Sitemap\Sources;

use App\Repositories\TagRepository;
use App\Services\Sitemap\IndexItem;
use App\Services\Sitemap\MapItem;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;

class TagSource implements SitemapSource
{

    public const ITEMS_PER_PAGE = 15000;

    public function __construct(private TagRepository $repository)
    {
    }

    public function generate(?string $locale = null): array
    {
        $pages = range(0, (int)($this->repository->getTagsCount($locale) / self::ITEMS_PER_PAGE));

        $items = [];
        foreach ($pages as $page) {
            $mapItems = $this->getMapItems($page, $locale);

            if ($mapItems->isEmpty()) {
                Cache::delete("sitemap-tags-{$page}-{$locale}.xml");
                break;
            }

            $items[] = new IndexItem(
                route('sitemap.tags', $page),
                $mapItems->last()->lastModified
            );

            $xmlContent = View::make("frontend.sitemap.posts", ["postItems" => $mapItems->toArray()])->render();
            Cache::set("sitemap-tags-{$page}-{$locale}.xml", $xmlContent);
        }

        return $items;
    }

    private function getMapItems(int $page, string $locale): Collection
    {
        $mapItems = new Collection();

        $tags = $this->repository->getByRange($page * self::ITEMS_PER_PAGE, self::ITEMS_PER_PAGE, $locale);
        foreach ($tags as $tag) {
            $mapItems->add(new MapItem(route('tag', [$tag['slug']]), strtotime($tag['updated_at'])));
        }

        return $mapItems->reverse();
    }
}
