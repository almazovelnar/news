<?php
declare(strict_types=1);

namespace App\Services\Sitemap\Sources;

use App\Repositories\CategoryRepository;
use App\Services\Sitemap\IndexItem;
use App\Services\Sitemap\MapItem;

class CategorySource extends SinglePageSource implements SitemapSource
{

    public function __construct(private CategoryRepository $repository)
    {
    }

    public function generate(?string $locale = null): array
    {
        $this->generateWithParams(
            $this->repository->allByLang($locale)->toArray(),
            "sitemap-categories-{$locale}.xml",
            function (array $record) {
                return route('category', $record['slug']);
            },
            'updated_at',
            MapItem::HOURLY
        );

        return [new IndexItem(route('sitemap.categories'))];
    }
}
