<?php

declare(strict_types=1);

namespace App\Services\Sitemap\Sources;

use App\Models\Sitemap;
use App\Models\Sitemap as SitemapModel;
use App\Repositories\PostRepository;
use App\Services\Sitemap\IndexItem;
use App\Services\Sitemap\MapItem;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;

class PostSource implements SitemapSource
{

    public const ITEMS_PER_PAGE = 15000;

    public function __construct(private PostRepository $repository)
    {
    }

    public function generate(?string $locale = null): array
    {
        $pages = range(0, (int)($this->repository->getActivePostsCount($locale) / self::ITEMS_PER_PAGE));

        $items = [];
        foreach ($pages as $page) {
            $mapItems = $this->getMapItems($page, $locale);

            $items[] = new IndexItem(
                route('sitemap.posts', $page),
                $mapItems->last()?->lastModified
            );

            $postsXmlData = View::make("frontend.sitemap.posts", ["postItems" => $mapItems->toArray()])->render();
            Cache::set("sitemap-posts-{$page}-{$locale}.xml", $postsXmlData);
        }

        return $items;
    }

    public function generateQuick(string $locale): array
    {
        $items = [];
        foreach ($this->getPageItems($locale) as $pageItem) {
            $items[] = new IndexItem(
                route('sitemap.posts', $pageItem->page),
                $pageItem->last_mod->getTimestamp()
            );
        }
        return $items;
    }

    public function updateOrCreatePage(string $locale): void
    {
        $page = (int)Sitemap::where('language', $locale)->max('page');
        $postsCount = Sitemap::query()->where('page', $page)->where('language', $locale)->count('id');
        if ($postsCount >= self::ITEMS_PER_PAGE) {
            $page++;
        }

        $mapItems = $this->getMapItems($page, $locale);

        $postsXmlData = View::make("frontend.sitemap.posts", ["postItems" => $mapItems->toArray()])->render();
        Cache::set("sitemap-posts-{$page}-{$locale}.xml", $postsXmlData);

        Cache::delete("sitemap-{$locale}.xml");
    }

    private function getMapItems(int $page, string $locale): Collection
    {
        $mapItems = new Collection();

        $posts = $this->repository->getByRange($page * self::ITEMS_PER_PAGE, self::ITEMS_PER_PAGE, $locale);
        foreach ($posts as $post) {
            if (!$post->slug || !$post->category_slug) {
                continue;
            }

            $link = route('post', ['category' => $post->category_slug, 'slug' => $post->slug]);
            Sitemap::createNewOrUpdate($post->id, $page, $locale, $link, $post->updated_at ?? $post->created_at);
            $mapItems->add(new MapItem($link, strtotime($post->updated_at ?? $post->created_at)));
        }

        return $mapItems->reverse();
    }

    private function getPageItems(string $locale): \Illuminate\Database\Eloquent\Collection
    {
        return SitemapModel::query()
            ->selectRaw('page, MAX(last_mod) as last_mod')
            ->where('language', $locale)
            ->groupBy('page')
            ->oldest('page')
            ->get();
    }
}
