<?php
declare(strict_types=1);

namespace App\Services\Sitemap\Sources;

use App\Repositories\PageRepository;
use App\Services\Sitemap\IndexItem;

class PageSource extends SinglePageSource implements SitemapSource
{

    public function __construct(private PageRepository $repository)
    {
    }

    public function generate(?string $locale = null): array
    {
        $sourceExists = $this->generateWithParams(
            $this->repository->pageQuery()->get()->toArray(),
            sprintf('sitemap-pages-%s.xml', $locale),
            function (array $record) {
                return route('page', $record['slug']);
            }
        );

        return $sourceExists ? [new IndexItem(route('sitemap.pages'))] : [];
    }
}
