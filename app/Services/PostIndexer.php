<?php

namespace App\Services;

use Meilisearch\Client;
use Meilisearch\Endpoints\Indexes;

class PostIndexer
{
    protected Indexes $index;

    public function __construct(
        private Client $client
    )
    {
        $this->index = $this->client->index(config('search.index'));
    }

    public function batchIndex(array $posts)
    {
        $this->index->addDocumentsInBatches($posts, 5000);
    }

    public function index(array $posts)
    {
        $this->index->addDocuments($posts);
    }

    public function remove(int $postId)
    {
        $this->index->deleteDocument($postId);
    }

    public function clear()
    {
        $this->client->index(config('search.index'))->deleteAllDocuments();
    }
}
