<?php

namespace App\Widgets\PhotoManager;

use App\Models\Folder;
use Arrilot\Widgets\AbstractWidget;

class PhotoManager extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $modal = \View::make('widgets.photo_manager._modals',
            [
                'mode' => 'gallery',
                'folders' => Folder::query()->latest()->get()
            ]);

        return view('widgets.photo_manager.gallery', [
            'config' => $this->config,
            'modal' => $modal,
            'main' => $this->config['main'] ?? [],
            'photos' => $this->config['photos'] ?? [],
        ]);
    }
}
