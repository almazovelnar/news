<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

class LangSwitcher extends AbstractWidget
{
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        return view('widgets.lang_switcher', [
            'config' => $this->config,
        ]);
    }
}
